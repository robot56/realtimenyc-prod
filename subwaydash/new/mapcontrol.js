var viewPos = [40.740, -73.950];
var viewZoom = 12;

var map = L.map('map', {
    // markerZoomAnimation: false
}).setView(viewPos, viewZoom);

map.zoomControl.setPosition('bottomright');

var tileservers = [
    "https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png",
    "http://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png",
    "http://cartodb-basemaps-{s}.global.ssl.fastly.net/rastertiles/voyager/{z}/{x}/{y}.png"
]
var embedded = false;

var tileserver = tileservers[2];

var tile = L.tileLayer(tileserver, {
	attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>',
	subdomains: 'abcd',
	maxZoom: 19
});

var Options = {
    ConnectCAFE: !embedded
};

// /* Leaflet Clickable Area Patch */
// // We use this for mobile devices because of morons
// L.Path.include({
//     _clickTolerance: function() {
//         console.log('called _clickTolerance ' + this.options.clickTolerance)
//         return null;
//         // return (this.options.clickTolerance ? this.options.clickTolerance : 0) + (this.options.stroke ? this.options.weight / 2 : 0) + (L.Browser.touch ? 10 : 0);
//     }
// });

var gm_arial_tile = L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
    maxZoom: 20,
    attribution: 'Google Maps Arial View',
    subdomains:['mt0','mt1','mt2','mt3']
});

//tile.addTo(map);
map.addLayer(tile)

RTNYC.Subway.init();

setInterval(() => {
    
}, 1000);