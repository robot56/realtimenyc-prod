class ViewController {
    viewSelectSubwayLine() {
        $('#frontui').load('components/selectsubwayline.htm');
    }

    viewStation(id) {
        $('#frontui').load('components/viewstation.htm');
    }

    viewSequence() {
        $('#superui').load('components/narrated.htm');
    }

    setMapPanToLocation(lat, long, zoom) {
        console.debug('Panning to lat=%s, lng=%s, zoom=%d', lat, long, zoom);
        map.flyTo([lat, long], zoom);
    }
}

var vc = new ViewController();
// vc.viewSelectSubwayLine();
// vc.viewStation('601');
vc.viewSequence();

$('#toggle-overview').on('click', () => {
    console.log('click')
    $('#frontui').toggle();
})
