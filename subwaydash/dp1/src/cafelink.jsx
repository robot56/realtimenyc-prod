var ws = new WebSocket('ws://192.168.1.25:6464');

Status = {
    At: 0,
    Approaching: 1,
    Enroute: 2
}

function cStateUpdate(data) {
    dash.states = {...dash.states, ...data.d};
}

function cHeadwayUpdate(data) {
    dash.headways = {...dash.headways, ...data.d};
}

function cTripsStats(data) {
    dash.trips = {...dash.trips, ...data.d};
}

function cServiceAdvisory(data) {
    // var payload = data.d;

    dash.serviceadvisories = {...dash.serviceadvisories, ...data.advisories};

    // for (var advisory of data.advisories) {
    //     if ()
    // }
}

function cStopStatus(data) {
    dash.onStopsStatusUpdate(data.stops);
}

function cTripStatus(data) {
    dash.onTripStatusUpdate(data);
}

ws.onmessage = function(socketdata) {
    // console.log(socketdata, socketdata.data);
    var data = JSON.parse(socketdata.data);
    if (data.v != 2) {
        console.info('CL *> Unknown protocol version', data.v);
    }

    switch (data.t) {
        case 'STATE_UPDATE': {
            cStateUpdate(data);
            break;
        }
        case 'STATS_HEADWAY_UPDATE': {
            cHeadwayUpdate(data);
            break;
        }
        case 'STOP_STATUS': {
            cStopStatus(data.d);
            break;
        }
        case 'SREVICE_ADVISORY': {
            cServiceAdvisory(data.d);
            break;
        }
        default: {
            console.info('CL -> dropping unknown message type: ', data.t);
        }
    }
}

cStateUpdate({
    'c': 'STATE_UPDATE',
    'd': {
        '4': {
            'grade': 'A',
            'text': 'Good Service'
        }
    }
});

cHeadwayUpdate({
    'c': 'STATS_HEADWAY_UPDATE',
    'd': {
        '4..S': 21
    }
})

cServiceAdvisory({
    'advisories': {
        "1556838374773": {
            'affects': [ '6..1' ],
            'type': '',
            'message': [
                'There are delays and service changes on the [2], [4] and [5] lines because of a switch problem at 149 St-Grand Concourse.'
            ],
            'source': 'twitter',
            'source-attr': '@NYCTSubways',
            'source-time': 12450909,
            'created': Date.now(),
        }
    }
})

cStopStatus({
    'stops': [
        {
            'at': '601N',
            'trips': [
                {
                    'TripID': '06 01111 PEL/BBR',
                    'CurrentStatus': 'approaching',
                    'StopID': '',
                    /* Schedule Data */
                    'GTFSTripID': '0000',
                    'StartDate': 'at',
                    'RouteID': '',
                }
            ]
        }
    ]
});

function sendRequest(messageType, data) {
    try {
        ws.send(JSON.stringify({
            v: 2,
            t: messageType,
            d: data
        }));    
        return true;
    } catch (e) {
        console.warn('failed to send message', messageType, data, e);
        return false;
    }
}

function sendRequestServiceAdvisories(routes) {
    // ws.send(JSON.stringify({
    //     'v': 1,
    //     't': 'REQUEST_SERVICE_ADVISORIES',
    //     'd': {
    //         'routes': routes
    //     }
    // }));
}

function sendRequestStopStatus(gtfsStopId) {
    // cStopStatus({
    //     'stops': [
    //         {
    //             'at': '601N',
    //             'trips': [
    //                 {
    //                     'TripID': '06 01111 PEL/BBR',
    //                     'CurrentStatus': 'approaching',
    //                     'StopID': '',
    //                     /* Schedule Data */
    //                     'GTFSTripID': '0000',
    //                     'StartDate': 'at',
    //                     'RouteID': '',
    //                 }
    //             ]
    //         }
    //     ]
    // });
    
    var stops = gtfsStopId;
    sendRequest('REQUEST_STOP_STATUS', { stops: stops });
    // ws.send(JSON.stringify({
    //     v: 1,
    //     t: 'REQUEST_STOP_STATUS',
    //     d: {
    //         'stops': stops
    //     }
    // }));
}

function sendRequestTripStatus(tripId) {
    sendRequest('REQUEST_TRIP_STATUS', { tripId: tripId });
    cTripStatus({
        'stops': {
            trip: {
                TripID: '',
                CurrentStatus: 0,
                CumulativeArrivalDelay: 0,
                CumulativeDepartureDelay: 0,
                StopID: '400',
                GTFSTripID: '',
                RouteID: '4'
            },
            schedule: [
                {
                    StopID: '401',
                    ArrivalTime: 1,
                    DepartureTime: 2
                }
            ]
        }
    });
}

function requestServiceAdvisories(routes, callback) {
    sendRequestServiceAdvisories(routes);
    dash.addCallbackQueue('SERVICE_ADVISORIES', callback);
}


function requestStopStatus(gtfsStopId, callback) {
    dash.addCallbackQueue('STOP_STATUS', callback);
    sendRequestStopStatus([gtfsStopId + 'N', gtfsStopId + 'S'], callback);
}

function requestTripStatus(tripId, callback) {
    dash.addCallbackQueue('TRIP_STATUS', callback);
    sendRequestTripStatus(tripId);
}

// {
//     'STATS_HEADWAY_UPDATE',
//     // '': {

//     // }
// }


var Temp = {
    'c': 'ROUTE_STATUS',
    'd': {
        'A': {
            'grade': 'A',
            'text': 'Good Service'
        }
    }
};

// GetPathFrom(RouteID, OriginLatLng, Destination);