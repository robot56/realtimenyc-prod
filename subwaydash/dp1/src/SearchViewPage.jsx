class QVSubwayStopCard extends React.Component {

    onClick() {
        setStateViewingStop(this.props.id);
        console.log('VIEWING BSUBWAY STOP', this.props.id)
        update();
    }

    render() {

        var stopName = this.props.name;

        return (
            <div class='search-entry-stop search-entry' onClick={this.onClick.bind(this)}>
                <div class='title'>{stopName}</div>
                <br/>
                <div>Subway station</div>
                <div class='services'>
                {
                    this.props.services.map((v,k) => 
                        <img key={k} class='bullet-lcl' src={SubwayIconSVGMap[v]} />
                    )
                }
                </div>
            </div>
        )
    }
}

class QVSubwayLineCard extends React.Component {
    render() {
        console.log('route', this.props.route);
        var routeLine = SubwayLineData[this.props.route][0];

        return (
            <div class='search-entry-service search-entry'>
                <div class='title'>
                    <img class='bullet-lcl' src={SubwayIconSVGMap[this.props.route]} /> <span>{routeLine}</span>
                </div>
                <br/>
                <div>Subway line</div>
                <p>{this.props.description}</p>
                {/* <p>Service PDF</p> */}
            </div>
        )
    }
}

class QVPage extends React.Component {

    constructor(props) {
        super(props);


        this.state = { results: [] };
        // this.setState( { results: [] } );

        this.update(this.props);
    }

    update(props) {
        var q = props.query;

        this.state.results = [];

        searchAsync(q, (items) => {
            if (!this.didMount)
                this.state = {results: this.state.results.concat(items)};
            else
                this.setState({results: this.state.results.concat(items)});
            // this.state.results = 
            console.info(q, items, this.state.results);
            // console.info(JSON.stringify(this.state.results));
            // this.forceUpdate();
            // console.warn(q);
            // if (q.length>5)
            // debugger;
        })
    }

    componentWillReceiveProps(nextProps) {
        this.update(nextProps);
    }

    render() {
        this.didMount = true;
        return (
            <div>
                <h4>Searching "{this.props.query}"</h4>

                <div class='search-results'>
                    {
                        this.state.results.map((V, k) => {
                            console.log(V, k);
                            var type = V[0];
                            var v = V[1];
                            if (type == 'stop') {
                                return <QVSubwayStopCard key={k} id={v.stopid} name={v.name} services={v.services} />
                            } else if (type == 'service') {
                                return <QVSubwayLineCard key={k} route={v.route} />
                            } else {
                                throw 'error: ' + type
                            }
                        })
                    }
                </div>
            </div>
        )
    }    
}