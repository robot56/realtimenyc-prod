
// Cards 

class CVCardAdvisory extends React.Component {
    render() {
        var type = this.props.type;
        var title = this.props.title;
        var text = this.props.text;
        var attrType = this.props.attrType;

        var className = 'advisory-box';

        if (type == 'advisory') {
            className = 'advisory-box';
        } else if (type == 'alert') {
            className = 'alert-box';
        }

        return (
            <div class={className}>
                <strong>{title}</strong>
                <p>{text}</p>
            </div>
        );
    }
}


class CVTripsDirection extends React.Component {

    onMouseEnter(tripId) {
        console.info('mouse entered', tripId, this);
    }

    render() {

        var name = this.props.name;

        console.warn(this.props.advisories);

        return (
        <div>
            <h5 id="x-direction1" class="x-clearable">{name}</h5>

            <div class='alerts'>
                {
                    this.props.advisories.map((v,k) => {
                        var title = v.title 
                        return <CVCardAdvisory key={k} type="alert" title="Notice" text={v.message[0]} />
                    })
                }
            </div>

            <table class="mdl-data-table mdl-js-data-table cv-direction-table" style={{ backgroundColor: '#00000070'}} >
            <thead>
                <tr>
                <th>Bullet</th>
                <th>Trip ID</th>
                {/* <th>Trip Path</th> */}
                <th>Terminal</th>
                <th>Status</th>
                <th>At / From</th>
                <th>Next</th>
                {/* <th>Cum. Delay</th> */}
                <th>ETA</th>
                </tr>
            </thead>
            <tbody>
                {
                    this.props.trips.map((v,k) =>
                    <tr key={k} class='flashing'
                    onMouseEnter={() => {
                        // console.log('hover', v);
                        dash.mapPanToTripID(v.tripid)
                    }}>
                        <td style={{ textAlign: 'left' }}><img src={SubwayIconSVGMap[v.route]} class='bullet-lcl' /></td>
                        <td style={{ textAlign: 'left' }}>{v.tripid}</td>
                        {/* <td>{v.trippath}</td> */}
                        <td style={{ textAlign: 'left' }}>{v.terminal}</td>
                        <td style={{ textAlign: 'left' }} class='t-status'>{v.status}</td>
                        <td style={{ textAlign: 'left' }}>{v.stop1} ({v.stopindex})</td>
                        <td style={{ textAlign: 'left' }}>{v.stop2}</td>
                        {/* <td></td> */}
                        <td style={{ textAlign: 'left' }}>{v.eta}</td>
                    </tr>
                    )
                }
            </tbody>
            </table>
        </div>
        )    
    }
}

class CVPage extends React.Component {
    constructor(props) {
        super(props);
        // var route = this.props.route;
        var gtfsId = this.props.gtfsId;
        this.gtfsId = gtfsId;
        this.updator = null;
        var route = gtfsId[0];

        this.state = {
            stopName: 'Loading...'
        };
        this.state.directions = [
            {
                'id': '4..S',
                'name': 'Downtown & Brooklyn',
                'advisories': [],
                'trips': [
                    {
                        'route':'4',
                        'tripid': '04 0000 WDL/NLT',
                        'trippath': '06..23S',
                        'status': 'Approaching next',
                        'stop1': '149 St',
                        'stop2': '138 St',
                        'stopindex': '4/6',
                        'eta': '2 min'
                    }
                ]
            },
        ];
        this.state.directions = [
            { advisories: [], trips: [] }, { advisories: [], trips: [] }
        ];

        requestServiceAdvisories([route], this.onServiceAdvisoriesUpdate);
        requestStopStatus(gtfsId, this.onStopStatusUpdate.bind(this));
        this.stopDataRecieve(this.props.gtfsId);
        this.run();
    }

    run() {
        clearInterval(this.updator);
        this.updator = setInterval(() => {
            requestStopStatus(this.gtfsId, this.onStopStatusUpdate.bind(this));
        }, 15 * 1000);
    }

    onServiceAdvisoriesUpdate() {
        console.log('onServiceAdvisoriesUpdate called!')
        for (var direction of this.directions) {
            var advisoryIds = GlobalState.getServiceAdvisoriesFor(direction.id);
            var advisories = [];
            for (var advisoryId of advisoryIds) {
                var advisory = GlobalState.getServiceAdvisory(advisoryId);
            }
            this.directions[direction].advisories = advisories;
        }
        
    }

    onStopStatusUpdate(data) {
        // setInterval(function() {
        //     requestStopStatus(gtfsId, this.onStopStatusUpdate);
        // }, 15 * 1000); // update every 15 seconds
        
        console.log('onStopStatusUpdate(), got ', data);

        var stopN = this.gtfsId + 'N';
        var stopS = this.gtfsId + 'S';

        var stops = [[], []];
        
        var getCurrentStatusStr = function(statusCode) {
            switch (statusCode) {
                case 0:
                    return 'At';
                case 1:
                    return 'Approaching next';
                case 2:
                    return 'Enroute to next';
            }
        }

        var getETAStr = function(ETA) {
            if (ETA == 0xFF) {
                return 'Now';
            }

            var tn = Date.now()/1000;
            var eta_sec = Math.floor(ETA  - tn);

            return `${Math.floor(eta_sec / 60)} min`;
        }

        var i = -1;
        for (var stop of [stopN, stopS]) {
            i++;
            if (data[stop]) {
                var S = data[stop];
                if (!S) continue;
                console.log(S);
                for (var s of S.trips) {
                    var trip = s;
                    if (s.CurrentStatus == 0) {
                        var stop1 = s.StopID;
                        var stop2 = s.NextScheduledStop;
                    } else {
                        var stop1 = s.LastStopID;
                        var stop2 = s.StopID;
                    }
        
                    stops[i].push({
                        route: trip.RouteID,
                        tripid: trip.TripID,
                        terminal: 'TBA',
                        status: getCurrentStatusStr(trip.CurrentStatus),
                        stop1: getStationNameFromGTFSStopID(stop1),
                        stop2: getStationNameFromGTFSStopID(stop2),
                        stopindex: '',
                        eta: getETAStr(s.ETA),
                        ETA: s.ETA
                    })
                }
            }
        }

        stops[0].sort((a, b) => {
            return a.ETA - b.ETA;
        });
        stops[1].sort((a, b) => {
            return a.ETA - b.ETA;
        });

        console.info(stops[0]);
        console.info(stops[1]);

        this.state.directions[0].trips = stops[0];
        this.state.directions[1].trips = stops[1];
        this.setState({})
    }

    stopDataRecieve(stopId) {
        $.getJSON(`http://traintimelb-367443097.us-east-1.elb.amazonaws.com/getTime/${stopId[0]}/` + stopId, (data) => {
            var stationName = data.stationName;
            var direction1Name = data.direction1.name;
            var direction2Name = data.direction2.name;

            this.state.stopName = stationName;

            this.state.directions[0].name = direction1Name;
            this.state.directions[0].id = stopId + 'N';
            

            this.state.directions[1].name = direction2Name;
            this.state.directions[1].id = stopId + 'S';

            // this.state.directions[1] = {
            //     ...this.state.directions[1],
            //     name: direction2Name,
            //     id: stopId + 'S'
            // }

            var stops1 = [];
            var stops2 = [];

            for (var i in data.direction1.times) {
                let stop = data.direction1.times[i];
                stops1.push({
                    route: stop.route,
                    tripid: '',
                    terminal: stop.lastStation,
                    status: '',
                    stop1: '',
                    stop2: '',
                    stopindex: '',
                    eta: `${stop.minutes} min`
                })
            }

            for (var i in data.direction2.times) {
                let stop = data.direction2.times[i];
                stops2.push({
                    route: stop.route,
                    tripid: '',
                    terminal: stop.lastStation,
                    status: '',
                    stop1: '',
                    stop2: '',
                    stopindex: '',
                    eta: `${stop.minutes} min`
                })
            }

            if (!this.state.directions[0].trips.length)
                this.state.directions[0].trips = stops1;
            if (!this.state.directions[1].trips.length)
                this.state.directions[1].trips = stops2;

            if (data.message) {
                if (data.message.messageType != 'SUCCESS') {
                    var message = data.message.message;
                    this.state.directions[0].advisories.push({
                        type: 'alert',
                        message: [message]
                    })    
                }
            }

            this.setState({});
        });
    }

    componentWillReceiveProps(newProps) {
        this.gtfsId = newProps.gtfsId;
        this.state.directions = [
            { advisories: [], trips: [] }, { advisories: [], trips: [] }
        ];

        this.stopDataRecieve(newProps.gtfsId);
        // requestServiceAdvisories([route], this.onServiceAdvisoriesUpdate);
        requestStopStatus(this.gtfsId, this.onStopStatusUpdate.bind(this));
        this.run();
    }

    render() {
        var stopName = this.state.stopName || 'Parkchester';
        
        var lastUpdateTime = '';

        console.info(this.state.directions);

        return (
        <div class="full-margin x-stationdata">
            <h4 id="x-station-name" class="x-clearable">{stopName}</h4>

            {
                this.state.directions.map((v,k) =>
                    <CVTripsDirection key={k} name={v.name} id={v.id} advisories={v.advisories} trips={v.trips}  />
                )
            }

            <hr/>
            <span>Last Updated: </span><span class="x-clearable" id="x-lastupdate">{lastUpdateTime}</span>
            <hr/>
        </div>
        );
    }

    componentDidUpdate() {
        $('#x-lastupdate').text(new Date().toLocaleString());
    }

    componentWillUnmount() {
        clearInterval(this.updator);
    }
}