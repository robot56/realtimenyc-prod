// import * as React from "react";
// import * as ReactDOM from "react-dom";
// throw 1;
class Welcome extends React.Component {
    render() {
      return <h1>Hello, {this.props.name}</h1>;
    }
}

var States = {
    ViewingService: 'ViewingService',
    ViewingLine: 'ViewingLine',
    ViewingTrip: 'ViewingTrip',
    ViewingStop: 'ViewingStop',
    Searching: 'Searching',
    KioskViewingService: 'KioskViewingService',
    LoadingNavBar: 'LoadingNavBar'
}

var state = States.Loading;
var statedata = {};

var statestack = [];

/// COMPONENTS

class ServiceNavigationBarItem extends React.Component {

    onSubwayBulletClick(bullet) {
        // console.log('clicked bullet!');
        if (this.props.service == statedata.service) {
            return; // Same state
        }
        window.onServiceViewBulletClick(this.props.service, '');
    }

    render() {
        return (
        <a href={"#service-cp"} class={`mdl-layout__tab  mdl-js-ripple-effect ${this.props.active ? "is-active" : ''}`} id={"bullet-" + this.props.service} onClick={this.onSubwayBulletClick.bind(this)}>
            {
                this.props.images.map((image,i) => {
                    console.log('rendering ', this.props.service, image, i);
                    return <img key={i} class='bullet-nv' id={`sb-bullet-${i}` + this.props.service} src={image} />
                })
            }
        </a>
        );
    }
}

class ServiceNavigationBar extends React.Component {
    render() {
        statedata.substate = States.LoadingNavBar;
        console.log('rendering nav bar!');
        var services = [
            '1', '2', '3', '4', '5', '6', '7',
            'A', 'C', 'E', 'B', 'D', 'F', 'M',
            'N', 'Q', 'R', 'W', 'G', 'JZ', 'L'
        ];

        return (
            <div style={{display: 'inherit'}}>
                {
                    services.map((service, i) => {
                        return <ServiceNavigationBarItem key={i} service={service} 
                        images={service.length == 1 ? [SubwayIconSVGMap[service]] : [SubwayIconSVGMap[service[0]], SubwayIconSVGMap[service[1]]] } />
                    })
                }
            </div>
        )
    }

    componentDidUpdate() {
        if (this.props.active)
            $('#bullet-' + this.props.active + ' span').click();
    }

    componentDidMount() {
        state = States.ViewingService;
    }
}


/// END COMPONENTS


var Svy

// STATE MANAGER

function setStateViewingService(service) {
    statestack.push({ state: state, data: statedata });
    state = States.ViewingService;
    statedata = { service: service };
}

function setStateViewingStop(stopId) {
    statestack.push({ state: state, data: statedata });
    state = States.ViewingStop;
    statedata = { stop: stopId };
}

function setStateViewingTrip(tripId) {
    statestack.push({ state: state, data: statedata });
    state = States.ViewingTrip;
    statedata = { tripid: tripId };    
}

function setStateSearching(searchQuery) {
    statestack.push({ state: state, data: statedata });
    state = States.Searching;
    statedata = { query: searchQuery };    
}

function backState(amount) {
    if (!statestack.length) return;
    if (!amount) amount = 1;

    if (amount == -1) {
        amount = 0;
        var lastStateState = null;
        for (var i = statestack.length - 1; i >= 0; i--) {
            var se = statestack[i];
            console.log(amount);
            amount++;
            console.log(lastStateState, se);

            if (lastStateState && se.state != lastStateState) {
                break;
            }

            lastStateState = se.state;
        }
        if (amount == (statestack.length - 1)) {
            console.warn('backState(): did not find different state, defaulting back amount to 1.');
            amount = 1;    
        }
    }

    while (amount--) {
        var lastState = statestack.pop();
        if (lastState) {
            state = lastState.state;
            statedata = lastState.data;    
        }    
    }
}

function update() {
    switch (state) {
        case States.ViewingService:
            renderServiceViewPage(statedata.service);
            break;
        case States.ViewingStop:
            renderStopViewPage(statedata.stop);
            break;
        case States.ViewingTrip:
            renderTripViewPage(statedata.tripid);
            break;
        case States.Searching:
            renderSearchViewPage(statedata.query);
            break;
        default:
            console.warn('cannot handle unknown state: ', state);
    }
}

// 

function onServiceViewBulletClick(bullet, bulletLine) {
    if (state != States.ViewingService) {
        console.warn('onServiceViewBulletClick was triggered but not currently viewing any service');
        return;
    }

    console.log('onBulletClick, ', bullet);

    if (bullet == 'JZ')
        bullet = 'J';

    ReactDOM.render(
        <SVPage route={bullet} />,
        document.getElementById('service-cp')
    )

    // renderServiceViewPage();
    setStateViewingService(bullet);
    update();

    // ReactDOM.render(
    //     // <ServiceViewPage route={bullet} />,
    //     // <ServicViewTripsPage route={bullet} />,
    //     // <SubwayServiceViewStatus status={"Good Service"} grade="A" />,
    //     document.getElementById('service-cp')
    // )
}

// ReactDOM.render(
//     <Welcome name="World" />,
//     document.getElementById('message')
// );

function setVisibleNavBar(item) {
    console.warn('visible bar', item);
    $('#title-bar').hide();
    // $('#services-bar').hide();
    $('#x-searchbar').hide();
    $('.mdl-layout__tab-bar-container').hide();

    if (item == 'services-bar') {
        toggleServicesBar(true);
    } else if (item == 'title-bar') {
        toggleTitleBar(true);
    } else if (item == 'search-bar') {
        toggleSearchBar(true);
    } else if (item == null) {
        switch (state) {
            case States.ViewingService:
                toggleServicesBar(true); break;
            case States.Searching:
                toggleSearchBar(true); break;
            default:
                toggleTitleBar(true);
            
        }
    }
}

function toggleServicesBar(visible) {
    document.getElementById('loading-styles').innerHTML = '';
    if (visible) {
        $('.mdl-layout__tab-bar-container').show();
    } else {
        // $('#services-bar').html(`<p style='left:50%;'>Loading...</p>`);
        $('.mdl-layout__tab-bar-container').hide();
    }
}

function toggleTitleBar(visible) {
    if (visible) {
        $('#title-bar').show();
    } else {
        $('#title-bar').hide();
    }
}

function toggleSearchBar(visible) {
    if (visible) {
        $('#x-searchbar').show();
    } else {
        $('#x-searchbar').hide();
    }
}

ReactDOM.render(
    <ServiceNavigationBar />,
    document.getElementById('services-bar')
);    


function renderServiceViewPage(bullet) {
    // state = States.ViewingService;
    if (!bullet) bullet = '1';
    setVisibleNavBar('services-bar');
    setVisiblePage('service-cp');
    ReactDOM.render(
        <ServiceNavigationBar active={bullet} />,
        document.getElementById('services-bar')
    );    

    ReactDOM.render(
        <SVPage route={bullet} />,
        document.getElementById('service-cp')
    )
}

function renderStopViewPage(stopId) {
    // state = States.ViewingStop;
    console.log('STOP VIEW RENDERING', stopId)
    if (!stopId) {
        stopId = '601';
    }
    console.log('STOP VIEW RENDERING', stopId)
    setVisiblePage('stop-cp');
    ReactDOM.render(
        <CVPage gtfsId={stopId} />,
        document.getElementById('stop-cp')
    )
    setVisibleNavBar('title-bar');
}

function renderTripViewPage(tripId) {
    // state = States.ViewingTrip;
    if (!tripId) {
        route = '1';
    }
    setVisibleNavBar('title-bar');
    setVisiblePage('trip-cp');
}

function renderSearchViewPage(searchQuery) {
    // console.info('Search View not yet implemented!');

    if (!searchQuery) {
        console.info('Nothing to search');
        return;
    }

    setVisiblePage('search-cp');
    ReactDOM.render(
        <QVPage query={searchQuery} />,
        document.getElementById('search-cp')
    )
    setVisibleNavBar('search-bar');
}

function setVisiblePage(item) {
    $('.scp').hide();
    $('#' + item).show();
}

$('#x-nav-serviceview').click(function(d) {
    if (state == States.ViewingService) {
        console.log('cannot switch states while already on state');
        return;
    }

    // renderServiceViewPage();
    setStateViewingService('1');
    update();
});

$('#x-nav-stopview').click(function() {
    if (state == States.ViewingStop) {
        console.log('cannot switch states while already on state');
        return;
    }

    // renderStopViewPage();
    setStateViewingStop('601');
    update();
});

$('#x-nav-tripview').click(function() {
    if (state == States.ViewingTrip) {
        console.log('cannot switch states while already on state');
        return;
    }

    // renderTripViewPage();
    setStateViewingTrip(null);
    update();
});

$('#x-back-btn').click(function() {
    backState();
    update();
})

// renderSearchViewPage('parkchester');

// renderServiceViewPage();
setStateViewingService('1');
update();
// renderStopViewPage();