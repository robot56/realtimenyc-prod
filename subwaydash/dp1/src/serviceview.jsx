// Misc Utils

function randomIntFromInterval(min,max) {
    return Math.floor(Math.random()*(max-min+1)+min);
}

// Cards

class SVCardStatus extends React.Component {
    render() {

        var status = this.props.status;
        var grade = this.props.grade;

        return (
        <div class="mdl-card mdl-shadow--2dp card-wide CurrentStatus-card" style={{textAlign:'center'}}>
            <div>
                <span>
                    <h3>{status}</h3>
                    <h3>{grade}</h3>
                </span>
            </div>
        </div>
        );
    }
}

class SVCardHeadwayMonitor extends React.Component {
    render() {
        return (
            <div class="mdl-card card-wide">
            <div class='trips'>
                <iframe id='headway-component' src="components/headway_component.html"></iframe>
            </div>
            </div>
        )
    }
}

class SVCardServiceChangeCurrent extends React.Component {
    render() {

        var id = this.props.id;
        var message = this.props.message;
        var active_for = this.props.active_for;
        var active_until = this.props.active_until;

        return (
        <div class="mdl-card mdl-shadow--2dp card-wide Changes-card active-svc-changes" style={{textAlign:'center',color:'white'}}>
            <h4>Service Change</h4>
            <p><strong>{message}</strong></p>
            <p style={{fontFamily:'Verdana, Geneva, Tahoma, sans-serif'}}>{active_for}</p>
            <p>{active_until}</p>
        </div>
        );
    }
}

class SVCardStatistics extends React.Component {
    render() {
        return (
        <div class="mdl-card mdl-shadow--2dp card-wide Stats-card">
            <div style={{marginTop: '20px'}}>
                <div>
                    <div>Terminal trip time</div>
                    <table style={{textAlign:'center', width: '100%'}}>
                    <tbody>
                    {
                        this.props.terminalTripTime.map((e,k) =>
                        <tr key={k}>
                            <td class='bold'>to {e.name}</td>
                            <td>
                                <span style={{ color: e.color ? e.color : 'lime' }}>{e.eta}</span><br/>
                            </td>
                        </tr>
                        )
                    }
                    </tbody>
                    </table>
                    <br/>
                    <div>Average headway</div>
                    <table style={{textAlign:'center', width: '100%'}}>
                    <tbody>
                    {
                        this.props.averageHeadway.map((e,k) => 
                        <tr key={k}>
                            <td class='bold'>to {e.name}</td>
                            <td>
                                <span style={{ color: e.color ? e.color : 'lime' }}>{e.eta}</span>
                            </td>
                        </tr>
                        )
                    }
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
        );
    }
}

// End cards

class SVTopHeader extends React.Component {
    render() {

        var route = this.props.route;
        var when = this.props.when;

        var _name = SubwayLineData[route][0];
        var _imgSource = SubwayIconSVGMap[route];

        return (
        <div>
            <h4>
                <img class='bullet-lcl' id='cp-service-bullet' src={_imgSource} />&nbsp;
                <span id='cp-service-name'>{_name}</span> &bull; <span style={{color:'lightgray'}}>{when.toLowerCase()}</span>
            </h4>
        </div>
        );
    }
}

// Page Components
class SVIncidentStreamItem extends React.Component {
    render() {

        var label = null;

        if (this.props.type == 'prediction') {
            label = <span class='is-marker is-pred-marker'>Prediction</span>;
        }

        return (
        <div class='entry'>
            <strong>{label} {this.props.text}</strong>
            <p>{this.props.when}</p>
        </div>
        );
    }
}

class SVIncidentStream extends React.Component {
    constructor() {
        super()
    }

    render() {
        return (
    <div id='incident-stream'>
        <div>
            <h5 style={{textAlign: 'center'}}>Incident Stream</h5>
        </div>
        <div class='cont'>
            <SVIncidentStreamItem type='prediction' text="'04 00000 PEL/BBR' cannot proceed to 138 St as '05 0000 PEL/BBR' is enroute on S/B LCL (4) track." when="today" />
        </div>
    </div>
        );
    }
}


class SVTripsTable extends React.Component {
    render() {

        var direction_title = this.props.directionTitle;
        var table_entries = [{
            img: 'https://upload.wikimedia.org/wikipedia/commons/0/0c/NYCS-bull-trans-6d.svg',
            trip_id: '00 0000 SOM/WHR',
            trip_path: 'S..0000',
            trip_status: 'Approaching',
            stop_id_and_index: 'HPA (0/2)',
            si2: 'Withlock Av',
            eta: '3 min'
        }];

        return (
            <div class='xtc-table'>
            <h5>{direction_title}</h5>
            <table>
                <thead>
                    <tr>
                        <th>Bullet</th>
                        <th>Trip ID</th>
                        <th>Trip Path</th>
                        <th>Status</th>
                        <th>At</th>
                        <th>Next</th>
                        <th>Cum. Delay</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        table_entries.map(e =>
                            <tr>
                                <td><img class={ e.img.includes("d.") ? "bullet-exp" : "bullet-lcl" } src={e.img} /></td>
                                <td>{e.trip_id}</td>
                                <td>{e.trip_path}</td>
                                <td>{e.trip_status}</td>
                                <td>{e.stop_id_and_index}</td>
                                <td>{e.si2}</td>
                                <td>{e.eta}</td>
                            </tr>
                        )
                    }
                </tbody>
            </table>
            </div>   
        )
    }
}

// Pages

class SVTripsPage extends React.Component {
    render() {
        var route = this.props.route;
        return (
        <div>
            <SVTopHeader route={route} when="today"></SVTopHeader>
            <div class='trips'>
                <SVTripsTable directionTitle="Downtown &mdash; South Ferry" />
            </div>
        </div>
        );
    }
}


class SVPage extends React.Component {
    
    constructor() {
        super();
    }
    
    render() {
        
        var route = this.props.route;

        var _svc_message = 'Good Service';
        var _svc_grade = 'A';

        var possibleGrades = ['A', 'B', 'C', 'D', 'F'];
        var possibleMessages = ['Good Service ✅', 'Service Change 🚧', 'Delays', 'Significant Delays 🔶', 'Poor Serivce'];

        var randomGrade = 'A';
        var randomMessage = '';
        var random = Math.random() * 10;

        if (random > 7) {
            randomGrade = 'A';
            randomMessage = possibleMessages[0];
        } else if (random > 5) {
            randomGrade = 'B';
            randomMessage = possibleMessages[randomIntFromInterval(1, possibleMessages.length - 1)];
        } else if (random > 4) {
            randomGrade = 'C';
            randomMessage = possibleMessages[randomIntFromInterval(1, possibleMessages.length - 1)];
        } else {
            randomGrade = `F`;
            randomMessage = possibleMessages[randomIntFromInterval(1, possibleMessages.length - 1)];
        }

        return (
        <div>
            <SVTopHeader route={route} when="today"></SVTopHeader>
            <SVIncidentStream />
            <div class='cards' id='x-cards'>
                <SVCardStatus status={randomGrade} grade={randomMessage} />
                <SVCardStatistics terminalTripTime={[ { 'name': 'Pelham Bay Park', eta: '3 min' } ]} averageHeadway={[ {name: 'Brooklyn Bridge', eta: '2 min'} ]} />
                <SVCardHeadwayMonitor />
                <SVCardServiceChangeCurrent
                    message={`Downtown  trains run express from Hunts Point Avenue to 3 Av - 138 St.`}
                    active_for={'January 28st - February 3rd • Weekdays 11am-12:30pm'}
                    active_until={'Today until 3PM'}
                />
            </div>
        </div>
        );
    }
}
// ReactDOM.render(
//     <SVPage />,
//     // <SubwayServiceViewStatus status={"Good Service"} grade="A" />,
//     document.getElementById('x-cards')
// )