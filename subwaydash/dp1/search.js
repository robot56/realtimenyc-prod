function searchAsync(query, entriesCallback) {
    // RTNYC.Subway.stationSearch(query, function(stopid, sname, lat, lng, services) {
    //     entriesCallback('stop', { stopid: stopid, name: sname, lat: lat, lng: lng, services: services });
    // });

    query = query.toUpperCase();


    if (query.length == 1) {
        var routes = '1234567 ACE BDFM NQRW JZ G L';
        if (routes.includes(query)) {
            entriesCallback([['service', { route: query }]]);
        }
    }

    // TODO: Import logic from NYC Now Alexa/Google skills
    // query.replace('WEST ', 'W ').replace('EAST ', 'E ').replace('SOUTH')

    var sresults = RTNYC.Subway.stationSearch(query);

    console.log(sresults)

    var rsresults = [];

    for (var sresult of sresults) {
        rsresults.push(['stop', { stopid: sresult[0], name: sresult[1], lat: sresult[2], lng: sresult[3], services: sresult[4] }]);
    }

    entriesCallback(rsresults);


    console.log(query)
}


var searchBarShown = false;
var searchTimer = null;

function onClickSearch() {
    if (!searchBarShown) {
        searchBarShown = true;
        setVisibleNavBar('search-bar');
    } else {
        searchBarShown = false;
        setVisibleNavBar();
    }
    // $('#x-searchbar').show();
    // $('.mdl-layout__tab-bar-container').hide();
}

$("#fixed-header-drawer-exp").on('input', function (data) {
    var val = $(this).val();
    console.log(val);
    if (val.length == 0) {
        // $('.mdl-layout__tab-bar-container').show();
        // $('#x-searchbar').hide();
        searchBarShown = false;
        setVisibleNavBar();
        backState(-1);
        update();
        return;
    } else {
        setStateSearching(val);
        clearTimeout(searchTimer);
        searchTimer = setTimeout(function() {
            update();
        }, 200);
    }
    setVisibleNavBar('search-bar');

    $('.mdl-layout__tab-bar-container').hide();
    // $('#title-bar').hide();
});

$('#fixed-header-drawer-exp').focusout(function () {
    // $('.mdl-layout__tab-bar-container').show();
    // $('#x-searchbar').hide();
    var val = $(this).val();
    console.log(val);
    if (val.length == 0) {
        searchBarShown = false;
        backState(-1);
        update();
        setVisibleNavBar();
    }
})