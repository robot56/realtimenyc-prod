// var headways = {'6..1': [34]};
// var trips = {'7..1': {'count': 53}};
// var states = {};
// var servicechanges = {
//     '10000': {
//         'text': 'Some Service Change Title',
//         'messageHTML': `<h1>what's up gangsta</h1>`
//     }
// };
// var serviceadvisories = {};
// var callbackQueue = [];

var station_to_latlng = {};
var station_to_name = {};

var l = subwaystationscsv.split("\n");
for (var i in l) {
    var line = l[i].split(",");
    var lat = line[9];
    var lng = line[10];
    station_to_latlng[line[2]] = [lat, lng];
    station_to_name[line[2]] = line[5];
}

function getStationNameFromGTFSStopID(gtfsStopID) {
    _id = gtfsStopID;
    if (!gtfsStopID)
        return gtfsStopID;
    if (gtfsStopID.length == 4) {
        gtfsStopID = gtfsStopID.slice(0,3);
    }
    return station_to_name[gtfsStopID] || _id;
}

class SubwayDash {
    constructor() {
        this.headways = {'6..1': [34]};
        this.trips = {'7..1': {'count': 53}};
        this.states = {};
        this.servicechanges = {
            '10000': {
                'text': 'Some Service Change Title',
                'messageHTML': `<h1>what's up gangsta</h1>`
            }
        };
        this.serviceadvisories = {};
        this.callbackQueue = [];
    }

    getStateFor(route) {

        if (this.states[route] == null) {
            return {
                'grade': '?',
                'text': 'Lost Connection To Server'
            }
        }

        return this.states[route];
    }

    getStatsFor(route) {
        // return {
        //     'ttt': {
        //         'Pelham Bay Park': 3,
        //         'Brooklyn Bridge': 4
        //     },
        //     'headway': {
        //         '6..1': 3,
        //         '6..2': 4
        //     },
        //     'trips': 34
        // }

        var r = {
            'ttt': {
                'Pelham Bay Park': null,
                'Brooklyn Bridge': null
            },
            'headway': {},
            'trips': null
        };

        r.headway = this.headways[route];
        r.trips = this.trips[route];
        
        return r;
    }

    getServiceChangesFor(route) {
        return {
            'active': ['10000', '10001'],
            'scheduled': ['10000']
        };
    }

    getServiceAdvisoriesFor(route) {
        var ids = []
        for (var id in this.serviceadvisories) {
            var serviceadvisory = this.serviceadvisories[id];
            for (var affected of serviceadvisory.affects) {
                if (affected.includes(route)) {
                    ids.push(id);
                }
            }
        }

        return ids;
    }

    getServiceAdvisory(id) {
        return this.serviceadvisories[id];
    }

    addCallbackQueue(type, callback) {
        this.callbackQueue.push({
            'type': type,
            'callback': callback,
            'on': Date.now(),
            'expires': Date.now() + 10 * 100000
        });
        console.log('added callback queue', callback);
    }

    execute(callback_type, data) {
        for (var i in this.callbackQueue) {
            var cq = this.callbackQueue[i];
            console.log(cq.type, callback_type);
            if (cq.type == callback_type) {
                if (cq.expires <  Date.now() || true) {
                    cq.callback(data);
                } else {
                    console.log('cannot call',cq,'because out of date')
                }
                this.callbackQueue.splice(i,1);
                console.log('matched, aborting');
                return;
            }
        }
    }

    onServiceChangeUpdate() {
        this.execute('SERVICE_ADVISORIES');
    }

    onStopsStatusUpdate(data) {
        console.log('onStopStatusUpdate provided', data);
        this.execute('STOP_STATUS', data);
    }

    onTripStatusUpdate(data) {
        console.log('onTripStatusUpdate called', data);
        this.execute('TRIP_STATUS', data);
    }

    mapPanToTripID(tripId) {
        $('#x-mapframe')[0].contentWindow.goToTrip(tripId);
    }
}

var dash = new SubwayDash();