var RDI = {
    getServiceStatus: function(route) {
        var current_statuses = ['majordelays', 'good', 'servicechange'];
        var current_status = current_statuses[Math.round(Math.random() * 2)];
        return {
            current_status: current_status,
            avg_headway: Math.floor(Math.random() * 100),
            route_times: {
                '1..S04R': Math.floor(Math.random() * 100),
                '1..S12R': Math.floor(Math.random() * 100)
            },
            tracked_trips: Math.floor(Math.random() * 100)
        }
    }
}

let lineNames = {"FS": "Franklin Avenue Shuttle", "5X": "Lexington Avenue Express", "E": "8 Avenue Local", "1": "Broadway - 7 Avenue Local", "3": "7 Avenue Express", "2": "7 Avenue Express", "5": "Lexington Avenue Express", "4": "Lexington Avenue Express", "7": "Flushing Local", "6": "Lexington Avenue Local", "7X": "Flushing Express", "A": "8 Avenue Express", "C": "8 Avenue Local", "B": "6 Avenue Express", "GS": "42 St Shuttle", "D": "6 Avenue Express", "G": "Brooklyn-Queens Crosstown", "F": "Queens Blvd Express/ 6 Av Local", "H": "Rockaway Park Shuttle", "J": "Nassau St Local", "M": "QNS BLVD-6th AVE/ Myrtle Local", "L": "14 St-Canarsie Local", "N": "Broadway Local", "Q": "Broadway Express", "R": "Broadway Local", "W": "Broadway Local", "Z": "Nassau St Express", "route_id": "route_long_name", "SI": "Staten Island Railway", "6X": "Lexington Avenue Express"};

var Smash = {

    stateStack: [],

    initialize: function() {

    },

    getPageParamaters: function() {
        let params = {
            positionString: QueryString.getValue('mappos')
        }

        let view = QueryString.getValue('view');
        if (view == 'service') {
            params.view = 'service';
            params.line = QueryString.getValue('line');
        } else if (view == 'line') {
            params.view = 'line';
        } else if (view == 'trip') {
            params.view = 'trip';
        } else if (view == 'map') {
            params.view = 'map';
        } else {
            params.view = 'service';
        }

        let panel = QueryString.getValue('panel');
        if (panel) {
            params.panel = panel;
        } else {
            params.panel = 'dash';
        }

        return params;
    },


    load: function() {
        let params = this.getPageParamaters();

        switch (params.view) {
            case 'service':
                this.openServiceView(params.line, params.panel);
                break;
            case 'line':
                this.openLineView();
                break;
            case 'trip':
                this.openTripView();
                break;
            case 'map':
                this.openMapView();
                break;
        }
    },

    openMapGroup: function() {
        $('.content-group').hide();
        $('.content-group-map').show();
        $('.scp').hide();
    },

    openServiceView: function(line, panel) {
        this.openMapGroup();
        $('.cards').hide();
        $('.trips').hide();
        $('.service-cp').show();
        if (panel == 'trips') {
            $('.trips').show();
        } else {
            Smash.serviceViewLineDash(line);
            $('.cards').show(); 
        }
    },

    openLineView: function() {
        this.openMapGroup();
        $('.line-cp').show();
    },

    openTripView: function() {
        this.openMapGroup();
        $('.trip-cp').show();
    },

    openMapView: function() {
        $('.content-page').hide();
        $('.map-cp').show();
    },

    serviceViewLineDash: function(line) {
        $('.service-cp').hide();
        let svcStatus = RDI.getServiceStatus(line);

        let icon = RTNYC.Subway.getSubwayIconURL(line);

        $('#cp-service-bullet').attr('src', icon);
        $('#cp-service-name').text(lineNames[line]);

        let _headway = svcStatus.avg_headway;
        let _trackedCount = svcStatus.tracked_trips;
        let _routehtml = '';

        for (var route in svcStatus.route_times) {
            let routeTime = svcStatus.route_times[route];
            _routehtml += ` <strong>${routeTime} minutes</strong>,`;
        }

        if (_routehtml.length) {
            _routehtml.slice(0, -1);
        }

        switch (svcStatus.current_status) {
            case 'good':
                var _status = 'Good Service ✅';
                break;
            case 'servicechange':
                var _status = 'Service Change 🚧';
                break;
            case 'majordelays':
                var _status = 'Major Delays 🔶';
                break;
            default:
                var _status = '❔ Unknown Status';
        }
        $('#overview-card-title').text(_status);

        $('#overview-card-text').html(`
        <ul>
            <li>Trains running every <strong>${_headway} minutes</strong> on average</li>
            <li>✔ No active service changes 🚧</li>
            <li>Terminal-to-terminal past hour average trip time is ${_routehtml}</li>
        </ul>`);

        $('#stats-card-tracked').text(`${_trackedCount} tracked trips`);
        $('.service-cp').show();
        // setTimeout(function() {
        //     $('.service-cp').show();
        // }, 1000);
    },

    serviceViewLineOverview: function() {
        QueryString.setValue('line', line);
        QueryString.setValue('panel', 'overview');
    },

    serviceViewLineTracked: function() {

    }
}

Smash.initialize();
Smash.load();

window.addEventListener('hashchange', function() {
    Smash.load();
})

function bulletClick(bullet, group) {
    QueryString.setValue('line', bullet);
}

function openServiceOverview() {
    Smash.service
}

function openServiceOverview() {

}

$(function() {
    $('.scp').addClass('animated slideInDown');

    setTimeout(function() {
        // this is a very expensive frame to embed so we delay loading it to allow other content to load first
        $('#x-mapframe').attr('src', "../../transitmaps/subway/index.html?embed=2&2");
    }, 2000);
});

var ws = new WebSocket('ws://127.0.0.1:6464');

ws.onmessage = function(data) {
    console.log(data);
    var d = JSON.parse(data.data);
    if (d.t == 'STATS_HEADWAY_UPDATE') {
        console.log(d.d['4..S']);
        $('#headway-component').onStatsHeadwayUpdate(d.d['4..S']);
    }
}