var map = tm.mapController.map;

var darkMode = true;

L.easyButton('fa-sun-o', function(btn, map){
    // var antarctica = [-77,70];
    // map.setView(antarctica);
    darkMode = !darkMode;
    console.log('flipping!');
    document.documentElement.style.setProperty('--fg-text-color', darkMode ? 'white' : 'black');
    document.documentElement.style.setProperty('--bg-color', darkMode ? 'black' : 'white');
    if (darkMode) {
        tm.setMapTileServer(0);
    } else {
        tm.setMapTileServer(1);
    }
}).addTo(map);