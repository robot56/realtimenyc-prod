var $LogVerbose = function() {} // empty function, no action
var $LogDebug = () => {};

function serviceIdToRouteId(bullet) {
    bullet = bullet.replace("X", "")
    if ("123".includes(bullet)) return "1";
    if ("456".includes(bullet)) return "4";
    if ("7".includes(bullet)) return "7";
    if ("ACE".includes(bullet)) return "A";
    if ("BDFM".includes(bullet)) return "B";
    if ("NQRW".includes(bullet)) return "N";
    if ("JMZ".includes(bullet)) return "J";
    return bullet;
}

function distanceBetween(lat1, lng1, lat2, lng2) {
    var s = Math.sqrt(
        (lat2 - lat1)**2 + (lng2 - lng1)**2
    );
    $LogVerbose('distanceBetween (%s, %s) (%s, %s): ' + s, lat1, lng1, lat2, lng2);
    return s;
}

// unstupidify long,lat to lat,lng array
function longLatToLatLngArr(arrin, from_station_id, to_station_id) {
    var newarr = []
    //console.debug('Correcting array: ' + arrin.toString())
    for (let i in arrin) {
        let long = arrin[i][0];
        let lat = arrin[i][1];
        newarr.push([lat, long]);
    }

    if (from_station_id != undefined) {
        var t = RTNYC.Subway.getStopIDLatLng(from_station_id);
        if (t == undefined) {
            console.log('llll: Did not find station data1 for %s -- aborting', from_station_id);
            return newarr;
        }
        var t2 = RTNYC.Subway.getStopIDLatLng(to_station_id);
        if (t2 == undefined) {
            console.log('lll: Did not find station data2 for %s -- aborting', to_station_id);
            return newarr;
        }
        var lat = t[0];
        var lng = t[1];
        var lat2 = t2[0];
        var lng2 = t2[1];
        // this below may not be 100% accurate, look into it
        // breaks with 2/3 manhattan-brooklyn train tunnel
        if (distanceBetween(newarr[0][0], newarr[0][1], lat, lng) > 
            distanceBetween(newarr[0][0], newarr[0][1], lat2, lng2)) {
            $LogVerbose('llll: Need to reverse')
            newarr.reverse();
        } else {
            // alert('no need to reverse');
        }
    }

    return newarr;
}

function polyMerge(polys, fallback_max_distance, max_distance) {
    $LogVerbose('polyMerge: merging:', polys);
    //polys = JSON.parse(JSON.stringify($polys));
    //console.warn(JSON.stringify(polys[1]))
    if (max_distance == null) 
        var max_distance = 0.0005;
    let first_latlng = 0;
    var polyarray_out = [];
    for (let i in polys) {
        // holy ---- this single line below was causing such weirdness on Chrome
        // because `polys` was being passed by reference, somehow the code below was
        // modifying the data of `polys` causing quite a bit of odd behavior in the code
        // where if you called animatedPathFinder() twice on the same data, it would fail
        // the second try for no clear reason...
        // `slice` solution to copy per https://stackoverflow.com/a/21761178
        if (polys[i] == undefined) continue;
        let poly = polys[i].slice(0);
        let first_point = poly[0];
        let last_point = poly[poly.length - 1];
        if (first_point == undefined || last_point == undefined) continue;

        // console.debug('PM: first_point: lat=%s, lng=%s', first_point[0], first_point[1]);
        // console.debug('PM: last_point: lat=%s, lng=%s', last_point[0], last_point[1]);

        if (polyarray_out.length == 0) {
            //console.debug('pushing')
            polyarray_out = poly;
            continue;
        }

        // console.log('polyarray_out', polyarray_out);
        
        let fl = polyarray_out[0];
        let ll = polyarray_out[polyarray_out.length - 1]
        
        let distance_first_first = distanceBetween(fl[0], fl[1], first_point[0], first_point[1]);
        let distance_first_last = distanceBetween(fl[0], fl[1], last_point[0], last_point[1])
        let distance_last_first = distanceBetween(ll[0], ll[1] /* output last */, 
            first_point[0], first_point[1]);
        let distance_last_last = distanceBetween(ll[0], ll[1], last_point[0], last_point[1]);

        var distances = [distance_first_first + 0.00001, distance_first_last + 0.00001, 
            distance_last_first + 0.00001, distance_last_last + 0.00001];
        if (fallback_max_distance == undefined)
            fallback_max_distance = Math.min(...distances)
        else  
            Math.min(fallback_max_distance, Math.min(...distances));

        if (distance_first_first < max_distance) {
            // // no idea how to even deal with this!
            // // we have to reverse the array, presumably
            // console.warn('This should never be hit')
            // console.info(fl)
            // console.info(first_point)
            // //alert('distanceBetween(1) fail');
            
            // below removes duplicate first entry if it exists to smooth transitions
            $LogVerbose('polyMerge: will shift and prepend')
            if (fl[0] == first_point[0] && fl[1] == first_point[1]) poly.shift();
            poly.reverse()
            polyarray_out = poly.concat(polyarray_out);
        } else if (distance_first_last < max_distance) {
            $LogVerbose('polyMerge: will prepend')
            polyarray_out = poly.concat(polyarray_out);
            //console.debug(polyarray_out)
        } else if (distance_last_first < max_distance /* matches first point of this */) {
            $LogVerbose('polyMerge: will append');
            polyarray_out = polyarray_out.concat(poly);
            //console.debug(polyarray_out);
        } else if (distance_last_last < max_distance) {
            $LogVerbose('polyMerge: will reverse and append'); // TODO: should we shift here?
            poly.reverse()
            polyarray_out = polyarray_out.concat(poly)
        } else {
            if (fallback_max_distance != undefined && fallback_max_distance != max_distance) {
                console.warn('polymerge is running itself again!')
                let ret = polyMerge(polys, undefined, fallback_max_distance);
                return ret;
            }
            console.error('polyMerge: Fell through for (%s) (%s) -- undefined behavior ' + max_distance, fl.toString(), ll.toString())
            //throw 'merge error'
        }

        $LogVerbose(poly)
        $LogVerbose('polyMerge: CurrentIteration 1st point: (%s, %s), last pt: (%s, %s)', 
            first_point[0].toFixed(5), first_point[1].toFixed(5), last_point[0].toFixed(5), last_point[1].toFixed(5))
    }
    // console.debug(polyarray_out)
    return polyarray_out;
}

function pathFind(from_stop_id, to_stop_id, max_recusion, max_path_size) {
    if (max_recusion == undefined) var max_recusion = 200;
    let findConnectingStops = function(stop) {
        let branches = [];
        for (var i in polystop_map) {
            let item = polystop_map[i];
            if (item.stop1 == stop || item.stop2 == stop) {
                let s2 = (item.stop1 == stop) ? item.stop2 : item.stop1;
                if (branches.includes(s2))
                    continue;
                else
                    branches.push(s2);
            }
        }
        return branches;
    };

    let possible_routes = findConnectingStops(from_stop_id);

    $LogDebug(possible_routes);
    if (possible_routes.includes(to_stop_id)) {
        return [from_stop_id, to_stop_id];
    }
    let runs_allowed = max_recusion;
    var m = [];
    var completed_paths = possible_routes;
    var max_path_size = max_path_size == undefined ? 8 : max_path_size;
    $LogDebug('looking for stop ' + to_stop_id + ' from ' + from_stop_id + ' - ')
    function find(laststop, routes, path, visited_paths) {
        if (visited_paths == undefined) visited_paths = [from_stop_id];
        $LogDebug(routes)
        var path_before_route = path//.slice(0)
        var visited_paths_before = visited_paths//.slice(0)
        // PATCH: Sort routes to prefer current line
        var _routes1 = [];
        var _routes2 = [];
        for (var routei in routes) {
            let _route = routes[routei];
            if (!_route) { // sometimes 'route' can be undefined...
                _routes1 = routes;
                _routes2 = [];
                break;
            }
            if (_route[0] == from_stop_id[0]) {
                _routes1.push(_route);
            } else {
                _routes2.push(_route);
            }
        }
        if (_routes1.concat(_routes2) != routes) {
            $LogDebug('SWAPPING!', _routes1.concat(_routes2), routes);
        }
        routes = _routes1.concat(_routes2);
        // ENDPATCH
        for (let i in routes) {
            let route = routes[i];
            let pr = findConnectingStops(route);
            pr = pr.filter(function(item) {
                return !visited_paths.includes(item);
            });
            if (pr.includes(to_stop_id)) {
                m = path.concat([[laststop, route],[route, to_stop_id]])
                $LogDebug("MATCH route=%s, pr=%s, path=%s", route, pr.toString(), path.toString())
                return true;
            } else {
                runs_allowed--;
                if (runs_allowed == 0) {
                    console.warn('pathFind(): too much runs, giving up')
                    return false;
                }
                if (path.length > max_path_size) {
//                    path = path_before_route;
//                    visited_paths = visited_paths_before;
                    visited_paths.push(route)
                    //console.debug('plen kill '+path_before_route>max_path_size)
                    //console.debug('blocked: ' + visited_paths.toString())
                    continue;
                }
                //console.debug('visited ' + route)
//                visited_paths = visited_paths_before;
                visited_paths.push(route);
                if (pr.length == 0) continue;
                //console.debug("route=%s, pr=%s, path=%s", route, pr.toString(), path.toString())
                let ret = find(route, pr, path.concat([[laststop, route]]), visited_paths);
                if (ret) return true;
            }
        }
        return false;
        //completed_paths.concat(visited_paths);
        //throw completed_paths;
    }

    let t = find(from_stop_id, possible_routes, []);
    //console.clear()
    $LogDebug("find(): ");
    $LogDebug(t)
    $LogDebug('Computed path: %s', m.toString())
    // DEBUG CODE BELOW
    // var pb =  [];
    // for (var i of m) {
    //     pb.push([Number(gtfsToStationId(i[0])), Number(gtfsToStationId(i[1]))]);
    //     // pb.push(gtfsToStationId(i[1]));
    // }
    // console.warn('Computed path (SIDs): ', pb);
    return m;
}

function findConnectingPolysRecursive(from_stop_id, to_stop_id, max_recusion, max_path_len) {
    let origin_stop = from_stop_id;

    let initial_try = polyFindingMagic(from_stop_id, to_stop_id);

    if (initial_try)
        return initial_try;

    let found_path = pathFind(from_stop_id, to_stop_id, max_recusion, max_path_len);
    
    let _foundroutes = [];
    for (var k in found_path) {
        let tk = found_path[k]
        let from = tk[0]
        let to = tk[1]
        $LogVerbose("k=%s, tk=%s, from=%s, to=%s", k, tk, from, to)
        let pfm = polyFindingMagic(from, to, null);
        //let pfm2 = polyFindingMagic(from, to, null);
        //console.log(JSON.stringify(pfm))
        //console.log(JSON.stringify(pfm2))
        //console.assert(JSON.stringify(pfm) == JSON.stringify(pfm2))
        //let pm1 = polyMerge([_foundroutes, pfm])
        //let pm2 = polyMerge([_foundroutes, pfm])
        //console.log(JSON.stringify(pm1));
        //console.log(JSON.stringify(pm2));
        //console.assert(JSON.stringify(pm1)===JSON.stringify(pm2));
        _foundroutes = polyMerge([_foundroutes, pfm], 0.005);
        //_foundroutes = pfm.concat(_foundroutes);
        // oh js...

        // console.info('FR', k, _foundroutes);
    }
    //console.assert(_foundroutes.length > 0)
    $LogDebug(_foundroutes)

    if (_foundroutes.length)
        return _foundroutes;
    else
        return null;
}

function polyFindingMagic(from_stop_id, to_stop_id, linez) {
    var relevant_services = serviceIdToRouteId(from_stop_id[0]) + serviceIdToRouteId(to_stop_id[0])
    $LogDebug("polyFind: from %s to %s -- services: %s", from_stop_id, to_stop_id, relevant_services)   

    let polyline_arr = [];
    let found = false;
    let found_lines = [];

    for (var i in polystop_map) {
        let item = polystop_map[i];
        if (item.stop1 == from_stop_id || item.stop2 == from_stop_id) {
            let s1 = (item.stop1 == from_stop_id) ? item.stop1 : item.stop2;
            let s2 = (item.stop1 == from_stop_id) ? item.stop2 : item.stop1;
            if (item.stop1 == to_stop_id || item.stop2 == to_stop_id) {      
                for (var j in item.polys) {
                    let poly = item.polys[j];
                    let s = poly.split(".");
                    let polyObjId = s[1].replace("\*", "");

                    let match = gSubwayLinesGeoJsonMapped[polyObjId];

                    if (match == undefined) {
                        console.warn('Did not find poly feature for objid %s -- undefined behavior!', polyObjId)
                        return null;
                    }

                    let rts = !linez ? relevant_services : linez;

                    if (rts.includes(match.properties.rt_symbol) || item.ignorelines) {
                        let polyline = match.geometry.coordinates;
                        $LogDebug('polyFind: found match for object id %s:', polyObjId)
                        $LogDebug(polyline)
                        if (found && polyline_arr.toString() == polyline.toString()) {
                            console.error("POLYLINE DUPLICATE, Remove from polystop_map: " + poly);
                            continue;
                        }
                        polyline_arr.push(polyline);
                        found = true;
                        //break;
                    } else {
                        $LogDebug('polyFind: found match for object id %s:', poly)
                        console.debug('polyFind: svc match fail ' + linez + ' ' + match.properties.rt_symbol)
                        $LogDebug(match)
                        found_lines.push(match.properties.rt_symbol)
                    }
                }
                

                // let poly = item.polys[0];
                // let s = poly.split(".");
                // let polyObjId = s[1].replace("\*", "");
                // console.log('found initial and dest requested stop id in %s', JSON.stringify(item));
                // // for (var j in gSubwayLinesGeoJsonMapped) {
                // //     let feature_data = gSubwayLinesGeoJsonMapped[j];
                // // }
                // if (gSubwayLinesGeoJsonMapped[polyObjId] == undefined) {
                //     console.log('did not find poly feature for objid')
                // } else {
                //     console.log('all is well, found')
                //     console.log(gSubwayLinesGeoJsonMapped[polyObjId].geometry.coordinates)
                //     return gSubwayLinesGeoJsonMapped[polyObjId];
                // }
            }
        }
    }

    if (found) {
        console.assert(polyline_arr.length > 0)
        let out = polyMerge(polyline_arr, 0.005)
        return out;
    } else {
        if (found_lines.length > 0) {
            console.warn('no match, but found lines, will try again with first of the following (if not already tried):')
            console.warn(found_lines);
            let first = found_lines[0];
            if (line != first) {
                let ret = polyFindingMagic(from_stop_id, to_stop_id, first);
                if (ret != null)
                    return ret;
                console.error('failed polyFindingMagic 2nd try')
                console.log(ret)
            }
        }
    }
    
    $LogDebug('FAILED');
    return null;
}

function animatedPathFind(from_stop, to_stop, route, speed, maxrecursion, maxpathlen) {
    let polys = findConnectingPolysRecursive(from_stop, to_stop, maxrecursion, maxpathlen);
    if (!polys) return null;
    let coords = longLatToLatLngArr(polys, from_stop, to_stop);
    let mm3 = L.Marker.movingMarker(coords, speed * 1000, {icon: getIconFor(route == undefined ? "A" : route)}).addTo(map);
    mm3.setPercentageCompletion(0.85);
    mm3.start();
    return mm3;
}