// DECLS

// maputil.js decl
declare function findClosestSequentialLLIndex(latlng_array, latlng);

var gEntities = [];

declare var L: any;
declare var RTNYC: any;

declare function getColorForRoute(route_id: string): any;
declare function getLineNameForRoute(route_id: string): any;

declare function currentUnixTS(): number;

// pathfind.js

declare function findConnectingPolysRecursive(from_stop_id, to_stop_id, max_recusion, max_path_len);
declare function longLatToLatLngArr(arrin, from_station_id, to_station_id);

// END DECLS

var trainIcon6 = L.icon({
    iconUrl: 'http://subwaytime.mta.info/img/6.png',
    iconSize: [32, 32], // size of the icon
});

enum TripStatus {
    Approaching,
    At,
    Enroute
};

class AnimatableEntity {
    marker: any;
    public tripId: string;

    constructor(latlngs, duration) {
        // lat latlngs = positions || null;
        this.marker = L.Marker.movingMarker(latlngs, duration);
    }

    addTo(map) {
        this.marker.addTo(map);
    }

    removeFrom(map) {
        map.removeLayer(this.marker);
    }

    _animate(latlng_points: any[], arrival_time: number) {
        console.debug('_animate: alls goods bruh', latlng_points);
        let currentTime = currentUnixTS();
        let tdelta = arrival_time - currentTime;
        console.debug(tdelta * 1000);
        this.marker.initialize(latlng_points, tdelta * 1000, {icon: trainIcon6});
        this.marker.start();
    }

    // if restartIfBusy == false, either hold or speed up current action
    // by stopping current motion, and creating new motion based on
    // the new arrival time. IF we alre already at destination, we always reset.
    animate(latlng_points: any[], arrival_time: number, restartIfBusy: boolean) {
        let tj = [latlng_points[0], latlng_points[latlng_points.length - 1]];
        if (this.isBusy()) {
            let cj = this.getCurrentJob();
            if (!cj) {
                console.debug('animate: is busy, but no positions so overwriting')
                this._animate(latlng_points, arrival_time);
            } else { // have a current job
                console.debug('animate: have a current job')
                if (restartIfBusy) {
                    this._animate(latlng_points, arrival_time);
                } else {
                    this.hold();
                    if (cj == tj) { // We are re-doing the same job, just with a different AT
                        console.debug('animate: We are re-doing the same job, just with a different AT')
                        let cp = this.getCurrentPosition();
                        let closestPointIndex = findClosestSequentialLLIndex(latlng_points, cp);
                        let na = latlng_points.slice(closestPointIndex);
                        this._animate(na /* new arr from current pos */, arrival_time);    
                    } else { // have a current job, BUT is different so overwrite
                        console.debug('animate: have a current job, BUT is different so overwrite')
                        this._animate(latlng_points, arrival_time);
                    }
                }
            }
        } else {
            console.debug('animate: is not busy, running action');
            this._animate(latlng_points, arrival_time);
        }
    }

    getCurrentPosition() {
        return this.marker.getLatLng();
    }

    hold() {
        this.marker.pause();
    }

    isBusy(): boolean {
        return this.marker.isRunning();
    }

    // Check where we are currently animating between
    getCurrentJob() {
        let lls = this.marker._latlngs;
        if (lls.length < 2) {
            return null;
        }
        return [ lls[0], lls[lls.length - 1] ];
    }
}

class SubwayTrainMarker extends AnimatableEntity {

    public fromStopId: string;
    public fromStopTime: number;
    public toStopId: string;
    public toStopTime: number;
    public status: TripStatus;

    constructor(initialPosition) {
        super([initialPosition], 0);
    }

}

class AnimatableEntityController {
    entities: AnimatableEntity[];
    entitescount: 0;

    constructor() {
        this.entities = [];
        this.entitescount = 0;
    }

    choreograph(id, latlng_points, arrival_time) {
        let entity = this.entities[id];
        entity.animate(latlng_points, arrival_time, false);
    }

    // @type 
    addEntity(entity: AnimatableEntity) {
        this.entities[this.entitescount] = entity;
        return this.entitescount++;
    }

    removeEntity(entityid: number) {
        delete this.entities[entityid];
    }

    getEntity(entityid: number) {
        return this.entities[entityid];
    }

    start() {

    }

    stop() {

    }

    cleanup(beforeTs) {

    }
}

function onSubwayRouteHighlight() {

}

function onSubwayRouteUnhighlight() {

}

// Leaflet Map Controller. Ideally we can swap this out for another
// class if we wanted to switch map backend & keep other logic intact.
class MapController {
    entityController: AnimatableEntityController;

    persistentPolys: any;
    secondaryPolys: any[];

    secondaryPolysVisibiliyZoomLevel = 14;

    map: any;

    tile: any;

    constructor(mapid, initialZoomPosition, initialZoomLevel) {
        this.entityController = new AnimatableEntityController();
        this.map = L.map(mapid, {
            // markerZoomAnimation: false
        });
        this.map.setView(initialZoomPosition, initialZoomLevel);

        this.persistentPolys = L.layerGroup();
        this.showPersistentServiceRoutes();
    }

    initializeWithProps(props) {
        this._setTileServer(props.tileserver);
    }

    _setTileServer(tileserver) {
        this.tile = L.tileLayer(tileserver, {
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>',
            subdomains: 'abcd',
            maxZoom: 19
        });

        this.map.addLayer(this.tile);
    }

    _removeTileServer() {
        this.map.removeLayer(this.tile);
    }

    // Map Options
    mapPanToLocation(lat, lng) {
        // this.map.
    }
    //

    addPersistentServiceRoute(layer) {
        layer.addTo(this.persistentPolys);
    }

    addSecondaryServiceRoute(layer) {
        this.secondaryPolys.push(layer);
    }

    emptyPersistentServiceRoutes() {
        this.map.removeLayer(this.persistentPolys)
        this.persistentPolys = L.layerGroup();
        this.persistentPolys.addTo(this.map);
    }

    hidePersistentServiceRoutes() {
        this.map.removeLayer(this.persistentPolys);
    }

    showPersistentServiceRoutes() {
        this.map.addLayer(this.persistentPolys);
    }

    //

    addSubwayTrain(latlng, marker: SubwayTrainMarker): Number {
        var eid = this.entityController.addEntity(marker);
        marker.addTo(this.map);
        return eid;
    }

    addBusVehicle() {

    }

    addGenericEntity() {

    }

    getEntity(id) {
        return this.entityController.getEntity(id);
    }

    removeEntity(id) {
        let entity = this.getEntity(id);
        this.map.removeLayer(entity);
        this.entityController.removeEntity(id);
    }
}

class GoogleMapController {

}

class AppleMapController {

}

var subwayMappedGeoJson: any;

class TransitMap {
    subwayTripIDToEntityID = {};
    busVehicleIDToEntityID = {};

    mapController: MapController;

    tileservers = [
        "https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png",
        "http://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png",
        "http://cartodb-basemaps-{s}.global.ssl.fastly.net/rastertiles/voyager/{z}/{x}/{y}.png"
    ];

    constructor(initMode) {
        // this.initMap();
    }

    startDefault(mapId: string, pos: [number, number], zoom?: number, tileserverId?: number) {
        this.mapController = new MapController(mapId, pos, zoom);

        this.mapController.initializeWithProps({
            'tileserver': this.tileservers[tileserverId || 0]
        });
    }

    startEmbed(mapId, pos, zoom) {

    }

    setMapTileServer(index) {
        this.mapController._removeTileServer();
        this.mapController._setTileServer(this.tileservers[index]);
    }

    addSubwayRoute(features, styles, lineDescription) {

        //TODO: make code below run Controller.loadServiceRouteFromGeoJson();

        var geoj_layer = L.geoJson(features, {style: styles});
        // geoj_layer.addTo(this.mapController.map);
        geoj_layer.on('mouseover', onSubwayRouteHighlight).on('mouseout', onSubwayRouteUnhighlight);
        geoj_layer.bindTooltip(lineDescription, {sticky: true, direction: 'auto'});
    
        this.mapController.addPersistentServiceRoute(geoj_layer);
    }

    addBusRoute() {

    }

    addSubwayTrip(tripid, tripstate, position) {
        if (!position) {
            position = RTNYC.Subway.getStopIDLatLng(tripstate.from_stop_id);
            if (!position) {
                position = RTNYC.Subway.getStopIDLatLng(tripstate.to_stop_id);
            }
        }

        console.log('Stop ID is', tripstate.from_stop_id || tripstate.to_stop_time, 'Position is ', position);

        let trip = new SubwayTrainMarker(position);
        trip.tripId = tripid;
        trip.fromStopId = tripstate.from_stop_id;
        trip.fromStopTime = tripstate.from_stop_time;
        trip.status = tripstate.status;
        trip.toStopId = tripstate.to_stop_id;
        trip.toStopTime = tripstate.to_stop_time;

        this.subwayTripIDToEntityID[tripid] =  this.mapController.addSubwayTrain(position, trip);
        return trip;
    }

    // IF tripstatus is At, to_station_id is the next stop
    // IF tripstatus is enroute/approaching to_station_id is the next stop
    private _triggerSubwayMove(trip_id, from_station_id: string, to_station_id: string, tripstatus: TripStatus, from_time: number, to_time: number) {
        let tripindex = this.subwayTripIDToEntityID[trip_id];
        let trip = this.mapController.getEntity(tripindex);
        
        if (tripindex == undefined) {
            return false;
        }

        if (!(from_station_id && to_station_id)) {
            console.debug('_triggerSubwayMove: ignoring b/c missing both from_station_id & to_station_id');
            return;
        }

        let polys = findConnectingPolysRecursive(from_station_id, to_station_id, 200, 100);
        console.assert(polys.length);
        let coords = longLatToLatLngArr(polys, from_station_id, to_station_id);
        
        // let mm3 = L.Marker.movingMarker(coords, speed * 1000, {icon: getIconFor(route == undefined ? "A" : route)}).addTo(map);
        // mm3.setPercentageCompletion(0.85);
        // mm3.start();
        // return mm3;    

        if (to_time) {
            this.mapController.entityController.choreograph(tripindex, coords, to_time);
            console.debug("_triggerSubwayMove: choreo'd")
        } else {
            console.debug('_triggerSubwayMove: must move to dest. b/c no to_time');
        }
    }

    updateSubwayTrip(trip_id, tripstate) {
        let tripindex = this.subwayTripIDToEntityID[trip_id];
        let trip = this.mapController.getEntity(tripindex) as SubwayTrainMarker;
        if (!trip) {
            console.debug('updateSubwayTrip: ignoring: not activly tracking trip id ' + trip_id);
            return; // no such trip
        }
        if ((tripstate.from_stop_id != trip.fromStopId) || (tripstate.to_stop_id != trip.toStopId)) {
            console.log('updateSubwayTrip: Change in from_stop_id/to_stop_id')
            this._triggerSubwayMove(trip_id, tripstate.from_stop_id, tripstate.to_stop_id, tripstate.status, null, tripstate.to_stop_time);
        } else if ((tripstate.from_stop_time != trip.fromStopTime) || (tripstate.to_stop_time != trip.toStopTime)) {
            console.log('updateSubwayTrip: Change in from_stop_time/to_stop_time')
            if (!tripstate.to_stop_time) {
                console.debug('updateSubwayTrip: not a valid to stop time: ', tripstate.to_stop_time);
            }
            this._triggerSubwayMove(trip_id, tripstate.from_stop_id, tripstate.to_stop_id, tripstate.status, null, tripstate.to_stop_time);
        }
    }

    //

    loadSubwayRoutes() {
        for (var subwayroute in subwayMappedGeoJson) {
            var styles = {
                "color": getColorForRoute(subwayroute),
            };
            var features = subwayMappedGeoJson[subwayroute];
            var parsed_features_list = [];
            for (var i in features) {
                var feature = features[i];
                //console.log(feature)
            }

            this.addSubwayRoute(features, styles, getLineNameForRoute(subwayroute));
        }
    }

    loadSubwayRoute(subwayroute) {
        var styles = {
            "color": getColorForRoute(subwayroute),
        };
        
        var features = subwayMappedGeoJson[subwayroute];
        var parsed_features_list = [];
        for (var i in features) {
            var feature = features[i];
            //console.log(feature)
        }

        this.addSubwayRoute(features, styles, getLineNameForRoute(subwayroute));
    }

    unloadSubwayRoutes() {
        this.mapController.emptyPersistentServiceRoutes();
    }

    hideSubwayRoutes() {
        this.mapController.hidePersistentServiceRoutes();
    }
}

// var subwayTripID

// function getInternalVehicleID() {

// }

var tm = new TransitMap(null);
tm.startDefault('map', [40.740, -73.950], 12, 1);
tm.loadSubwayRoutes();
tm.addSubwayTrip('1', { from_stop_id: '601' }, null);
tm.updateSubwayTrip('1', { from_stop_id: '601', to_stop_id: '621', to_stop_time: currentUnixTS() + 10 });
// tm.addSubwayTrip('0', {  }, [40.83248, -73.86327]);