/// <reference path="flatbuffers.d.ts"/>
/// <reference path="sess_generated.ts"/>

var trainIcon6X = L.icon({
    iconUrl: 'https://upload.wikimedia.org/wikipedia/commons/0/0c/NYCS-bull-trans-6d.svg',
    iconSize: [34, 34], // size of the icon
});

var trainIcon5 = L.icon({
    iconUrl: 'http://subwaytime.mta.info/img/5.png',
    iconSize: [32, 32], // size of the icon
});

var trainIcon6 = L.icon({
    iconUrl: 'http://subwaytime.mta.info/img/6.png',
    iconSize: [32, 32], // size of the icon
});

var group = new L.featureGroup();
group.addTo(map);

// SAMPLE CODE

function step1() {
    var penny = L.marker([40.833226, -73.860816], {icon: trainIcon6X});

    penny.bindTooltip(`<div style='width:40vw;white-space:normal;'><div style='font-size:14px;color:gray;'>8:30A.M.</div> <div style='font-size:16px;'>Downtown to Brooklyn Bridge-City Hall departs <strong>Parkchester</strong></div></div>`, {
        permanent: true
    });
    penny.openTooltip();
    penny.addTo(group);
    map.flyTo(penny.getLatLng(), 12);

    return penny;
}

function displayMessage(message) {
    document.getElementById('message-bar').innerHTML = message;
}


function step2() {
    displayMessage(
        `<h3>Downtown 6 train enroute to 125 St</h3>
        <p>8:30pm - 8:32pm, 12 min</p>`);
    var penny2 = animatedPathFind('608','621','6X',10,1000,40);
    map.fitBounds([[40.833226,-73.860816], [40.804138,-73.937594]]);
    return penny2;
}

function step3() {
    map.flyTo([40.804138,-73.937594], 16);
    displayMessage('<h3>Train stalled at 125 St</h3><p>8:32pm - now</p>');

    var penny = L.marker([40.804138,-73.937594], {icon: trainIcon6X});

    penny.bindTooltip(`
    <div style='width:40vw;white-space:normal;'>
        <div style='font-size:14px;color:gray;'>8:30A.M.</div>
        <div style='font-size:16px;'>
            Stalled train on M track
        </div>
    </div>`, {
        // permanent: true
    });
    penny.openTooltip();
    penny.addTo(group);
    // map.flyTo(penny.getLatLng(), 12);

    return penny;
}

function step4() {
    var penny6 = L.marker([40.810476,-73.926138], {icon: trainIcon6});

    penny6.bindTooltip(`
    <div style='width:40vw;white-space:normal;'>
        <div style='font-size:14px;color:gray;'>8:30A.M.</div>
        <div style='font-size:16px;'>
        Downtown <img src="http://subwaytime.mta.info/img/6.png" class="bullet-lcl" /> to Brooklyn Bridge delayed at <strong>3 Av - 138 St</strong>
        </div>
    </div>`, {
        // permanent: true
    });
    penny6.addTo(group);

    var penny5 = L.marker([40.813224,-73.929849], {icon: trainIcon5});

    penny5.bindTooltip(`
    <div style='width:40vw;white-space:normal;'>
        <div style='font-size:14px;color:gray;'>8:30A.M.</div>
        <div style='font-size:16px;'>
        Downtown <img src="http://subwaytime.mta.info/img/5.png" class='bullet-lcl' /> to Flatbush Av-Brooklyn College delayed at <strong>138 St - Grand Concourse</strong>
        </div>
    </div>`, {
        // permanent: true
    });
    penny5.addTo(group);

    penny6.openTooltip();
    map.fitBounds(group.getBounds(), {padding: [50, 50]});

    setTimeout(function() {
        penny6.closeTooltip();
        penny5.openTooltip();
    }, 2000);


    //map.flyTo(penny.getLatLng(), 12);

    return [penny5, penny6];
}

function play() {
    var mk1 = step1();
    setTimeout(function() {
        group.removeLayer(mk1);
        var mk2 = step2();

        setTimeout(function() {
            group.removeLayer(mk2);
            var mk3 = step3();
            setTimeout(function() {
                step4();
            }, 5000);
        }, 11000);
    }, 2000);
}

//play();
// END SAMPLE CODE

var tripsch = {};

var mark = null;

function replay(trip_id, tsepoch) {
    var tripvals = Object.values(tripsch);


    var i = 0;

    var interval;
    var canContinue = true;

    interval = setInterval(function() {
        if (!canContinue)
            return;
        var tripval = tripvals[i];
        var stopid = tripval[0].slice(0,3);
        var stopname = RTNYC.Subway.getStopIDName(stopid);
        var triploc = RTNYC.Subway.getStopIDLatLng(stopid);
        var time = tripval[1] + tsepoch;
        console.log(tripval);
        group.removeLayer(mark);
        if (mark)
        map.removeLayer(mark);

        if (i == 0) {
            mark = L.marker(triploc, {icon: trainIcon6X});

            mark.bindTooltip(`
            <div style='width:40vw;white-space:normal;'>
                <div style='font-size:14px;color:gray;'>${new Date(time).toLocaleTimeString()}</div>
                <div style='font-size:16px;'>
                    ${trip_id}
                </div>
            </div>`, {
                permanent: true
            });
            mark.openTooltip();
            mark.addTo(group);
            map.flyTo(mark.getLatLng(), 12);
        } else {
            var lasttripval = tripvals[i - 1];
            var laststopid = lasttripval[0].slice(0,3);
            var lastriploc = RTNYC.Subway.getStopIDLatLng(laststopid);
            var lasttime = lasttripval[1] + tsepoch;

            map.fitBounds([triploc, lastriploc], {padding: [100, 100]});

            mark = animatedPathFind(laststopid, stopid, '6', 1, 100, 20);

            displayMessage(`<h3>Enroute to <strong>${stopname}</strong></h3><p>${new Date(lasttime * 1000).toLocaleTimeString()} - ${new Date(time * 1000).toLocaleTimeString()}</p>`)
        }

        i++;
        if (i >= tripvals.length) {
            clearInterval(interval);
        }
    }, 2000);

    // for (var i = 0; i < tripvals.length; i++) {
    // }
}

function processSubwayTripFB(buf) {
    var trip_id = buf.nyctTripId();
    var ts = buf.timestamp();
    
    var pathsLength = buf.pathsLength();
    
    console.log(`[PB] Trip ID '${trip_id}' at ${new Date(ts * 1000).toLocaleString()} with ${pathsLength} paths`);

    for (var i = 0; i < pathsLength; i++) {
        var path = buf.paths(i);
        var gtfsTripId = path.tripId();
        var stopsLength = path.stopsLength();

        console.log(`[PB] GTFS Trip ID path #${i+1} "${gtfsTripId}" made ${stopsLength} stops`);

        for (var j = 0; j < stopsLength; j++) {
            var stop = path.stops(j);
            var stopId = stop.stopId();
            var stopTime = stop.on();
            var track = stop.arrivalTrack();
            console.log(`[PB] .. at stop ID ${stopId} on ${stopTime} at '${track}' track`)
            tripsch[stopTime] = [stopId, stopTime, track];
        }
    }

    replay(trip_id, ts);
}


// Test

var oReq = new XMLHttpRequest();
oReq.open("GET", "../subway/SESS_06_0119__PEL_BBR_1547969235.fb", true);
oReq.responseType = "arraybuffer";

oReq.onload = function (oEvent) {
  var arrayBuffer = oReq.response; // Note: not oReq.responseText
  if (arrayBuffer) {
    var byteArray = new Uint8Array(arrayBuffer);
    // for (var i = 0; i < byteArray.byteLength; i++) {
    //   // do something with each byte in the array
    // }
    // console.log(byteArray);
    var message_buf = new flatbuffers.ByteBuffer(byteArray);

    nyc.realtime.SubwayTrip()
    /** @type {nyc.realtime.SubwayTrip} */
    var subway_trip = nyc.realtime.SubwayTrip.getRootAsSubwayTrip(message_buf);
    
    processSubwayTripFB(subway_trip);
  }
};

oReq.send(null);