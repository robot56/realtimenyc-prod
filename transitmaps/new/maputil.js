function getColorForRoute(route_id) {
    let map = {
        '1': 'red',
        '2': 'red',
        '3': 'red',
        '4': 'green',
        '5': 'green',
        '6': 'green',
        '7': 'purple',
        'A': 'blue',
        'C': 'blue',
        'E': 'blue',
        'B': 'orange',
        'D': 'orange',
        'F': 'orange',
        'M': 'orange',
        'N': 'yellow',
        'Q': 'yellow',
        'R': 'yellow',
        'W': 'yellow',
        'G': 'lightgreen',
        'J': 'brown',
        'Z': 'brown'
    }
    return map[route_id] || 'white';
}

function getIconFor(route) {
    var url = 'http://subwaytime.mta.info/img/'+route+'.png';
    var size = [32, 32];
    if (route == "6X") {
        url = 'https://upload.wikimedia.org/wikipedia/commons/0/0c/NYCS-bull-trans-6d.svg';
        size = [36, 36];
    }
    if (route == 'H') { // H shuttle train still used internally
        url = 'https://upload.wikimedia.org/wikipedia/commons/b/b6/NYCS-bull-trans-H.svg';
        size = [36, 36];
    }
    return L.icon({
        iconUrl: url,
        iconSize: size, // size of the icon
    });
}

function getLineNameForRoute(route_id) {
    let map = {
        '1': 'Seventh Avenue Line<br/>1, 2, 3',
        '4': 'Lexington Avenue Line<br/>4, 5, 6',
        '7': 'Flushing Line',
        'A': 'Eigth Avenue Line<br/>A, C, E',
        'B': 'Sixth Avenue Line<br/>B, D, F, M'
    };
    return map[route_id] || route_id;
}

function distanceBetween(lat1, lng1, lat2, lng2) {
    var s = Math.sqrt(
        (lat2 - lat1)**2 + (lng2 - lng1)**2
    );
    // console.debug('distanceBetween (%s, %s) (%s, %s): ' + s, lat1, lng1, lat2, lng2);
    return s;
}

function findClosestSequentialLLPoint(latlng_array, latlng) {
    let closestPoint = latlng_array[0];
    let closestDistance = 1000;
    for (var ll of latlng_array) {
        let d = distanceBetween(ll[0], ll[1], latlng[0], latlng[1]);
        if (d < closestDistance) {
            closestDistance = d;
            closestPoint = ll;
        }
    }

    console.log('Closest point in array is ' + closestPoint);
    return closestPoint;
}

function findClosestSequentialLLIndex(latlng_array, latlng) {
    let closestPoint = latlng_array[0];
    let closestDistance = 1000;
    let closestIndex = -1;
    for (var i = 0; i < latlng_array.length; i++) {
        let ll = latlng_array[i];
        let d = distanceBetween(ll[0], ll[1], latlng[0], latlng[1]);
        if (d < closestDistance) {
            closestDistance = d;
            closestPoint = ll;
            closestIndex = i;
        }
    }

    console.log('Closest point in array is ' + closestPoint);
    return i;
}

function currentUnixTS() {
    return Math.floor(Date.now() / 1000);
}