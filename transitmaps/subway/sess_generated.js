// automatically generated by the FlatBuffers compiler, do not modify

/**
 * @const
 * @namespace
 */
var nyc = nyc || {};

/**
 * @const
 * @namespace
 */
nyc.realtime = nyc.realtime || {};

/**
 * @constructor
 */
nyc.realtime.SubwayTripStop = function() {
  /**
   * @type {flatbuffers.ByteBuffer}
   */
  this.bb = null;

  /**
   * @type {number}
   */
  this.bb_pos = 0;
};

/**
 * @param {number} i
 * @param {flatbuffers.ByteBuffer} bb
 * @returns {nyc.realtime.SubwayTripStop}
 */
nyc.realtime.SubwayTripStop.prototype.__init = function(i, bb) {
  this.bb_pos = i;
  this.bb = bb;
  return this;
};

/**
 * @param {flatbuffers.ByteBuffer} bb
 * @param {nyc.realtime.SubwayTripStop=} obj
 * @returns {nyc.realtime.SubwayTripStop}
 */
nyc.realtime.SubwayTripStop.getRootAsSubwayTripStop = function(bb, obj) {
  return (obj || new nyc.realtime.SubwayTripStop).__init(bb.readInt32(bb.position()) + bb.position(), bb);
};

/**
 * @returns {number}
 */
nyc.realtime.SubwayTripStop.prototype.stopSequence = function() {
  var offset = this.bb.__offset(this.bb_pos, 4);
  return offset ? this.bb.readInt8(this.bb_pos + offset) : 0;
};

/**
 * @param {flatbuffers.Encoding=} optionalEncoding
 * @returns {string|Uint8Array|null}
 */
nyc.realtime.SubwayTripStop.prototype.stopId = function(optionalEncoding) {
  var offset = this.bb.__offset(this.bb_pos, 6);
  return offset ? this.bb.__string(this.bb_pos + offset, optionalEncoding) : null;
};

/**
 * @returns {number}
 */
nyc.realtime.SubwayTripStop.prototype.confidence = function() {
  var offset = this.bb.__offset(this.bb_pos, 8);
  return offset ? this.bb.readInt8(this.bb_pos + offset) : 0;
};

/**
 * @returns {number}
 */
nyc.realtime.SubwayTripStop.prototype.on = function() {
  var offset = this.bb.__offset(this.bb_pos, 10);
  return offset ? this.bb.readInt16(this.bb_pos + offset) : 0;
};

/**
 * @param {flatbuffers.Encoding=} optionalEncoding
 * @returns {string|Uint8Array|null}
 */
nyc.realtime.SubwayTripStop.prototype.arrivalTrack = function(optionalEncoding) {
  var offset = this.bb.__offset(this.bb_pos, 12);
  return offset ? this.bb.__string(this.bb_pos + offset, optionalEncoding) : null;
};

/**
 * @param {flatbuffers.Builder} builder
 */
nyc.realtime.SubwayTripStop.startSubwayTripStop = function(builder) {
  builder.startObject(5);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {number} stopSequence
 */
nyc.realtime.SubwayTripStop.addStopSequence = function(builder, stopSequence) {
  builder.addFieldInt8(0, stopSequence, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} stopIdOffset
 */
nyc.realtime.SubwayTripStop.addStopId = function(builder, stopIdOffset) {
  builder.addFieldOffset(1, stopIdOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {number} confidence
 */
nyc.realtime.SubwayTripStop.addConfidence = function(builder, confidence) {
  builder.addFieldInt8(2, confidence, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {number} on
 */
nyc.realtime.SubwayTripStop.addOn = function(builder, on) {
  builder.addFieldInt16(3, on, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} arrivalTrackOffset
 */
nyc.realtime.SubwayTripStop.addArrivalTrack = function(builder, arrivalTrackOffset) {
  builder.addFieldOffset(4, arrivalTrackOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @returns {flatbuffers.Offset}
 */
nyc.realtime.SubwayTripStop.endSubwayTripStop = function(builder) {
  var offset = builder.endObject();
  return offset;
};

/**
 * @constructor
 */
nyc.realtime.SubwayTripPath = function() {
  /**
   * @type {flatbuffers.ByteBuffer}
   */
  this.bb = null;

  /**
   * @type {number}
   */
  this.bb_pos = 0;
};

/**
 * @param {number} i
 * @param {flatbuffers.ByteBuffer} bb
 * @returns {nyc.realtime.SubwayTripPath}
 */
nyc.realtime.SubwayTripPath.prototype.__init = function(i, bb) {
  this.bb_pos = i;
  this.bb = bb;
  return this;
};

/**
 * @param {flatbuffers.ByteBuffer} bb
 * @param {nyc.realtime.SubwayTripPath=} obj
 * @returns {nyc.realtime.SubwayTripPath}
 */
nyc.realtime.SubwayTripPath.getRootAsSubwayTripPath = function(bb, obj) {
  return (obj || new nyc.realtime.SubwayTripPath).__init(bb.readInt32(bb.position()) + bb.position(), bb);
};

/**
 * @param {flatbuffers.Encoding=} optionalEncoding
 * @returns {string|Uint8Array|null}
 */
nyc.realtime.SubwayTripPath.prototype.tripId = function(optionalEncoding) {
  var offset = this.bb.__offset(this.bb_pos, 4);
  return offset ? this.bb.__string(this.bb_pos + offset, optionalEncoding) : null;
};

/**
 * @param {number} index
 * @param {nyc.realtime.SubwayTripStop=} obj
 * @returns {nyc.realtime.SubwayTripStop}
 */
nyc.realtime.SubwayTripPath.prototype.stops = function(index, obj) {
  var offset = this.bb.__offset(this.bb_pos, 6);
  return offset ? (obj || new nyc.realtime.SubwayTripStop).__init(this.bb.__indirect(this.bb.__vector(this.bb_pos + offset) + index * 4), this.bb) : null;
};

/**
 * @returns {number}
 */
nyc.realtime.SubwayTripPath.prototype.stopsLength = function() {
  var offset = this.bb.__offset(this.bb_pos, 6);
  return offset ? this.bb.__vector_len(this.bb_pos + offset) : 0;
};

/**
 * @param {flatbuffers.Builder} builder
 */
nyc.realtime.SubwayTripPath.startSubwayTripPath = function(builder) {
  builder.startObject(2);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} tripIdOffset
 */
nyc.realtime.SubwayTripPath.addTripId = function(builder, tripIdOffset) {
  builder.addFieldOffset(0, tripIdOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} stopsOffset
 */
nyc.realtime.SubwayTripPath.addStops = function(builder, stopsOffset) {
  builder.addFieldOffset(1, stopsOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {Array.<flatbuffers.Offset>} data
 * @returns {flatbuffers.Offset}
 */
nyc.realtime.SubwayTripPath.createStopsVector = function(builder, data) {
  builder.startVector(4, data.length, 4);
  for (var i = data.length - 1; i >= 0; i--) {
    builder.addOffset(data[i]);
  }
  return builder.endVector();
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {number} numElems
 */
nyc.realtime.SubwayTripPath.startStopsVector = function(builder, numElems) {
  builder.startVector(4, numElems, 4);
};

/**
 * @param {flatbuffers.Builder} builder
 * @returns {flatbuffers.Offset}
 */
nyc.realtime.SubwayTripPath.endSubwayTripPath = function(builder) {
  var offset = builder.endObject();
  return offset;
};

/**
 * @constructor
 */
nyc.realtime.SubwayTrip = function() {
  /**
   * @type {flatbuffers.ByteBuffer}
   */
  this.bb = null;

  /**
   * @type {number}
   */
  this.bb_pos = 0;
};

/**
 * @param {number} i
 * @param {flatbuffers.ByteBuffer} bb
 * @returns {nyc.realtime.SubwayTrip}
 */
nyc.realtime.SubwayTrip.prototype.__init = function(i, bb) {
  this.bb_pos = i;
  this.bb = bb;
  return this;
};

/**
 * @param {flatbuffers.ByteBuffer} bb
 * @param {nyc.realtime.SubwayTrip=} obj
 * @returns {nyc.realtime.SubwayTrip}
 */
nyc.realtime.SubwayTrip.getRootAsSubwayTrip = function(bb, obj) {
  return (obj || new nyc.realtime.SubwayTrip).__init(bb.readInt32(bb.position()) + bb.position(), bb);
};

/**
 * @param {flatbuffers.Encoding=} optionalEncoding
 * @returns {string|Uint8Array|null}
 */
nyc.realtime.SubwayTrip.prototype.nyctTripId = function(optionalEncoding) {
  var offset = this.bb.__offset(this.bb_pos, 4);
  return offset ? this.bb.__string(this.bb_pos + offset, optionalEncoding) : null;
};

/**
 * @returns {number}
 */
nyc.realtime.SubwayTrip.prototype.timestamp = function() {
  var offset = this.bb.__offset(this.bb_pos, 6);
  return offset ? this.bb.readInt32(this.bb_pos + offset) : 0;
};

/**
 * @param {number} index
 * @param {nyc.realtime.SubwayTripPath=} obj
 * @returns {nyc.realtime.SubwayTripPath}
 */
nyc.realtime.SubwayTrip.prototype.paths = function(index, obj) {
  var offset = this.bb.__offset(this.bb_pos, 8);
  return offset ? (obj || new nyc.realtime.SubwayTripPath).__init(this.bb.__indirect(this.bb.__vector(this.bb_pos + offset) + index * 4), this.bb) : null;
};

/**
 * @returns {number}
 */
nyc.realtime.SubwayTrip.prototype.pathsLength = function() {
  var offset = this.bb.__offset(this.bb_pos, 8);
  return offset ? this.bb.__vector_len(this.bb_pos + offset) : 0;
};

/**
 * @returns {number}
 */
nyc.realtime.SubwayTrip.prototype.direction = function() {
  var offset = this.bb.__offset(this.bb_pos, 10);
  return offset ? this.bb.readInt8(this.bb_pos + offset) : 0;
};

/**
 * @param {flatbuffers.Builder} builder
 */
nyc.realtime.SubwayTrip.startSubwayTrip = function(builder) {
  builder.startObject(4);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} nyctTripIdOffset
 */
nyc.realtime.SubwayTrip.addNyctTripId = function(builder, nyctTripIdOffset) {
  builder.addFieldOffset(0, nyctTripIdOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {number} timestamp
 */
nyc.realtime.SubwayTrip.addTimestamp = function(builder, timestamp) {
  builder.addFieldInt32(1, timestamp, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} pathsOffset
 */
nyc.realtime.SubwayTrip.addPaths = function(builder, pathsOffset) {
  builder.addFieldOffset(2, pathsOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {Array.<flatbuffers.Offset>} data
 * @returns {flatbuffers.Offset}
 */
nyc.realtime.SubwayTrip.createPathsVector = function(builder, data) {
  builder.startVector(4, data.length, 4);
  for (var i = data.length - 1; i >= 0; i--) {
    builder.addOffset(data[i]);
  }
  return builder.endVector();
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {number} numElems
 */
nyc.realtime.SubwayTrip.startPathsVector = function(builder, numElems) {
  builder.startVector(4, numElems, 4);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {number} direction
 */
nyc.realtime.SubwayTrip.addDirection = function(builder, direction) {
  builder.addFieldInt8(3, direction, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @returns {flatbuffers.Offset}
 */
nyc.realtime.SubwayTrip.endSubwayTrip = function(builder) {
  var offset = builder.endObject();
  return offset;
};

// Exports for Node.js and RequireJS
this.nyc = nyc;
