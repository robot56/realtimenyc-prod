function processServerUpdates(j) {
    for (var i in j) {
        console.debug('READING UPDATE ' + i);
        let update = j[i];
        let timenow = Math.ceil(Date.now() / 1000);
        if (update.c == "TripAssigned") {
            //console.log('new assigned trip:')
            //console.log(update);
        } else if (update.c == 'StopChange') {
            console.info('Got new StopChange update')
            console.debug(JSON.stringify(update))

            let tripid = update.data.TripID;
            let timestamp = update.data.timestamp;

            var route = tripid[1];

            let current_status = update.data.CurrentStatus;
            let from_stop = (update.data.LastStopID != undefined) ? update.data.LastStopID : update.data.CurrentStopID;
            if (from_stop == undefined) {
                console.warn('Missing "from_stop" for trip "%s", this needs to be provided by the backend or we are unable to animate the said train.',
                tripid);
                continue;
            }
                
            from_stop = from_stop.slice(0, 3);

            var to_stop = from_stop;
            if (update.data.NextScheduledStop != undefined) {
                to_stop = update.data.NextScheduledStop.slice(0,3);
            }

            $LogVerbose(station_to_latlng[to_stop])
            $LogVerbose(station_to_latlng[from_stop])

            // 

            let next_stop_time = update.data.NextStopArrivingOn;

            var duration = 1000;
            
            let tooltip_extra = "";

            let last_marker_data = visible_subway_cars[tripid];
            let tracked_trip = tracked_trips[tripid];

            var $marker;

            let _departure_time = update.data.CurrentStopDepartingOn;
            // depature time is 15s+ arrival time, at least on the IRT feeds
            // depature time = arrival time on this feed, so +15s is required
            var departure_time;
            if (_departure_time != undefined)
                var departure_time = _departure_time + 15;
            //console.warn('Departure time is %d', departure_time);
            if (tracked_trip == undefined) {
                if (next_stop_time == undefined) {
                    // Our train is not going to be moving anywhere
                    // $marker = _createStationaryTrainMarker(station_to_latlng[from_stop], route);
                    $marker = createStationaryTrainMarker(tripid, from_stop, route, to_stop, departure_time, next_stop_time);
                    tooltip_extra += "<br/>No next stop (Created)";
                } else {
                    if (current_status == 0 /* at station */) {
                        let _depature_time = update.data.CurrentStopDepartingOn;
                        // depature time is 15s+ arrival time, at least on the IRT feeds
                        // depature time = arrival time on this feed, so +15s is required
                        // let departure_time = _depature_time + 15;
                        let time_remaining = departure_time - timenow;
                        console.log('TU: NEW Trip "%s" will depart "%s" in %d seconds (%d - %d)',
                            tripid, from_stop, time_remaining, departure_time, timenow);
                        // $marker = createStationaryTrainMarker(station_to_latlng[from_stop], route);
                        $marker = createStationaryTrainMarker(tripid, from_stop, route, to_stop, departure_time, next_stop_time);
                        console.warn($marker);
                        tooltip_extra += "<br/>Status: At Station (Created)";
                    } else if (current_status == 1 /* approaching */) {
                        // let poly = findConnectingPolysRecursive(from_stop, to_stop);
                        // let coords = longLatToLatLngArr(poly, from_stop)
                        // duration = next_stop_time - timenow;
                        // duration = Math.min(duration, 1000);
                        // console.log('TU: Trip "%s" now approaching "%s", will arrive in %d secs (from %d to %d)', 
                        //     tripid, to_stop, duration, next_stop_time, timenow);
                        // $marker = createTrainMarker(coords, duration * 1000, route);

                        $marker = createTrainMarker(tripid, from_stop, to_stop, timenow, next_stop_time, route);
                        console.warn($marker);
                        tooltip_extra += "<br/>Status: Approaching (Created)";
                    } else /* enroute to */ {
                        // let poly = findConnectingPolysRecursive(from_stop, to_stop);
                        // let coords = longLatToLatLngArr(poly, from_stop)
                        // duration = next_stop_time - timenow;
                        // duration = Math.min(duration, 1000);
                        // console.log('TU: Trip "%s" now approaching "%s", will arrive in %d secs (from %d to %d)', 
                        //     tripid, to_stop, duration, next_stop_time, timenow);
                        // $marker = createWaitingTrainMarker(coords, duration * 1000, route);

                        $marker = createWaitingTrainMarker(tripid, from_stop, to_stop, timenow, next_stop_time, route);
                        //console.warn($marker);
                        tooltip_extra += "<br/>Status: Enroute To (Created)";
                    }
                }

                tracked_trips[tripid] = {
                    marker: $marker,
                    schedule: getTripSchedule(tripid)
                };
                console.log($marker);
                $marker.addTo(map);
                // console.warn($marker);
            } else {
                $marker = tracked_trip.marker;
                $trip = $marker.trip;
                $trip.from_stop_depart = departure_time;
                $trip.to_stop_at = next_stop_time;

                /* Work off of existing trip */
                if (next_stop_time == undefined) { // train is stationary, not going anywhere
                    //if (tracked_trip.marker.trip.to_stop)
                    console.log("Trip '%s' is at %s/%s, not going anywhere", 
                        tripid, $trip.from_stop, $trip.to_stop);
                    tooltip_extra += "<br/>No next stop";
                    $trip.from_stop = from_stop;
                    //$trip.from_stop_depart = departure_time;
                    $trip.to_stop = to_stop;
                    moveTrainMarkerToStation($marker, from_stop);
                } else {
                    if (current_status == 0 /* at station */) {
                        let _current_stop = from_stop;
                        tooltip_extra += "<br/>Status: At Station";
                        if ($marker.isEnded()) {
                            tooltip_extra += " (Already there?)";
                        }
                        // TODO: Fix following code below which is broken and causes odd behavior
                        // in the meantime we fly over to the station if not already there w/o
                        // following the polyline
                        if ($trip.to_stop && false) { // was last approaching the stop
                            if ($trip.to_stop == _current_stop /* this is CurrentStop for status 0 */) {
                                progressTrainMarkerToStation($marker, $trip.from_stop, _current_stop, // becomes 'from_stop' when we're AtStation
                                     timenow, timenow);
                                tooltip_extra += "<br>moved via progressTrainMarkerToStation() to " + from_stop;                  
                            } else {
                                console.warn('Trip "%s" is at a stop that we were not enroute to: %s (expected %s)',
                                    tripid, _current_stop, $trip.to_stop);
                                throw 'not enroute to stop';
                                console.log($trip);
                                moveTrainMarkerToStation($marker, from_stop);
                                tooltip_extra += "<br>moved via moveTrainMarkerToStation() to " + from_stop;
                            }
                            $trip.from_stop = from_stop;
                            $trip.to_stop = to_stop;
                            $trip.to_stop_on = next_stop_time;
                        } else {
                            console.log("Trip '%s' is now at at station that wasn't planned: ",
                                tripid, _current_stop);
                            tooltip_extra += "<br>moved via moveTrainMarkerToStation(2) to " + _current_stop;
                            moveTrainMarkerToStation($marker, _current_stop);
                            $trip.from_stop = _current_stop;
                            $trip.to_stop = to_stop;
                            $trip.to_stop_on = next_stop_time;
                        }
                    } else if (current_status == 1 /* approaching */) {
                        if ($marker.isEnded()) {
                            tooltip_extra += "<br/>Marker was ended before approaching!"
                        }
                        tooltip_extra += "<br/>Status: Approaching";
                        if ($trip.to_stop == from_stop && !$marker.isEnded()) { // traveling somewhere before arriving to the last stop
                            moveTrainMarkerToStation($marker, from_stop);
                            console.log("approaching: moved trip '%s' which didn't get to '%s' in time, trip has already left!",
                                tripid, getFriendlyStopName(from_stop));
                        }
                        //console.assert($trip.current_status == 2); // make sure we were already enroute to
                        $trip.to_stop = to_stop;
                        $trip.to_stop_on = next_stop_time;
                        $trip.from_stop = from_stop;
                        progressTrainMarkerToStation($marker, from_stop, to_stop, timenow, next_stop_time);
                    } else if (current_status == 2 /* enroute to */) {
                        //console.assert($trip.current_status == 2 || $trip.current_status == 0, "Enroute before getting to a stop!");
                        tooltip_extra += "<br/>Status: Enroute To";
                        if ($trip.to_stop == from_stop && !$marker.isEnded()) { // traveling somewhere before arriving to the last stop
                            moveTrainMarkerToStation($marker, from_stop);
                            console.log("enroute: moved trip '%s' which didn't get to '%s' in time, trip has already left!",
                                tripid, getFriendlyStopName(from_stop));
                        }
                        $trip.to_stop = to_stop;
                        $trip.to_stop_on = next_stop_time;
                        $trip.from_stop = from_stop;
                        travelTrainMarkerToStation($marker, from_stop, to_stop, timenow, next_stop_time)
                    } else {
                        throw 'Unknown current_status: ' + current_status;
                    }
                }
            }
            if ($marker.trip.extra_str != undefined) {
                tooltip_extra += $marker.trip.extra_str;
                $marker.trip.extra_str = undefined;
            }
            if ($marker.trip.extra_str_persistant != undefined) {
                tooltip_extra += $marker.trip.extra_str_persistant;
            }
            $marker.trip.current_status = current_status;
            //console.assert($marker.trip.from_stop_depart != undefined);

            $marker.start();
            $marker.bindTooltip();
            $marker.setTooltipContent(tripid + ' @ ' + getTimeStringFromUnixTimestamp(timestamp) + ' - Cum Arr/Dep Delay: {6}/{7}s<br/>from "{0}" ({1}) to "{2}" ({3})<br/> on {4} (dept. {5} + 15s)'
                .format(station_to_name[from_stop], /* 0 */ 
                    from_stop, /* 1 */
                    station_to_name[to_stop], /* 2 */ 
                    to_stop, /* 3 */
                    new Date(next_stop_time * 1000).toLocaleTimeString(), /* 4 */ 
                    new Date(_departure_time * 1000).toLocaleTimeString(), /* 5 */ update.data.CumulativeArrivalDelay, /* 6 */ update.data.CumulativeDepartureDelay /* 7 fucking typos! */)
                + tooltip_extra);

            console.log("Trip '%s' is %s:", tripid, tooltip_extra)
            console.log("marker.trip.from_stop=%s, from_stop=%s",
            getFriendlyStopName($marker.trip.from_stop), getFriendlyStopName(from_stop))
            console.log("marker.trip.to_stop=%s, to_stop=%s",
                getFriendlyStopName($marker.trip.to_stop), getFriendlyStopName(to_stop))
            console.assert($marker.trip.from_stop == from_stop, "$marker.trip.from_stop, from_stop")
            console.assert($marker.trip.to_stop == to_stop, "$marker.trip.to_stop, to_stop")

            // if (next_stop_time == undefined) {
            //     //var marker = new TrainMarker()
            //     var marker = L.marker(station_to_latlng[to_stop], {icon: getIconFor(route)});
            //     tooltip_extra += "<br/>No next stop";
            // } else {
            //     if (current_status == 0 /* at station */) {
            //         tooltip_extra += "<br/>Status: At station"
            //         let depature_time_ = update.data.CurrentStopDepartingOn;
            //         let departure_time = depature_time_ + 15; 
            //         // depature time is 15s+ arrival time, at least on the IRT feeds
            //         // depature time = arrival time on this feed, so +15s is required
            //         let timeremaining = depature_time_ - timenow;
            //         console.info("Train %s is at station will depart in %d seconds (%d - %d)",
            //             tripid, timeremaining, depature_time_, timenow)
            //         var marker = L.marker(station_to_latlng[from_stop], {icon: getIconFor(route)});
            //         if (timeremaining < 20) {
            //             tooltip_extra += "<br/>Will depare before we get next update"
            //             // we won't get updated data from the server in time so we need to update ourselves
            //         }
            //     } else {
            //         tooltip_extra += "<br/>Status: " + (current_status == 1 ? "Approaching" : "Enroute To")
            //         let poly = findConnectingPolysRecursive(from_stop, to_stop);
            //         console.log(poly)
            //         let coords = longLatToLatLngArr(poly/*.geometry.coordinates*/, from_stop)
            //         duration = next_stop_time - timenow;
            //         duration = Math.min(duration, 1000);
            //         console.log('seconds to go: %d (from %d to %d)', 
            //             duration, next_stop_time, timenow);
            //         var marker = L.Marker.movingMarker(coords, duration * 1000, {icon: getIconFor(route)});
            //         marker.start();
            //         marker.addTo(map);
            //     }   
            // }

            // tooltip_extra += "<hr/>A test!"
            
            // marker.bindTooltip(tripid + '<br/>from "{0}" ({1}) to "{2}" ({3})'
            //     .format(station_to_name[from_stop], from_stop, station_to_name[to_stop], to_stop)
            //     + tooltip_extra);
            // if (visible_subway_cars[tripid] != undefined) {
            //     let mkr = visible_subway_cars[tripid];
            //     map.removeLayer(mkr);
            // }
            // //else {
            //     visible_subway_cars[tripid] = marker;
            // //}

            // marker.addTo(map);
        } else if (update.c == 'ScheduleChange') {
            console.log('Got new schedule change:')
            console.log(update);
            let trip = update.data.trip;
            let schedule = update.data.schedule;
            let last_vehicle_pos = update.data.lvp;
            let timestamp = update.data.timestamp;

            let tracked_trip = tracked_trips[trip.TripID];

            let tripid = trip.TripID;

            if (update.data.schedule.length == 0) {
                console.warn('Must ignore schedule change because empty schedule', update);
                continue;
            }

            let next_stop_time = update.data.schedule[0].DepartureTime;

            if (tracked_trip == undefined) {
                console.info("Recieved ScheduleChange for a trip we aren't tracking: '%s'",
                trip.TripID);

                if (last_vehicle_pos) {
                    if (last_vehicle_pos.StopID) {
                        let stop_id = last_vehicle_pos.StopID.slice(0, 3);
                        
                        console.log("Can & will start tracking the above trip to " + stop_id);

                        let extra_str = '';

                        if (last_vehicle_pos.RelativeStatus == 0) {
                            $marker = createStationaryTrainMarker(trip.TripID, stop_id, trip.RouteID, stop_id, departure_time, next_stop_time);
                        } else {
                            // let schl = update.data.schedule.length;
                            // if (update.data.schedule[1]) {
                            //     console.warn(stop_id, update.data.schedule);
                            //     let last_stop = update.data.schedule[1].StopID.slice(0,3);
                            //     let next_stop_time = update.data.schedule[0].ArrivalTime;
                            //     $marker = createWaitingTrainMarker(tripid, last_stop, stop_id, timenow, next_stop_time + 10, 'Q');
                            //     console.warn('IT WORKS!',$marker, last_stop, stop_id, timenow, next_stop_time, timenow - next_stop_time);
                            //     extra_str = `from: ${last_stop} to ${stop_id}`;
                            // } else {
                            //     console.warn("IT DOESN'T WORK")
                            // }
                        }

                        if ($marker) {
                            tracked_trips[tripid] = {
                                marker: $marker,
                                schedule: getTripSchedule(tripid)
                            };
                            console.log($marker);
                            $marker.addTo(map);
                            $marker.bindTooltip(tripid + ' @ ' + getTimeStringFromUnixTimestamp(timestamp) + '<br/>'+extra_str);
                            $marker.start();
                        }
                    }
                }
            } else {
                let $marker = tracked_trip.marker;
                let $trip = $marker.trip;

                if (trip.RouteID != $trip.tripid[1] && trip.RouteID) {
                    // console.info('updating route icon for trip ' + $trip.tripid);
                    console.info(`Route icon for ${$trip.tripid} updated to ${trip.RouteID}, ${getIconFor(trip.RouteID)}`);
                    $marker.setIcon(getIconFor(trip.RouteID));
                }

                if ($trip.to_stop_at) {
                    for (var i in schedule) {
                        var st = schedule[i];
                        //console.warn("%s == %s", st.StopID.slice(0, 3), $trip.to_stop);
                        if (st.StopID.slice(0, 3) == $trip.to_stop) {
                            console.info("ScheduleChange for trip, ", tripid, st.StopID);
                            if ($trip.current_status == TStatus.Enroute) {
                                console.assert(st.ArrivalTime > 0, "st.ArrivalTime > 0");

                                travelTrainMarkerToStation($marker, $trip.from_stop, $trip.to_stop, Date.now() / 1000, st.ArrivalTime);
                                $marker.setTooltipContent($marker.getTooltip()._content + '<br/>Updated arrival time: {0} ({1} delay) (dept: {2})'.format(new Date(st.ArrivalTime * 1000).toLocaleTimeString(), st.ArrivalTime - $trip.to_stop_at, new Date(st.DepartureTime * 1000).toLocaleTimeString()));
                                $marker.trip.extra_str_persistant = '<br/>Schedule Updated on: ' + new Date().toLocaleTimeString();
                            } else if ($trip.current_status == TStatus.Approaching) {
                                console.assert(st.ArrivalTime > 0, "st.ArrivalTime > 0");
                                progressTrainMarkerToStation($marker, $trip.from_stop, $trip.to_stop, Date.now() / 1000, st.ArrivalTime);
                                $marker.setTooltipContent($marker.getTooltip()._content + '<br/>Updated arrival time: {0} ({1} delay) (dept: {2})'.format(new Date(st.ArrivalTime * 1000).toLocaleTimeString(), st.ArrivalTime - $trip.to_stop_at, new Date(st.DepartureTime * 1000).toLocaleTimeString()));                                $marker.trip.extra_str_persistant = '<br/>Schedule Updated on: ' + new Date().toLocaleTimeString();
                            }
                        }
                    }
                }                

                // if ($trip.current_status == TStatus.AtStation) {
                    
                // }

                // console.warn("");
            }

            //alert('got schedule change!');
        } else if (update.c == 'TripComplete') {
            console.log('Trip is now complete:', update.data.tripid);

            let tripid = update.data.TripID;

            let tracked_trip = tracked_trips[tripid];

            if (tracked_trip) {
                map.removeLayer(tracked_trip.marker);
                tracked_trips[tripid] = undefined;
            }
        }
    }
}

var FlatMessageType_v1 = {
    NYCSubway_StopChange: 0x1001,
    NYCSubway_TripAssigned: 0x1002,
    NYCSubway_ScheduleChange: 0x1003,
    NYCSubway_TripComplete: 0x1004,
    NYCSubway_CurrentTripsResponse: 0x1011,
    
    NYCBus_StopChange: 0x201,
    NYCBus_TripAssigned: 0x202,
    NYCBus_ScheduleChange: 0x203,
    NYCBus_PositionChange: 0x204,

    NYCBus_CurrentTripsResponse: 0x2011

};

//var ws = new WebSocket("ws://nyctlibdemo.serv.zubear.me:443/");
var ws = new WebSocket("ws://192.168.12.25:7777/")

var wsACE = new WebSocket("ws://192.168.1.25:7776/");
var wsBDFM = new WebSocket("ws://192.168.1.25:7778/");

function processFlatbufferStopChange(buffer) {
    let convertedJson = {
        "timestamp": buffer.timestamp(),
        "TripID": buffer.tripId(),
        "CurrentStatus": buffer.currentStatus(),
        "CumulativeArrivalDelay": buffer.cumulativeArrivalDelay(),
        "CumulativeDepartureDelay": buffer.cumulativeDepartureDelay(),
        "OnSchedule": buffer.onSchedule()
    };

    let stop_progress = buffer.currentStatus();
    if (stop_progress == 0 /* at station */) {
        convertedJson['CurrentStopID'] = buffer.currentStopId();
        convertedJson['CurrentStopDepartingOn'] = buffer.currentStopDepartingOn();
    } else {
        convertedJson['LastStopID'] = buffer.lastStopId();
        convertedJson['NextScheduledStop'] = buffer.nextStopId();
        convertedJson['NextStopArrivingOn'] = buffer.nextStopArrivingOn();
        convertedJson['NextStopDepartingOn'] = buffer.nextStopDepartingOn();
    }

    processServerUpdates([{
        'v': 1,
        't': 1,
        'c': 'StopChange',
        'data': convertedJson
    }]);
}

function processFlatbufferScheduleUpdate(buffer) {

    let tripbuffer = buffer.trip();

    let convertedJson = {
        'trip': {
            'GTFSTripID': tripbuffer.gtfsTripId(),
            // we don't actually provide the below
            // 'StartDate': tripbuffer.startDate(),
            'RouteID': tripbuffer.routeId(),
            'TripID': tripbuffer.tripId(),
            'Assigned': tripbuffer.assigned(),
            'Direction': 0
        },
        'schedule': [],
        'timestamp': buffer.timestamp()
    }

    if (buffer.lastVehiclePosition()) {
        var vpos = buffer.lastVehiclePosition();
        convertedJson['lvp'] = {
            'StopID': vpos.stopId(),
            'Track': vpos.track(),
            'RelativeStatus': vpos.relativeStatus(),
            'StopIndex': vpos.stopIndex()
        }
    }

    for (var i = 0; i < buffer.scheduleLength(); i++) {
        let schedule = buffer.schedule(i);
        convertedJson.schedule.push({
            'ScheduledTrack': schedule.scheduledTrack(),
            'ActualTrack': schedule.actualTrack(),
            'ArrivalTime': schedule.arrivalTime(),
            'DepartureTime': schedule.departureTime(),
            'StopID': schedule.stopId()
        });
    }

    // console.warn(convertedJson);

    processServerUpdates([{
        'v':1,
        't': 1,
        'c': 'ScheduleChange',
        'data': convertedJson
    }])
}

function processFlatbufferTripCompleteUpdate(buffer) {
    let tripid = buffer.tripId();
    processServerUpdates([{
        v: 1,
        t: 1,
        c: 'TripComplete',
        data: {
            tripid: tripid
        }
    }]);
}

function processFlatbufferUpdates(data) {
    
    var version_view = new DataView(data, 0, 2);
    
    
    let version = version_view.getUint16(0, true);

    var current_index = 4;

    var readMessage = function() {
        console.log('READING MESSAGE, CURR INDEX=%d', current_index)
        var header_view = new DataView(data, current_index, 8);
        let message_len = header_view.getInt32(0, true);
        if (message_len == 0)
            return;
        //console.assert(message_len > 4, 'message_len > 4');
        let message_type = header_view.getInt32(4, true);
        current_index += 8;

        console.debug('-> FB version=%d, len=%d, type=%d', version, message_len, message_type);

        var message = new Uint8Array(data, current_index, message_len);
        var message_buf = new flatbuffers.ByteBuffer(message);
        current_index += message_len;

        switch (message_type) {
            case FlatMessageType_v1.NYCSubway_StopChange:
                var stop_update = nyc.realtime.NYCSubwayStopUpdate.getRootAsNYCSubwayStopUpdate(message_buf);
                processFlatbufferStopChange(stop_update);
                break;
            case FlatMessageType_v1.NYCSubway_ScheduleChange:
            case FlatMessageType_v1.NYCSubway_TripAssigned:
            case FlatMessageType_v1.NYCSubway_CurrentTripsResponse:
                var schedule_update = nyc.realtime.NYCSubwayScheduleUpdate.getRootAsNYCSubwayScheduleUpdate(message_buf);
                processFlatbufferScheduleUpdate(schedule_update);
                break;
            case FlatMessageType_v1.NYCSubway_TripComplete:
                var tripcomplete_update = nyc.realtime.NYCSubwayTripComplete.getRootAsNYCSubwayTripComplete(message_buf);
                processFlatbufferTripCompleteUpdate(tripcomplete_update);
                break;
            default:
            console.error('unknown packet type %d', message_type)
        }    
    }
 
    var max_messages = 256;
    // data is LE so we can read first byte -- messages usually never above 256 bytes (for now)
    while ((current_index + 4) < data.byteLength) { // still have data to read
        readMessage();
        if (max_messages-- == 0) {
            break;
        }
    }
    console.log('Current Index: %d -- Byte Length: %d', current_index, data.byteLength)

    // var message = new Uint8Array(data, 10, message_len - 4);
    // var message_buf = new flatbuffers.ByteBuffer(message);
    
}

ws.binaryType = 'arraybuffer';
wsACE.binaryType = 'arraybuffer';
wsBDFM.binaryType = 'arraybuffer';

function onMessage(event) {
    if (event.data instanceof ArrayBuffer) {
        // binary data, read as flatbuffer
        console.debug(event.data)
        // console.log("-> BIN %d bytes", event.data.length);
        processFlatbufferUpdates(event.data);
    } else {
        // booooring text data :)
        let j = JSON.parse(event.data);
        if (j.t == 4) {
           processServerUpdates(j.updates);
        }
    }
}

ws.onmessage = onMessage;

wsACE.onmessage = onMessage;

wsBDFM.onmessage = onMessage;

ws.onclose = function(event) {
    if (embedded) {
        setTimeout(function() {
            document.location.reload();
        }, 1000);
        return;
    }
        
    var should_reconnect = confirm("Lost connection to the server. Would you like to reconnect?")
    if (should_reconnect) {
        document.location.reload();
    }
}

var tracker_service;

function startUpdatorService() {
    clearInterval(tracker_service);
    tracker_service = setInterval(function() {
        for (var i in tracked_trips) {
            let tracked_trip = tracked_trips[i].marker;
            // console.log(tracked_trips);
            let $trip = tracked_trip.trip;
            if ($trip.current_status == 0 || $trip.current_status == 10) {
                if ($trip.to_stop && $trip.to_stop_on) {
                    //console.debug($trip);
                    let next_stop_arrival_time = $trip.to_stop_on - (Date.now() / 1000);
                    let current_stop_departure_time = $trip.from_stop_depart - (Date.now() / 1000);
                    console.log("UpdatorService: Would arrive from stop '%s' to '%s' in %d seconds (departing in %d seconds), arriving at '%d'",
                        $trip.from_stop, $trip.to_stop, next_stop_arrival_time, current_stop_departure_time, $trip.to_stop_on);
                }
            }
        }
    }, 1 * 1000 /* Run every second */);
}

function onOpen(event) {
    // console.error(this);
    // console.warn(this.send);
    // throw 1;
    // Below requests Flatbuffers format instead of JSON
    // JSON remains supported
    // this.send('{"v":1,"t":3,"format":2}');
    setTimeout(() => {
        this.send('{"v":1,"t":1,"request":{"f":"subway","c":"get-current-trips","a":null}}');
    }, 100);
    //startUpdatorService();
}

ws.onopen = onOpen;

wsACE.onopen = onOpen;

wsBDFM.onopen = onOpen;
