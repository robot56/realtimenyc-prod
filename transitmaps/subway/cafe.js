if (Options.ConnectCAFE) {
    var cc = new WebSocket('ws://127.0.0.1:6464');

    cc.onmessage = function(data) {
        console.log('-> CAFE: ', data);
    
        var j = JSON.parse(data.data);
        // var d = j.d;
    
        onMessage(j);
    }    
} 

function displayMessage(message) {
    document.getElementById('message-bar').innerHTML = message;
}

var animationTimeout = null;

// onMessage({
//     t: 'DELAY_WATCHDOG',
//     d: {
//         monitored: [
//         {
//             'stop': '416',
//             track: '1',
//             effect: 'delay',
//             affected: [{
//                 'trip': '04 0047 NLT/WDL',
//                 'at': '416',
//                 'status': 'at',
//                 'why': 'stationOccupied',
//                 'by': ['06 0159+ PEL/BBR']
//             }]
//         }
//         ]
//     }
// })

function onMessage(j) {
    switch (j.t) {
        case 'DELAY_WATCHDOG': {
            var mon = j.d.monitored;

            for (var monitored of mon) {
                var ll = station_to_latlng[monitored.stop];

                clearInterval(animationTimeout);
                map.flyTo(ll, 14);
    
                for (var affected of monitored.affected) {
                    console.info(`Trip "${affected.trip}" is delayed by ${affected.by[0]} because of ${affected.why}`);
                    var tta = tracked_trips[affected.trip];
                    var ttr = tracked_trips[affected.by[0]];
                    tta.marker.openTooltip();
                    displayMessage(`Trip <mark>${affected.trip}</mark> is delayed by ${affected.by[0]} because of ${affected.why}`);
                    animationTimeout = setTimeout(function() {
                        tta.marker.closeTooltip();
                        ttr.marker.openTooltip();
                        displayMessage(`Trip ${affected.trip} is delayed by <mark>${affected.by[0]}</mark> because of ${affected.why}`);
                        setTimeout(function() {
                            ttr.marker.openTooltip();
                            displayMessage('')
                        }, 6000);
                    }, 3500);
                }
            }

            break;
        }
    }
}
console.log = function() {}
console.debug = function() {}