var viewPos = [40.740, -73.950];
var viewZoom = 12;

var zoomToLoc = QueryString.getValue('location');
var zoomToLevel = QueryString.getValue('zoom');
if (zoomToLoc) {
    let s = zoomToLoc.split(",");
    viewPos = [s[0], s[1]];
}

if (zoomToLevel) {
    viewZoom = zoomToLevel;
}

var map = L.map('map', {
    // markerZoomAnimation: false
}).setView(viewPos, viewZoom);

var tileservers = [
    "https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png",
    "http://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png",
    "http://cartodb-basemaps-{s}.global.ssl.fastly.net/rastertiles/voyager/{z}/{x}/{y}.png"
]
var embedded = false;

if (window.location.search.includes('embed=1')) {
    embedded = true;
    var tileserver = tileservers[2];
    // document.getElementById("map").style ='background-color:white'; // chrome bug fix
    document.getElementById("message-bar").style = 'color:black';
} else {
    var tileserver = tileservers[0];
    document.getElementById('alert-box').style = 'display:none;'
}

var tile = L.tileLayer(tileserver, {
	attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>',
	subdomains: 'abcd',
	maxZoom: 19
});

var Options = {
    ConnectCAFE: !embedded
};

// /* Leaflet Clickable Area Patch */
// // We use this for mobile devices because of morons
// L.Path.include({
//     _clickTolerance: function() {
//         console.log('called _clickTolerance ' + this.options.clickTolerance)
//         return null;
//         // return (this.options.clickTolerance ? this.options.clickTolerance : 0) + (this.options.stroke ? this.options.weight / 2 : 0) + (L.Browser.touch ? 10 : 0);
//     }
// });

var gm_arial_tile = L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
    maxZoom: 20,
    attribution: 'Google Maps Arial View',
    subdomains:['mt0','mt1','mt2','mt3']
});

//tile.addTo(map);
map.addLayer(tile)

if (QueryString.getValue('embed') == '2') {
    map.zoomControl.remove();
} else {
    L.control.locate().addTo(map);
}

function getColorForRoute(route_id) {
    let map = {
        '1': 'red',
        '2': 'red',
        '3': 'red',
        '4': 'green',
        '5': 'green',
        '6': 'green',
        '7': 'purple',
        'A': 'blue',
        'C': 'blue',
        'E': 'blue',
        'B': 'orange',
        'D': 'orange',
        'F': 'orange',
        'M': 'orange',
        'N': 'yellow',
        'Q': 'yellow',
        'R': 'yellow',
        'W': 'yellow',
        'G': 'lightgreen',
        'J': 'brown',
        'Z': 'brown'
    }
    return map[route_id] || 'white';
}

function getLineNameForRoute(route_id) {
    let map = {
        '1': 'Seventh Avenue Line<br/>1, 2, 3',
        '4': 'Lexington Avenue Line<br/>4, 5, 6',
        '7': 'Flushing Line',
        'A': 'Eigth Avenue Line<br/>A, C, E',
        'B': 'Sixth Avenue Line<br/>B, D, F, M'
    };
    return map[route_id] || route_id;
}

var stopmarker_layer = L.layerGroup().setZIndex(3);

function onLineHighlight(e) {
    // console.log('caleld onRouteHighlight');
    var layer = e.target;

    layer.setStyle({
        weight: 6
    });

    layer.bringToFront();
}

function onLineUnhighlight(e) {
     console.log('caleld unRouteHighlight');
    var layer = e.target;

    layer.setStyle({
        weight: 3
    });
}

for (var subwayroute in subwayMappedGeoJson) {
    var styles = {
        "color": getColorForRoute(subwayroute),
    };
    var features = subwayMappedGeoJson[subwayroute];
    var parsed_features_list = [];
    for (var i in features) {
        var feature = features[i];
        //console.log(feature)
    }
    var geoj_layer = L.geoJson(features, {style: styles});
    geoj_layer.addTo(map);
    geoj_layer
        .on('mouseover', onLineHighlight)
        .on('mouseout', onLineUnhighlight);
    geoj_layer.bindTooltip(getLineNameForRoute(subwayroute), {sticky: true, direction: 'auto'});
}

var station_to_latlng = {};
var station_to_name = {};

var showlabels = location.hash.includes("showlabels") ? true : false;
var showlabelicons = location.hash.includes("showlabels=2") ? true : false;

function getSvgIconUrlFor(route) {
    var line_to_svgurl_map = {
        '2': 'https://upload.wikimedia.org/wikipedia/commons/6/61/NYCS-bull-trans-2.svg',
        '6': 'https://upload.wikimedia.org/wikipedia/commons/5/53/NYCS-bull-trans-6.svg',
        '6X': 'https://upload.wikimedia.org/wikipedia/commons/0/0c/NYCS-bull-trans-6d.svg',
        '4': 'https://upload.wikimedia.org/wikipedia/commons/e/ec/NYCS-bull-trans-4.svg',
        '5': 'https://upload.wikimedia.org/wikipedia/commons/7/78/NYCS-bull-trans-5.svg',
        '5X': 'https://upload.wikimedia.org/wikipedia/commons/f/ff/NYCS-bull-trans-5d.svg',
        '7X': 'https://upload.wikimedia.org/wikipedia/commons/e/e6/NYCS-bull-trans-7d.svg',
        '': 'https://upload.wikimedia.org/wikipedia/commons/8/8a/NYCS-bull-trans-S.svg'
    }
    return line_to_svgurl_map[route] || "http://subwaytime.mta.info/img/" + route + ".png";
}

var l = subwaystationscsv.split("\n");
for (var i in l) {
    var line = l[i].split(",");
    var lat = line[9];
    var lng = line[10];
    station_to_latlng[line[2]] = [lat, lng];
    station_to_name[line[2]] = line[5];
    var latlng = new L.latLng(lat, lng);
    if (!showlabels) {
        var marker = L.circleMarker(latlng).setRadius(5);
        marker.bindTooltip(l[i]);
    } else {
        var lineiconstr = ""
        if (line[7] != undefined) {
            var svcs = line[7].split(" ")
            for (var j in svcs) {
                var svc = svcs[j];
                lineiconstr += '<img src="{0}" style="width:22px;height:22px;vertical-align:middle;" />'.format(getSvgIconUrlFor(svc));
            }    
        }
        var marker = new L.Marker(latlng, {
            icon: new L.DivIcon({
              className: 'subway-stop-dot',
              html: '<span style="color:white;position: absolute;padding-left: 16px;width: 230px;" class="subway-stop-label">'+ line[5] +' {0} ({1})</span>'.format(showlabelicons ? lineiconstr : '(' + line[7] + ')', line[2])
            })
        })
    }
//     marker.setZIndexOffset(-1);
//     let svg = `<svg width="260" height="60" xmlns="http://www.w3.org/2000/svg">
//     <style>text { font: italic 40px serif; fill: red; }</style>
//     <text x="1" y="40" class='station-name'>Parkchester</text>
// </svg>`;
    // let marker = L.marker(latlng, {
    //     'icon': L.icon({
    //         iconUrl: 'data:image/svg+xml;base64,' + btoa(svg),
    //         // iconUrl: 'https://upload.wikimedia.org/wikipedia/commons/0/0c/NYCS-bull-trans-6d.svg'
    //     })
    // });
    stopmarker_layer.addLayer(marker);
    //console.log('lat: ' + lat + ' lng: ' + lng);
}
// stopmarker_layer.addTo(map);

function gtfsToStationId(gtfsId) {
    for (var i in l) {
        var line = l[i].split(",");
        if (line[2] == gtfsId) {
            return line[0];
        }
    }

    throw 'did not find an approate station id for GTFS id: ' + gtfsId;
}

function stationIdToGtfs(stationId) {
    for (var i in l) {
        var line = l[i].split(",");
        if (line[0] == stationId) {
            return line[2];
        }
    }

    throw 'did not find an approate GTFS id for station id: ' + stationId;
}

var lastzoom = 0;

map.on('zoomend', function(e) {
    var zoom = e.target.getZoom();
	if (zoom <= 14 && lastzoom >= 14) {
		map.removeLayer(stopmarker_layer);
	} else if (zoom >= 14 && lastzoom <= 14) {
		map.addLayer(stopmarker_layer);
    }
    //console.log('zoom=%d', zoom)
    if (zoom <= 18 && lastzoom >= 18) {
        map.removeLayer(gm_arial_tile);
        map.addLayer(tile);
    } else if (zoom >= 18 && lastzoom <= 18) {
        //console.log('replacing layers')
        map.removeLayer(tile);
        map.addLayer(gm_arial_tile);
    }
    // if (zoom <= 16 && lastzoom >= 16) {
    //     // stopmarker_layer.eachLayer(function(layer) {
    //     //     layer.setRadius(5);
    //     // })
    //     map.removeLayer(stop_group);
    // } else if (zoom >= 16 && lastzoom <= 16) {
    //     // stopmarker_layer.eachLayer(function(layer) {
    //     //     layer.setRadius(14);
    //     // })
    //     map.addLayer(stop_group);
    // }
    lastzoom = zoom;
});

var trainIcon6 = L.icon({
    iconUrl: 'http://subwaytime.mta.info/img/6.png',
    iconSize: [32, 32], // size of the icon
});

// TODO: integrate with rtnyct.js
function getIconFor(route) {
    var url = 'http://subwaytime.mta.info/img/'+route+'.png';
    var size = [32, 32];
    if (route == "6X") {
        url = 'https://upload.wikimedia.org/wikipedia/commons/0/0c/NYCS-bull-trans-6d.svg';
        size = [36, 36];
    }
    if (route == 'H') { // H shuttle train still used internally
        url = 'https://upload.wikimedia.org/wikipedia/commons/b/b6/NYCS-bull-trans-H.svg';
        size = [36, 36];
    }
    return L.icon({
        iconUrl: url,
        iconSize: size, // size of the icon
    });
}

var trainIcon6X = L.icon({
        iconUrl: 'https://upload.wikimedia.org/wikipedia/commons/0/0c/NYCS-bull-trans-6d.svg',
        iconSize: [34, 34], // size of the icon
});

var trainIcon2 = L.icon({
    iconUrl: 'http://subwaytime.mta.info/img/2.png',
    iconSize: [32, 32], // size of the icon
});

//var mm = L.Marker.movingMarker([[40.805368, -73.914042], [40.833226, -73.860816]], 10000, {icon: trainIcon6}).addTo(map);
//mm.start();


// L.marker([40.833226, -73.860816], {icon: trainIcon6X}).addTo(map);
// L.marker([40.840295, -73.880049], {icon: trainIcon2}).addTo(map);

// Uncomment of the the below to change
//var $LogVerbose = console.debug.bind(console);
var $LogVerbose = function() {} // empty function, no action

function distanceBetween(lat1, lng1, lat2, lng2) {
    var s = Math.sqrt(
        (lat2 - lat1)**2 + (lng2 - lng1)**2
    );
    $LogVerbose('distanceBetween (%s, %s) (%s, %s): ' + s, lat1, lng1, lat2, lng2);
    return s;
}

// unstupidify long,lat to lat,lng array
function longLatToLatLngArr(arrin, from_station_id, to_station_id) {
    var newarr = []
    //console.debug('Correcting array: ' + arrin.toString())
    for (let i in arrin) {
        let long = arrin[i][0];
        let lat = arrin[i][1];
        newarr.push([lat, long]);
    }

    if (from_station_id != undefined) {
        var t = station_to_latlng[from_station_id];
        if (t == undefined) {
            console.log('llll: Did not find station data1 for %s -- aborting', from_station_id);
            return newarr;
        }
        var t2 = station_to_latlng[to_station_id];
        if (t2 == undefined) {
            console.log('lll: Did not find station data2 for %s -- aborting', to_station_id);
            return newarr;
        }
        var lat = t[0];
        var lng = t[1];
        var lat2 = t2[0];
        var lng2 = t2[1];
        // this below may not be 100% accurate, look into it
        // breaks with 2/3 manhattan-brooklyn train tunnel
        if (distanceBetween(newarr[0][0], newarr[0][1], lat, lng) > 
            distanceBetween(newarr[0][0], newarr[0][1], lat2, lng2)) {
            $LogVerbose('llll: Need to reverse')
            newarr.reverse();
        } else {
            // alert('no need to reverse');
        }
    }

    return newarr;
}

function polyMerge(polys, fallback_max_distance, max_distance) {
    $LogVerbose('polyMerge: merging:', polys);
    //polys = JSON.parse(JSON.stringify($polys));
    //console.warn(JSON.stringify(polys[1]))
    if (max_distance == null) 
        var max_distance = 0.0005;
    let first_latlng = 0;
    var polyarray_out = [];
    for (let i in polys) {
        // holy ---- this single line below was causing such weirdness on Chrome
        // because `polys` was being passed by reference, somehow the code below was
        // modifying the data of `polys` causing quite a bit of odd behavior in the code
        // where if you called animatedPathFinder() twice on the same data, it would fail
        // the second try for no clear reason...
        // `slice` solution to copy per https://stackoverflow.com/a/21761178
        if (polys[i] == undefined) continue;
        let poly = polys[i].slice(0);
        let first_point = poly[0];
        let last_point = poly[poly.length - 1];
        if (first_point == undefined || last_point == undefined) continue;

        // console.debug('PM: first_point: lat=%s, lng=%s', first_point[0], first_point[1]);
        // console.debug('PM: last_point: lat=%s, lng=%s', last_point[0], last_point[1]);

        if (polyarray_out.length == 0) {
            //console.debug('pushing')
            polyarray_out = poly;
            continue;
        }

        // console.log('polyarray_out', polyarray_out);
        
        let fl = polyarray_out[0];
        let ll = polyarray_out[polyarray_out.length - 1]
        
        let distance_first_first = distanceBetween(fl[0], fl[1], first_point[0], first_point[1]);
        let distance_first_last = distanceBetween(fl[0], fl[1], last_point[0], last_point[1])
        let distance_last_first = distanceBetween(ll[0], ll[1] /* output last */, 
            first_point[0], first_point[1]);
        let distance_last_last = distanceBetween(ll[0], ll[1], last_point[0], last_point[1]);

        var distances = [distance_first_first + 0.00001, distance_first_last + 0.00001, 
            distance_last_first + 0.00001, distance_last_last + 0.00001];
        if (fallback_max_distance == undefined)
            fallback_max_distance = Math.min(...distances)
        else  
            Math.min(fallback_max_distance, Math.min(...distances));

        if (distance_first_first < max_distance) {
            // // no idea how to even deal with this!
            // // we have to reverse the array, presumably
            // console.warn('This should never be hit')
            // console.info(fl)
            // console.info(first_point)
            // //alert('distanceBetween(1) fail');
            
            // below removes duplicate first entry if it exists to smooth transitions
            $LogVerbose('polyMerge: will shift and prepend')
            if (fl[0] == first_point[0] && fl[1] == first_point[1]) poly.shift();
            poly.reverse()
            polyarray_out = poly.concat(polyarray_out);
        } else if (distance_first_last < max_distance) {
            $LogVerbose('polyMerge: will prepend')
            polyarray_out = poly.concat(polyarray_out);
            //console.debug(polyarray_out)
        } else if (distance_last_first < max_distance /* matches first point of this */) {
            $LogVerbose('polyMerge: will append');
            polyarray_out = polyarray_out.concat(poly);
            //console.debug(polyarray_out);
        } else if (distance_last_last < max_distance) {
            $LogVerbose('polyMerge: will reverse and append'); // TODO: should we shift here?
            poly.reverse()
            polyarray_out = polyarray_out.concat(poly)
        } else {
            if (fallback_max_distance != undefined && fallback_max_distance != max_distance) {
                console.warn('polymerge is running itself again!')
                let ret = polyMerge(polys, undefined, fallback_max_distance);
                return ret;
            }
            console.error('polyMerge: Fell through for (%s) (%s) -- undefined behavior ' + max_distance, fl.toString(), ll.toString())
            //throw 'merge error'
        }

        $LogVerbose(poly)
        $LogVerbose('polyMerge: CurrentIteration 1st point: (%s, %s), last pt: (%s, %s)', 
            first_point[0].toFixed(5), first_point[1].toFixed(5), last_point[0].toFixed(5), last_point[1].toFixed(5))
    }
    // console.debug(polyarray_out)
    return polyarray_out;
}

function pathFind(from_stop_id, to_stop_id, max_recusion, max_path_size) {
    if (max_recusion == undefined) var max_recusion = 200;
    let findConnectingStops = function(stop) {
        let branches = [];
        for (var i in polystop_map) {
            let item = polystop_map[i];
            if (item.stop1 == stop || item.stop2 == stop) {
                let s2 = (item.stop1 == stop) ? item.stop2 : item.stop1;
                if (branches.includes(s2))
                    continue;
                else
                    branches.push(s2);
            }
        }
        return branches;
    };

    let possible_routes = findConnectingStops(from_stop_id);

    console.debug(possible_routes);
    if (possible_routes.includes(to_stop_id)) {
        return [from_stop_id, to_stop_id];
    }
    let runs_allowed = max_recusion;
    var m = [];
    var completed_paths = possible_routes;
    var max_path_size = max_path_size == undefined ? 8 : max_path_size;
    console.debug('looking for stop ' + to_stop_id + ' from ' + from_stop_id + ' - ')
    function find(laststop, routes, path, visited_paths) {
        if (visited_paths == undefined) visited_paths = [from_stop_id];
        console.debug(routes)
        var path_before_route = path//.slice(0)
        var visited_paths_before = visited_paths//.slice(0)
        // PATCH: Sort routes to prefer current line
        var _routes1 = [];
        var _routes2 = [];
        for (var routei in routes) {
            let _route = routes[routei];
            if (!_route) { // sometimes 'route' can be undefined...
                _routes1 = routes;
                _routes2 = [];
                break;
            }
            if (_route[0] == from_stop_id[0]) {
                _routes1.push(_route);
            } else {
                _routes2.push(_route);
            }
        }
        if (_routes1.concat(_routes2) != routes) {
            console.debug('SWAPPING!', _routes1.concat(_routes2), routes);
        }
        routes = _routes1.concat(_routes2);
        // ENDPATCH
        for (let i in routes) {
            let route = routes[i];
            let pr = findConnectingStops(route);
            pr = pr.filter(function(item) {
                return !visited_paths.includes(item);
            });
            if (pr.includes(to_stop_id)) {
                m = path.concat([[laststop, route],[route, to_stop_id]])
                console.debug("MATCH route=%s, pr=%s, path=%s", route, pr.toString(), path.toString())
                return true;
            } else {
                runs_allowed--;
                if (runs_allowed == 0) {
                    console.warn('pathFind(): too much runs, giving up')
                    return false;
                }
                if (path.length > max_path_size) {
//                    path = path_before_route;
//                    visited_paths = visited_paths_before;
                    visited_paths.push(route)
                    //console.debug('plen kill '+path_before_route>max_path_size)
                    //console.debug('blocked: ' + visited_paths.toString())
                    continue;
                }
                //console.debug('visited ' + route)
//                visited_paths = visited_paths_before;
                visited_paths.push(route);
                if (pr.length == 0) continue;
                //console.debug("route=%s, pr=%s, path=%s", route, pr.toString(), path.toString())
                let ret = find(route, pr, path.concat([[laststop, route]]), visited_paths);
                if (ret) return true;
            }
        }
        return false;
        //completed_paths.concat(visited_paths);
        //throw completed_paths;
    }

    let t = find(from_stop_id, possible_routes, []);
    //console.clear()
    console.debug("find(): ");
    console.debug(t)
    console.debug('Computed path: %s', m.toString())
    // DEBUG CODE BELOW
    // var pb =  [];
    // for (var i of m) {
    //     pb.push([Number(gtfsToStationId(i[0])), Number(gtfsToStationId(i[1]))]);
    //     // pb.push(gtfsToStationId(i[1]));
    // }
    // console.warn('Computed path (SIDs): ', pb);
    return m;
}

function findConnectingPolysRecursive(from_stop_id, to_stop_id, max_recusion, max_path_len) {
    let origin_stop = from_stop_id;

    let initial_try = polyFindingMagic(from_stop_id, to_stop_id);

    if (initial_try)
        return initial_try;

    let found_path = pathFind(from_stop_id, to_stop_id, max_recusion, max_path_len);
    
    let _foundroutes = [];
    for (var k in found_path) {
        let tk = found_path[k]
        let from = tk[0]
        let to = tk[1]
        $LogVerbose("k=%s, tk=%s, from=%s, to=%s", k, tk, from, to)
        let pfm = polyFindingMagic(from, to, null);
        //let pfm2 = polyFindingMagic(from, to, null);
        //console.log(JSON.stringify(pfm))
        //console.log(JSON.stringify(pfm2))
        //console.assert(JSON.stringify(pfm) == JSON.stringify(pfm2))
        //let pm1 = polyMerge([_foundroutes, pfm])
        //let pm2 = polyMerge([_foundroutes, pfm])
        //console.log(JSON.stringify(pm1));
        //console.log(JSON.stringify(pm2));
        //console.assert(JSON.stringify(pm1)===JSON.stringify(pm2));
        _foundroutes = polyMerge([_foundroutes, pfm], 0.005);
        //_foundroutes = pfm.concat(_foundroutes);
        // oh js...

        // console.info('FR', k, _foundroutes);
    }
    //console.assert(_foundroutes.length > 0)
    console.debug(_foundroutes)

    if (_foundroutes.length)
        return _foundroutes;
    else
        return null;
}

function polyFindingMagic(from_stop_id, to_stop_id, linez) {
    var relevant_services = serviceIdToRouteId(from_stop_id[0]) + serviceIdToRouteId(to_stop_id[0])
    console.debug("polyFind: from %s to %s -- services: %s", from_stop_id, to_stop_id, relevant_services)   

    let polyline_arr = [];
    let found = false;
    let found_lines = [];

    for (var i in polystop_map) {
        let item = polystop_map[i];
        if (item.stop1 == from_stop_id || item.stop2 == from_stop_id) {
            let s1 = (item.stop1 == from_stop_id) ? item.stop1 : item.stop2;
            let s2 = (item.stop1 == from_stop_id) ? item.stop2 : item.stop1;
            if (item.stop1 == to_stop_id || item.stop2 == to_stop_id) {      
                for (var j in item.polys) {
                    let poly = item.polys[j];
                    let s = poly.split(".");
                    let polyObjId = s[1].replace("\*", "");

                    let match = gSubwayLinesGeoJsonMapped[polyObjId];

                    if (match == undefined) {
                        console.warn('Did not find poly feature for objid %s -- undefined behavior!', polyObjId)
                        return null;
                    }

                    let rts = !linez ? relevant_services : linez;

                    if (rts.includes(match.properties.rt_symbol) || item.ignorelines) {
                        let polyline = match.geometry.coordinates;
                        console.debug('polyFind: found match for object id %s:', polyObjId)
                        console.debug(polyline)
                        if (found && polyline_arr.toString() == polyline.toString()) {
                            console.error("POLYLINE DUPLICATE, Remove from polystop_map: " + poly);
                            continue;
                        }
                        polyline_arr.push(polyline);
                        found = true;
                        //break;
                    } else {
                        console.debug('polyFind: found match for object id %s:', poly)
                        console.info('polyFind: svc match fail ' + linez + ' ' + match.properties.rt_symbol)
                        console.debug(match)
                        found_lines.push(match.properties.rt_symbol)
                    }
                }
                

                // let poly = item.polys[0];
                // let s = poly.split(".");
                // let polyObjId = s[1].replace("\*", "");
                // console.log('found initial and dest requested stop id in %s', JSON.stringify(item));
                // // for (var j in gSubwayLinesGeoJsonMapped) {
                // //     let feature_data = gSubwayLinesGeoJsonMapped[j];
                // // }
                // if (gSubwayLinesGeoJsonMapped[polyObjId] == undefined) {
                //     console.log('did not find poly feature for objid')
                // } else {
                //     console.log('all is well, found')
                //     console.log(gSubwayLinesGeoJsonMapped[polyObjId].geometry.coordinates)
                //     return gSubwayLinesGeoJsonMapped[polyObjId];
                // }
            }
        }
    }

    if (found) {
        console.assert(polyline_arr.length > 0)
        let out = polyMerge(polyline_arr, 0.005)
        return out;
    } else {
        if (found_lines.length > 0) {
            console.warn('no match, but found lines, will try again with first of the following (if not already tried):')
            console.warn(found_lines);
            let first = found_lines[0];
            if (line != first) {
                let ret = polyFindingMagic(from_stop_id, to_stop_id, first);
                if (ret != null)
                    return ret;
                console.error('failed polyFindingMagic 2nd try')
                console.log(ret)
            }
        }
    }
    
    console.debug('FAILED');
    return null;
}

function serviceIdToRouteId(bullet) {
    bullet = bullet.replace("X", "")
    if ("123".includes(bullet)) return "1";
    if ("456".includes(bullet)) return "4";
    if ("7".includes(bullet)) return "7";
    if ("ACE".includes(bullet)) return "A";
    if ("BDFM".includes(bullet)) return "B";
    if ("NQRW".includes(bullet)) return "N";
    if ("JMZ".includes(bullet)) return "J";
    return bullet;
}

let poly = polyFindingMagic("213", "214");
let poly2 = polyFindingMagic("214", "215");
let poly3 = polyFindingMagic("215", "216");

let coords = longLatToLatLngArr(poly/*.geometry.coordinates*/, "213", "214")
let coords2 = longLatToLatLngArr(poly2/*.geometry.coordinates*/, "214", "215")
let coords3 = longLatToLatLngArr(poly3/*.geometry.coordinates*/, "215", "216")

function animatedPathFind(from_stop, to_stop, route, speed, maxrecursion, maxpathlen) {
    let polys = findConnectingPolysRecursive(from_stop, to_stop, maxrecursion, maxpathlen);
    if (!polys) return null;
    let coords = longLatToLatLngArr(polys, from_stop, to_stop);
    let mm3 = L.Marker.movingMarker(coords, speed * 1000, {icon: getIconFor(route == undefined ? "A" : route)}).addTo(map);
    mm3.setPercentageCompletion(0.85);
    mm3.start();
    return mm3;
}

// movingmarker lib needs to be able to automatically adjust to new durations
// depending on if train is early or delayed

// console.log('coords was: ' + poly/*.geometry.coordinates*/)
// console.log('coords is: ' + coords)
//var mm2 = L.Marker.movingMarker(coords, 10000, {icon: trainIcon6}).addTo(map);
//mm2.start();
// mm2.on('end', function() {
//     mm2.initialize(coords2, 10000, {icon: trainIcon6});
//     mm2.start();
//     mm2.on('end', function() {
//         mm2.initialize(coords3, 10000, {icon: trainIcon6});
//         mm2.start();
//         mm2.off('end', function(){});
//     })
// })

var visible_subway_cars = {};

var TrainMarker = L.Marker.MovingMarker.extend({
    // trip: {
    //     tripid: "",
    //     from_stop: "",
    //     from_stop_depart: 0,
    //     to_stop: "",
    //     from_stop_on: 0,
    //     to_stop_on: "",
    //     // Below is encoded as following:
    //     // 0 - At Stop
    //     // 1 - Approaching
    //     // 2 - Enroute To
    //     // 10 - At Stop (estimated)
    //     // 13 - Enroute To (estimated)
    //     status: 0,
    // },

    setTrip: function(td){
        this.trip = td;
    },

    getTrip: function() {
        return this.trip;
    }
})

var trips_at_stop = {};
var tracked_trips = {
    // "0": {
    //     "marker": null, /* TrainMarker ref */
    //     "schedule": null, /* TripSchedule ref */
    // }
};

function getTripSchedule() {
    return "";
}

function _createTrainMarker(latlong_array, /* Path from origin to destination */
    reach_destination_time, 
    route) {
        return new TrainMarker(latlong_array, reach_destination_time, {icon: getIconFor(route)});
}

function createTrainMarker(tripid, from_stop, to_stop, current_time, destination_arrival_time, route) {
    let poly = findConnectingPolysRecursive(from_stop, to_stop);
    let coords = longLatToLatLngArr(poly, from_stop)
    duration = destination_arrival_time - current_time;
    console.warn('TU: Trip "%s" now enroute to "%s", will arrive in %d secs (from %d to %d)', 
        tripid, to_stop, duration, destination_arrival_time, current_time);
    duration = Math.max(duration, 1);
    let _mk = new TrainMarker(coords, duration * 1000, {icon: getIconFor(route)});
    //let _mk = _createTrainMarker(coords, duration * 1000, route);
    console.assert(from_stop != null);
    console.assert(to_stop != null);
    console.assert(current_time != null);

    //console.log(JSON.stringify(_mk));
    _mk.setTrip({
        tripid: tripid,
        from_stop: from_stop,
        to_stop: to_stop,
        to_stop_on: destination_arrival_time
    });
    //console.log(JSON.stringify(_mk));
    console.log(_mk.trip);
    return _mk;
}

// Gets 95% of way to destination and then stalls until 'Approach' or 'At Station'
// confirmation by server within 28 seconds
function _createWaitingTrainMarker(latlong_array, /* Path from origin to destination */
    reach_destination_time, 
    route) {
    return _createTrainMarker(latlong_array, reach_destination_time, route);
}

function createWaitingTrainMarker(tripid, from_stop, to_stop, current_time, destination_arrival_time, route) {
    let mkr = createTrainMarker(tripid, from_stop, to_stop, current_time, destination_arrival_time, route);
    console.log("createWaitingTrainMarker: Called for trip '%s'", tripid);
    mkr.setPercentageCompletion(0.85); // slow at 80% to wait for Trip Update
    return mkr;
}

function _createStationaryTrainMarker(latlong_pos, route) {
    return new TrainMarker([latlong_pos,latlong_pos], [0], {icon: getIconFor(route)});
}

function createStationaryTrainMarker(tripid, stop_id, route, to_stop_if_any, from_stop_depart, to_stop_on) {
    let mkr = _createStationaryTrainMarker(station_to_latlng[stop_id], route);
    mkr.setTrip({
        tripid: tripid,
        from_stop: stop_id,
        from_stop_depart: from_stop_depart,
        to_stop: to_stop_if_any,
        to_stop_on: to_stop_on
    })
    console.log(mkr);
    return mkr;
}

function getTripsAtStop(stop_id) {
    for (let i in tracked_trips) {
        let trip = tracked_trips[i];
        if (trip.trip.from_stop == stop_id) {
            //TODO
        }
    }
}

function getTimeStringFromUnixTimestamp(ts) {
    return new Date(ts * 1000).toLocaleTimeString();
}

function getFriendlyStopName(gtfs_stop_name) {
    return "{0} ({1})".format(station_to_name[gtfs_stop_name], gtfs_stop_name)
}

var TStatus = {
    AtStation: 0,
    Approaching: 1,
    Enroute: 2
}

// Called when train is marked as 'Approaching'
function progressTrainMarkerToStation(marker, from_station, to_station, current_time, arrival_time) {
    arrival_time -= 5
    // if (marker.isEnded()) {
    //     console.warn('attempted to progress a train that was ended for trip: ' + marker.trip.tripid);
    // }

    console.assert(from_station != null, "from_station is null");
    console.assert(to_station != null, "to_station is null");

    if (marker.trip.to_stop == to_station) { // we are scheduled to get to this station
        if (marker.isEnded()) { // marker has not started journey, is sitting at last station
            // if (marker.trip.current_status != TStatus.AtStation) {
            //     console.warn('progress(): ended marker should be at station, moving.');
            //     moveTrainMarkerToStation(marker, from_station);
            // }
            
            //console.assert(marker.trip.current_status == TStatus.AtStation);
            let poly = findConnectingPolysRecursive(from_station, to_station);
            let coords = longLatToLatLngArr(poly, from_station)
            let duration = Math.min(4, arrival_time - current_time);
            marker.initialize(coords, duration * 1000, -1);
            // no arrival delay
            // console.debug('progress(%s): no need to progress train to station %s from %s, already there.',
            //     marker.trip.tripid, getFriendlyStopName(to_station), getFriendlyStopName(from_station));
        } else { // updating existing arrival progress
            let duration = Math.min(1, arrival_time - current_time);
            console.debug('progress(%s): updated duration for trip from "%s" to "%s" to %d secs',
                marker.trip.tripid, getFriendlyStopName(from_station), getFriendlyStopName(to_station), duration);
            marker.updateDuration(duration * 1000);    
        }
    } else { // we are not already scheduled to get to this station
        console.error('progress(%s): cannot progress a trip when the destination stop does not match: %s != %s', marker.trip.to_stop, to_stop);
        console.debug('... to_station=%s, from_station=%s, marker.trip:', from_station, to_station);
        console.debug(marker.trip);
        throw 'progress() destination mismatch';
    }

    // let duration = Math.max(1, arrival_time - current_time);
    // console.debug('progress(%s): updated trip duration from %s to %s to %d secs',
    //     marker.trip.tripid, getFriendlyStopName(from_station), getFriendlyStopName(to_station), duration);
    
    // if (marker.trip.to_stop != undefined) {
    //     console.assert(marker.trip.to_stop == station_id);
    //     let dur = Math.max(1, arrival_time - current_time);
    //     console.debug('Updated duration for trip "%s" to %d', marker.trip.tripid, dur);
    //     marker.updateDuration(dur * 1000)
    //     // above clears any setPercentages()
    // } else {
    //     console.warn('Cannot progress "%s" because marker.trip.to_stop was undefined', marker.trip.tripid);
    // }
    // console.log('TU: APPROACHING  ' + marker.trip.tripid + ' to station ' + station_id)
    // // marker.trip.to_stop = station_id; // we will go all the way to the station
    // // marker.trip.to_stop_on = arrival_time;
    // // //marker.trip.from_stop = station_id; // other code should write this
    return marker;
}

// Called when train is marked as 'At Stop'
// teleports TrainMarker to a specific station
function moveTrainMarkerToStation(marker, station_id) {
    if (marker.trip.from_stop == null || marker.trip.from_stop == station_id || true) { // don't know where we are
        // marker.moveTo(station_to_latlng[station_id], 500)
        marker.stop();
        marker.setLatLng(station_to_latlng[station_id]);
    } else {
        marker.updateDuration(1 * 1000);
    }
    
    console.log('would move marker %s to %s', marker, station_id);
    // console.log(marker);
    // marker.trip.to_stop = station_id;
    // marker.trip.to_stop_on = undefined;
    // marker.trip.from_stop = station_id;
    return marker;
}

// Called when train is marked as 'Enroute To'
// Halts when 80% of way to station because by then we should have called
// progressTrainMarkerToStation() or moveTrainMarkerToStation() on this trip
function travelTrainMarkerToStation(marker, from_station, to_station, current_time, arrival_time, tooltip) {
    console.assert(to_station != null);
    console.assert(arrival_time != null);
    console.assert(current_time != null);

    console.log('travel(%s): Start trip animation to %s from %s, ',
        marker.trip.tripid, getFriendlyStopName(from_station), getFriendlyStopName(to_station));
    // console.log(marker);
    // if (marker.trip.to_stop != station_id) {
    //     marker.trip.from_stop = marker.trip.to_stop;
    //     marker.trip.to_stop = station_id;
    //     marker.trip.to_stop_on = arrival_time;
    // }
    
    let duration = arrival_time - current_time;
    console.log('travel(%s): Start trip animation to %s from %s will arrive in %d secs (from %d to %d)', 
        marker.trip.tripid, to_station, from_station, duration, arrival_time, current_time);
    duration = Math.max(duration, 1);

    if (duration < 5) {
        console.log("Because arrival time is less than 5 seconds, we will have the train approach the station and skip holds.");
        marker.trip.extra_str = "<br />Arrival Time < 5 sec, called travel() called progress() marker";
        return progressTrainMarkerToStation(marker, from_station, to_station, current_time, arrival_time);
    }

    if (marker.trip.to_stop == to_station) { // we're scheduled to arrive at this station
        if (marker.isEnded()) { // not yet started traveling, new journey
            if (marker.trip.current_status != TStatus.AtStation) {
                console.debug('travel(%s): marker now traveling to stop %s from %s, will arrive in %d secs', 
                marker.trip.tripid, getFriendlyStopName(to_station), getFriendlyStopName(from_station), duration);
                moveTrainMarkerToStation(marker, from_station);
            }
            
            //console.assert(marker.trip.current_status == TStatus.AtStation);
            let poly = findConnectingPolysRecursive(from_station, to_station);
            let coords = longLatToLatLngArr(poly, from_station);
            marker.initialize(coords, duration * 1000, -1);
            marker.setPercentageCompletion(0.8);
        } else { // update existing trip progress
            console.debug('travel(%s): updating existing STU to %d secs', 
                marker.trip.tripid, duration);
            marker.updateDuration(duration * 1000);
        }
    } else { // we are not already scheduled to get to this station
        console.warn('travel(%s): We are now traveling to a new destination before reaching the last!',
            marker.trip.tripid);
        console.error('travel(%s): the destination stop does not match: %s != %s', marker.trip.to_stop, to_stop);
        console.debug('... to_station=%s, from_station=%s, marker.trip:', from_station, to_station);
        console.debug(marker.trip);
        moveTrainMarkerToStation(marker, from_station);

        throw 'progress() destination mismatch';
    }

    // console.assert(from_station != undefined);
    // if (marker.trip.from_stop != station_id) {
    //     console.assert(marker.isEnded());
    //     let poly = findConnectingPolysRecursive(from_station, to_station);
    //     let coords = longLatToLatLngArr(poly, marker.trip.from_stop)   
    //     marker.initialize(coords, duration * 1000, -1);
    //     marker.setPercentageCompletion(0.8);
    // } else { // update in how long the trip will take to arrive, train is still 'Enroute To'
    //     console.log("Updated existing 'Enroute To' time.");
    //     marker.updateDuration(duration * 1000);
    //     // marker.setPercentageCompletion(0.90)
    //     // // use a higher % this time since we got recent data, likelihood of getting new data
    //     // // in time for train to approach stop is small so we must assume instead
    //     // actually, we can't do the above due to technical reasons
    // }
    
    return marker;
}


// setInterval(function() {
//     if (tr % 2 == 0)
//         map.flyTo([40.8291654,-73.8692907], 16);
//     else
//         map.flyTo([40.8656581,-73.8956842], 16);
//     tr++;
// }, 5000);

// RANDOM STOP PLANNER CODE

function randomValueOf( obj ) {
    var keys = Object.keys(obj);
    var len = keys.length;
    var rnd = Math.floor(Math.random()*len);
    var key = keys[rnd];
    return obj[key];
}

function randomStopPanner() {
    setInterval(function() {
        let rv = randomValueOf(this.station_to_latlng)
        console.warn('flying to ' + JSON.stringify(rv));
        map.flyTo(rv, 15);
    }, 10000);
}

function goTo(lat, long, zoom) {
    map.flyTo(lat, long, zoom);
}

function goToTrip(tripId) {
    if (tracked_trips[tripId]) {
        var tt = tracked_trips[tripId];
        map.flyTo(tt.marker.getLatLng(), 14);
        // var m =tt.marker._latlng;
        // goTo(m.lat, m.lng, 13);
    }
}

// randomStopPanner();

// END RANDOM STOP PLANNER