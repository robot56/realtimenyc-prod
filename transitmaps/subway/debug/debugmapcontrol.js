var map = L.map('map').setView([40.740, -73.950], 12);

var tileservers = [
    "https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png",
    "http://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png",
    "http://cartodb-basemaps-{s}.global.ssl.fastly.net/rastertiles/voyager/{z}/{x}/{y}.png"
]

var tileserver = tileservers[0];

var tile = L.tileLayer(tileserver, {
	attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>',
	subdomains: 'abcd',
	maxZoom: 19
});

// tile = L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
//     maxZoom: 20,
//     subdomains:['mt0','mt1','mt2','mt3']
// });

tile.addTo(map);

L.control.locate().addTo(map);

function getColorForRoute(route_id) {
    let map = {
        '1': 'red',
        '2': 'red',
        '3': 'red',
        '4': 'green',
        '5': 'green',
        '6': 'green',
        '7': 'purple',
        'A': 'blue',
        'C': 'blue',
        'E': 'blue',
        'B': 'orange',
        'D': 'orange',
        'F': 'orange',
        'M': 'orange',
        'N': 'yellow',
        'Q': 'yellow',
        'R': 'yellow',
        'W': 'yellow',
        'G': 'lightgreen',
        'J': 'brown',
        'Z': 'brown'
    }
    return map[route_id] || 'white';
}

var polyline_to_stopstop_list = [
    // {
    //     "stop1": "GTFSSTOPID",
    //     "stop2": "GTFSSTOPID",
    //     "polys": ["id1","id2","..."]
    // }
]

function getLineNameForRoute(route_id) {
    let map = {
        '1': 'Seventh Avenue Line<br/>1, 2, 3',
        '4': 'Lexington Avenue Line<br/>4, 5, 6',
        '7': 'Flushing Line',
        'A': 'Eigth Avenue Line<br/>A, C, E',
        'B': 'Sixth Avenue Line<br/>B, D, F, M'
    };
    return map[route_id] || route_id;
}

var stopmarker_layer = L.layerGroup().setZIndex(3);

function onLineHighlight(e) {
    // console.log('caleld onRouteHighlight');
    var layer = e.target;

    layer.setStyle({
        weight: 6
    });

    layer.bringToFront();
}

function onLineUnhighlight(e) {
     console.log('caleld unRouteHighlight');
    var layer = e.target;

    layer.setStyle({
        weight: 3
    });
}

var ommitted_five_line_features = []

function findCorrespondingStation(lat, lng, route) {
    lat = lat.toFixed(4).slice(0,-1);
    lng = lng.toFixed(4).slice(0,-1);
    let routes = route.replace(/-/g, " ").split(" ")

    var l = subwaystationscsv.split("\n");

    var matches = 0;

    for (var i in l) {
        var line = l[i].split(",");
        var slat = Number(line[9]).toFixed(4).slice(0,-1);
        var slng = Number(line[10]).toFixed(4).slice(0,-1);
        var daylightroutes = line[7].split(" ");
        var sname = line[5];
        // 

        //if (daylightroutes.indexOf(route) > -1)
            //console.log("Daylight routes: %s - Slat: %s, Lat: %s", 
            //daylightroutes, slat, lat);
        
        if (slat == lat && slng == lng) {
            var found = false;
            for (var i in routes) {
                if (daylightroutes.includes(routes[i])) {
                    found = true;
                    return line;
                }
            }
            if (found)
                matches++;
            /*console.warn('found lat lng match but no route match')
            console.log(routes);
            console.log(daylightroutes);*/
        }
            //console.log('MATCH lat: ' + lat + ' lng: ' + lng + ' -- ' + line[5]);
            //throw Error('');
            //return true;
    }
    //console.log('not found "%s" in "%s"', route, daylightroutes);
    return [];
}

for (var subwayroute in subwayMappedGeoJson) {
    var styles = {
        "color": getColorForRoute(subwayroute),
    };
    var features = subwayMappedGeoJson[subwayroute];
    var parsed_features_list = [];
    
    var _features = []
    
    for (var i in features) {
        var feature = features[i];

        var foundcs = false;
    
        var amount_found = 0;
        var matches = {};

        if (false)
        for (var i in feature.geometry.coordinates) {
            var latlng = feature.geometry.coordinates[i];
            //console.log('lat: ' + latlng[0] + ' lng: ' + latlng[1])
            var match = findCorrespondingStation(latlng[1], latlng[0], feature.properties.name.replace("-", " "));
            if (Object.keys(matches).length == 1 && (feature.geometry.coordinates.length+1) == i)
                throw "error";
            if (match.length != 0) {
                foundcs = true;
                amount_found++;
                matches[match[0]]=match;
            }
        }

        var matches_count = Object.keys(matches).length;
        console.log('number of matches (should be 2): %d - ' + JSON.stringify(matches), matches_count)

        if (!foundcs || matches_count != 2) {
            _features.push(feature);
            console.log(feature.properties.name + ' did not find:')
            console.log(feature)

            var geoj_layer2 = L.geoJson(feature, {style: styles});
            geoj_layer2.addTo(map);
            geoj_layer2
                .on('mouseover', onLineHighlight)
                .on('mouseout', onLineUnhighlight);
            geoj_layer2.bindTooltip(feature.properties.id + "." + feature.properties.objectid, {sticky: true, direction: 'auto'});        
        }

        var geoj_layer2 = L.geoJson(feature, {style: styles});
        geoj_layer2.addTo(map);
        geoj_layer2
            .on('mouseover', onLineHighlight)
            .on('mouseout', onLineUnhighlight);
        geoj_layer2.bindTooltip(feature.properties.id + "." + feature.properties.objectid, {sticky: true, direction: 'auto'});        

    
        if (matches_count > 0) {
            console.log(Object.keys(matches))
            var m1 = matches[Object.keys(matches)[0]];
            if (matches_count == 2)
                var m2 = matches[Object.keys(matches)[1]];
            else
                var m2 = "";
            polyline_to_stopstop_list.push({
                "stop1": m1[2],
                "stop2": m2[2],
                "polys": [feature.properties.id + "." + feature.properties.objectid]
            });
            // console.log(JSON.stringify(polyline_to_stopstop_list));
        }

        /*if (feature.properties.name == "5") {
            //console.log("feature on the 5 line")
            //console.log(_features)
            ommitted_five_line_features.push(feature)
        }
        else _features.push(feature);*/
        //console.log(feature)
    }
    // var geoj_layer = L.geoJson(_features, {style: styles});
    // geoj_layer.addTo(map);
    // geoj_layer
    //     .on('mouseover', onLineHighlight)
    //     .on('mouseout', onLineUnhighlight);
    // geoj_layer.bindTooltip(getLineNameForRoute(subwayroute), {sticky: true, direction: 'auto'});
}

var l = subwaystationscsv.split("\n");
for (var i in l) {
    var line = l[i].split(",");
    var lat = line[9];
    var lng = line[10];
    var latlng = new L.latLng(lat, lng);
    stopmarker_layer.addLayer(L.circleMarker(latlng).setRadius(5).bindTooltip(line[2] + "<br/>" + l[i]));
 //   console.log('lat: ' + lat + ' lng: ' + lng);
}
// stopmarker_layer.addTo(map);

var lastzoom = 0;

//var TrainIcon = 

map.on('zoomend', function(e) {
    var zoom = e.target.getZoom();
	if (zoom <= 14 && lastzoom >= 14) {
		map.removeLayer(stopmarker_layer);
	} else if (zoom >= 14 && lastzoom <= 14) {
		map.addLayer(stopmarker_layer);
    }
    lastzoom = zoom;
});

var trainIcon6 = L.icon({
    iconUrl: 'http://subwaytime.mta.info/img/6.png',
    iconSize: [32, 32], // size of the icon
});

var trainIcon6X = L.icon({
        iconUrl: 'https://upload.wikimedia.org/wikipedia/commons/0/0c/NYCS-bull-trans-6d.svg',
        iconSize: [34, 34], // size of the icon
});

var trainIcon2 = L.icon({
    iconUrl: 'http://subwaytime.mta.info/img/2.png',
    iconSize: [32, 32], // size of the icon
});

var busIconQ44SBS = L.icon({
    iconUrl: 'http://127.0.0.1/www/realtime.nyc/_localdemos/bussvg.php?direction=s&text=Q44&color=red&tcolor=white',
    iconSize: [64, 64], // size of the icon
});

var mm = L.Marker.movingMarker([[40.805368, -73.914042], [40.833226, -73.860816]], 10000, {icon: trainIcon6}).addTo(map);
mm.start();


L.marker([40.833226, -73.860816], {icon: trainIcon6X}).addTo(map);
//L.marker([40.840295, -73.880049], {icon: trainIcon2}).addTo(map);
L.marker([40.840295, -73.880049], {icon: busIconQ44SBS}).addTo(map);


// setInterval(function() {
//     if (tr % 2 == 0)
//         map.flyTo([40.8291654,-73.8692907], 16);
//     else
//         map.flyTo([40.8656581,-73.8956842], 16);
//     tr++;
// }, 5000);