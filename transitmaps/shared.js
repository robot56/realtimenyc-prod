var QueryString = {
    _getParameterByName: function(name) {
        var hashOrSearch = document.location.hash;
        var searchParams = new URLSearchParams(hashOrSearch.substring(1));
        if (searchParams.has(name)) {
            return searchParams.get(name);
        } else {
            let sp = new URLSearchParams(document.location.search.substring(1))
            if (sp.has(name)) {
                return sp.get(name);
            }
            return false;
        }
    },

    getValue: function(key) {
        let val = this._getParameterByName(key);
        if (val == 'undefined') {
            return false;
        }
        return val;
    },

    setValue: function(key, value) {
        var has_key = this._getParameterByName(key);
        if (!has_key) {
            document.location.hash += '&' + key + '=' + value;
        } else {
            if (document.location.hash.includes(key)) {
                var exp = new RegExp(key + '(=)?([\w\d]+)?', 'g')
                let hp = new URLSearchParams(document.location.hash.substring(1));
                hp.set(key, value);
                //document.location.hash = document.location.hash.replace(exp, key + '=' + value);
                document.location.hash = hp.toString();
            } else {
                // we cannot update the search string without refreshing
                // so just allow the user to 'permalink' or 'bookmark link'
                // the page with the function below
                document.location.hash += '&' + key + '=' + value;
            }
        }
    },

    remove: function(key) {
        var hp = new URLSearchParams(document.location.hash.substring(1));
        var sp = new URLSearchParams(document.location.search.substring(1));

        if (sp.has(key)) {
            hp.set(key, 'undefined');
        } else {
            hp.delete(key);
        }
        document.location.hash = hp.toString();
    },

    permalink: function() {
        // let ss = window.location.search;
        // let items = ss.split("&");
        // for (var i in items) {
        //     let item = items[i];
        //     let kv = item.split('=');
        // }

        // these are newer, but they're already implemented for us
        // so no need to re-invent the wheel...
        let sp = new URLSearchParams(window.location.search.substring(1));
        let hp = new URLSearchParams(window.location.hash.substring(1));

        for (var p of hp) {
            if (p[1] == 'undefined')
                sp.delete(p[0])
            else
                sp.set(p[0], p[1]);
        }

        return window.location.origin + window.location.pathname + '?' + sp.toString()
    }
}

function executeOnReadyFrame($frame, callback) {
    var currentState = $frame.contentDocument.readyState;
    if (currentState == 'complete') {
        callback();
    } else {
        $frame.contentWindow.onload = callback;
    }
}

if(!String.prototype.format){
    String.prototype.format=function(){var args=arguments;return this.replace(/{(\d+)}/g,function(match,number){return typeof args[number]!='undefined'?args[number]:match})}}