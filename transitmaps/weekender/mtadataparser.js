function parsePlannedWorkXml(data) {
    // Yes, this is stupid to do this as regex and not use an XML parser
    // However, because the plannedwork.xml provided by the MTA is considered
    // "invalid" XML pretty much by the browser and alot of the XML parsers out
    // there, the /simple/ options were to 1. use regex or 2. try to fix the xml
    // I did not like option 2 (which seemed problematic with little feed data 
    // insight, and there could be other issues down the line), 
    // so option 1 was the way to go...
    //var statuses = new RegExp(/\<Status\>(.*)\<\/Status\>/g);

    var statuses_re = /\<Status\>(.*?)\<\/Status\>/g;
    var routeinfo_re = /<Routeinfo>(.*?)<\/Routeinfo>/g;
    var stopinfo_re = /<Stopinfo>(.*?)<\/Stopinfo>/g;

    console.log(data);

    var processStatus = function(statusstr) {
        var title = /<Title>(.*)<\/Title>/.exec(statusstr);
        var Statustype = /<Statustype>(\w+)<\/Statustype>/.exec(statusstr);
        var Id = /<Id>(\d+)<\/Id>/.exec(statusstr);
        var begins = /<Begins>(.*)<\/Begins>/.exec(statusstr);
        var lastupdate = /<Lastupdate>(.*)<\/Lastupdate>/.exec(statusstr);
		var affectstops = /<Affectsstops>(\d+)<\/Affectsstops>/.exec(statusstr);
		var affectsroutes = /<Affectsroutes>(.*)<\/Affectsroutes>/.exec(statusstr)
		var doc = /<Document>(.*)<\/Document>/.exec(statusstr)
		var texts = /<Text>(.*)<\/Text>/.exec(statusstr);
		var cats = /<Categories><\/Categories>/.exec(statusstr);
		var summary = /<Summary>(.*)<\/Summary>/.exec(statusstr);
        
        console.log('PLANNED WORK "%s" (%s #%s) - %s', title[1], Statustype[1], Id[1], summary[1]);
    }

    var processRoute = function(route) {
        var _id = /<Id>(\d+)<\/Id>/;
        var _route = /<Route>(.*)<\/Route>/;
        var _direction = /<Direction>(.*)<\/Direction>/;
        
        console.log('Matched route')
    }

    var id_to_stopid_map = {};
    var stopid_to_station_data = {};

    var processStop = function(stopstr) {
        var _id = /<Id>(\d+)<\/Id>/.exec(stopstr);
		var _stop = /<Stop>(\d+)<\/Stop>/.exec(stopstr)
		var _atisstopid = /<Atisstopid>(\d+)<\/Atisstopid>/.exec(stopstr)
		var _stopid = /<Stopid>(\d+)<\/Stopid>/.exec(stopstr)
		var _status = /<State>(.*)<\/State>/.exec(stopstr)
		var _special = /<Special>(\w)<\/Special>/.exec(stopstr)
		var _route = /<Route>(.*)<\/Route>/.exec(stopstr)
        var _direction = /<Direction>(.*)<\/Direction>/.exec(stopstr)

        if (_route[1].length > 1 || !_stopid) {
            console.log('ignoring bus or invalid stop id')
            return; // we ignore busses.
        }

        console.log(_id)
        console.log(_stopid)
        
        console.log('id=%s, stopid=%s', _id.toString(), _stopid.toString())
        id_to_stopid_map[_id[1]] = _stopid[1]
        if (stopid_to_station_data[_stopid[1]] == undefined) {
            var station_data = ATISStopIDMap[_atisstopid[1] + '_' + _route[1]];
            if (station_data == undefined) {
                for (var i = 0; i < 26; i++) {
                    var chr = String.fromCharCode(65 + i);
                    //console.warn(chr);
                    if (station_data == undefined)
                        station_data = ATISStopIDMap[_atisstopid[1] + '_' + chr]
                    else break;
                }
                for (var i = 0; i < 9; i++) {
                    if (station_data == undefined)
                        station_data = ATISStopIDMap[_atisstopid[1] + '_' + i]
                    else break;
                }
            }
            //console.log(station_data);
            stopid_to_station_data[_stopid[1]] = station_data;
        }

        /*if (!gtfs_stop_name)
            console.warn('MATCH FAIL %s %s_%s', _id[1], _atisstopid[1], _route[1]);
        else
            console.log('[%s] ATISStopID %s = %s', _id[1], _atisstopid[1], gtfs_stop_name[5]);*/
    }

    var match;
    do {
        match = statuses_re.exec(data);
        if (match) {
            console.log(match);
            processStatus(match[0]);
        }
    } while (match);

    var stopmatch;
    do {
        stopmatch = stopinfo_re.exec(data);
        if (stopmatch) {
            console.log(stopmatch);
            processStop(stopmatch[1]);

        }
    } while (stopmatch);

    console.log(id_to_stopid_map)
    for (var i in id_to_stopid_map) {
        let stopid = id_to_stopid_map[i];
        let station_data = stopid_to_station_data[stopid];
        console.log('[%s] StopID = %s, StationData = %s', i, stopid, station_data)
    }

    //console.log(data.search(statuses));
}

$.ajax({
    url: 'plannedwork.txt',
    // dataType: 'xml',
    success: function(data){
        // Extract relevant data from XML
        console.log('ajax(plannedwork.xml): ok');
        // https://stackoverflow.com/a/10805292
        parsePlannedWorkXml(data.replace(/\r?\n|\r/g, ""));
    },
    error: function(data){
        console.log('Could not load MTA data');
    }
});