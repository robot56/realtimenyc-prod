var map = L.map('map').setView([40.740, -73.950], 12);

var tileservers = [
    "https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png",
    "http://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png",
    "http://cartodb-basemaps-{s}.global.ssl.fastly.net/rastertiles/voyager/{z}/{x}/{y}.png"
]

var tileserver = tileservers[1];

var tile = L.tileLayer(tileserver, {
	attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>',
	subdomains: 'abcd',
	maxZoom: 19
});


tile.addTo(map);

L.control.locate().addTo(map);

function getColorForRoute(route_id, shifted) {
    let map = {
        '1': 'red',
        '2': 'red',
        '3': 'red',
        '4': 'green',
        '5': 'green',
        '6': 'green',
        '7': 'purple',
        'A': 'blue',
        'C': 'blue',
        'E': 'blue',
        'B': 'orange',
        'D': 'orange',
        'F': 'orange',
        'M': 'orange',
        'N': 'yellow',
        'Q': 'yellow',
        'R': 'yellow',
        'W': 'yellow',
        'G': 'lightgreen',
        'J': 'brown',
        'Z': 'brown'
    }
    if (shifted) {
        if ((Math.random()) >= 0.5)
            return 'lightgreen';
        else
            return 'black';
    }
    return map[route_id] || 'white';
}

function getLineNameForRoute(route_id) {
    let map = {
        '1': 'Seventh Avenue Line<br/>1, 2, 3',
        '4': 'Lexington Avenue Line<br/>4, 5, 6',
        '7': 'Flushing Line',
        'A': 'Eigth Avenue Line<br/>A, C, E',
        'B': 'Sixth Avenue Line<br/>B, D, F, M'
    };
    return map[route_id] || route_id;
}

var stopmarker_layer = L.layerGroup().setZIndex(3);

function onLineHighlight(e) {
    // console.log('caleld onRouteHighlight');
    var layer = e.target;

    layer.setStyle({
        weight: 6
    });

    layer.bringToFront();
}

function onLineUnhighlight(e) {
     console.log('caleld unRouteHighlight');
    var layer = e.target;

    layer.setStyle({
        weight: 3
    });
}

var lines_layergroup = L.layerGroup();

function drawRoutes(routes) {
    map.removeLayer(lines_layergroup);
    map.removeLayer(stopmarker_layer);
    lines_layergroup = L.layerGroup();
    stopmarker_layer = L.layerGroup();
    console.log('removed layers')
    for (var subwayroute in subwayMappedGeoJson) {
        
        if (routes != undefined)
            if (!routes.includes(subwayroute))
                continue;
        let col = getColorForRoute(subwayroute, routes==undefined);
        var styles = {
            "color": col,
            "weight": col == "black" ? 6 : 3
        };
        var features = subwayMappedGeoJson[subwayroute];
        var parsed_features_list = [];
        for (var i in features) {
            var feature = features[i];
            //console.log(feature)
        }
        var geoj_layer = L.geoJson(features, {style: styles});
        geoj_layer.addTo(lines_layergroup);
        if (col == 'black') {
            console.log('black')
            geoj_layer.bringToFront();
        }
        else
            geoj_layer.bringToBack();
        // geoj_layer
        //     .on('mouseover', onLineHighlight)
        //     .on('mouseout', onLineUnhighlight);
        if (routes == undefined)
            geoj_layer.bindTooltip(getLineNameForRoute(subwayroute), 
                {sticky: true, direction: 'auto'});
    }

    lines_layergroup.addTo(map);
}

drawRoutes();

var station_to_latlng = {};
var station_to_name = {};

var showlabels = location.hash.includes("showlabels") ? true : false;
var showlabelicons = location.hash.includes("showlabels=2") ? true : false;

function getSvgIconUrlFor(route) {
    var line_to_svgurl_map = {
        '2': 'https://upload.wikimedia.org/wikipedia/commons/6/61/NYCS-bull-trans-2.svg',
        '6': 'https://upload.wikimedia.org/wikipedia/commons/5/53/NYCS-bull-trans-6.svg',
        '6X': 'https://upload.wikimedia.org/wikipedia/commons/0/0c/NYCS-bull-trans-6d.svg',
        '4': 'https://upload.wikimedia.org/wikipedia/commons/e/ec/NYCS-bull-trans-4.svg',
        '5': 'https://upload.wikimedia.org/wikipedia/commons/7/78/NYCS-bull-trans-5.svg',
        '5X': 'https://upload.wikimedia.org/wikipedia/commons/f/ff/NYCS-bull-trans-5d.svg',
        '7X': 'https://upload.wikimedia.org/wikipedia/commons/e/e6/NYCS-bull-trans-7d.svg',
        '': 'https://upload.wikimedia.org/wikipedia/commons/8/8a/NYCS-bull-trans-S.svg'
    }
    return line_to_svgurl_map[route] || "http://subwaytime.mta.info/img/" + route + ".png";
}

function drawStopMarkers(match_line, no_fake_markers) {
    var l = subwaystationscsv.split("\n");
    for (var i in l) {
        var line = l[i].split(",");
        
        if (match_line != undefined)
            if (!line[7].includes(match_line)) continue;

        var lat = line[9];
        var lng = line[10];
        station_to_latlng[line[2]] = [lat, lng];
        station_to_name[line[2]] = line[5];
        var latlng = new L.latLng(lat, lng);
        
        var showlabels = Math.random() <= 0.2;
        
        if (!showlabels || no_fake_markers) {
            var marker = L.circle(latlng).setRadius(32);
            marker.bindTooltip('<strong>' + line[5] + '</strong>');
        } else {
            var lineiconstr = ""
            if (line[7] != undefined) {
                var svcs = line[7].split(" ")
                for (var j in svcs) {
                    var svc = svcs[j];
                    lineiconstr += '<img src="{0}" style="width:22px;height:22px;vertical-align:middle;" />'.format(getSvgIconUrlFor(svc));
                }    
            }
            //var tagline = ['Planned Work<br/>WED, FRI 4pm-6pm', 'subway-stop-label-notice']
            //var tagline = ['No Service<br/>Today 4pm-6pm', 'subway-stop-label-important']
            var tagline = []
            var marker = new L.Marker(latlng, {
                icon: new L.DivIcon({
                  className: 'subway-stop-dot-alwaysshown',
                  html: '<span class="subway-stop-label {0}">'.format(tagline[1])+ line[5] +' {0}<br/>{1} </span>'.format(showlabelicons ? lineiconstr : '(' + line[7] + ')', tagline[0])
                })
            })
        }
    //     marker.setZIndexOffset(-1);
    //     let svg = `<svg width="260" height="60" xmlns="http://www.w3.org/2000/svg">
    //     <style>text { font: italic 40px serif; fill: red; }</style>
    //     <text x="1" y="40" class='station-name'>Parkchester</text>
    // </svg>`;
        // let marker = L.marker(latlng, {
        //     'icon': L.icon({
        //         iconUrl: 'data:image/svg+xml;base64,' + btoa(svg),
        //         // iconUrl: 'https://upload.wikimedia.org/wikipedia/commons/0/0c/NYCS-bull-trans-6d.svg'
        //     })
        // });
        stopmarker_layer.addLayer(marker);
        //console.log('lat: ' + lat + ' lng: ' + lng);
    }    
}

drawStopMarkers()
// stopmarker_layer.addTo(map);

// map.addLayer(stopmarker_layer);

var lastzoom = 0;

map.on('zoomend', function(e) {
    var zoom = e.target.getZoom();
	if (zoom <= 13 && lastzoom >= 13) {
		map.removeLayer(stopmarker_layer);
	} else if (zoom >= 13 && lastzoom <= 13) {
		map.addLayer(stopmarker_layer);
    }
    // if (zoom <= 16 && lastzoom >= 16) {
    //     // stopmarker_layer.eachLayer(function(layer) {
    //     //     layer.setRadius(5);
    //     // })
    //     map.removeLayer(stop_group);
    // } else if (zoom >= 16 && lastzoom <= 16) {
    //     // stopmarker_layer.eachLayer(function(layer) {
    //     //     layer.setRadius(14);
    //     // })
    //     map.addLayer(stop_group);
    // }
    lastzoom = zoom;
});

function toggleSidebar() {
    if ($('#map').hasClass('map-full')) {
        $('.contents').removeClass('contents-closed')
        $('#map').removeClass('map-full');
        map.invalidateSize(); // update map    
    } else {
        $('.contents').addClass('contents-closed');
        $('#map').addClass('map-full');
        map.invalidateSize(); // update map
    }
}

L.easyButton('fa-arrow-left', function(btn, map){
    toggleSidebar();
}).addTo(map);

$('#x-viewmap').click(function() {
    toggleSidebar();
})

var last_clicked = '';

function bulletReset() {
    // console.log('resetting')
    $('.bullet-lcl').removeClass('bullet-lcl-disabled');
    drawRoutes();
    drawStopMarkers()
}

function bulletClick(line, linegroup) {

    // if ($('.bullet-lcl-disabled').length > 0) {
    //     if (!$('#sb-bullet-'+line).hasClass('bullet-lcl-disabled')) {
    //         bulletReset();
    //         return;
    //     }
    // }

    if (last_clicked == line) {
        bulletReset();
        last_clicked = '';
        return;
    } else {
        last_clicked = line;
    }

    $('.bullet-lcl').addClass('bullet-lcl-disabled');
    $('#sb-bullet-'+line).removeClass('bullet-lcl-disabled')
    map.setView([40.740, -73.950], 12);
    drawRoutes(linegroup);
    drawStopMarkers(line, true)
    map.addLayer(stopmarker_layer);
    if (screen.width < 600) {
        toggleSidebar();
    }
}