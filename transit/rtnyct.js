var SubwayIconSVGMap = {
    '1': 'https://upload.wikimedia.org/wikipedia/commons/3/3f/NYCS-bull-trans-1.svg',
    '2': 'https://upload.wikimedia.org/wikipedia/commons/6/61/NYCS-bull-trans-2.svg',
    '3': 'https://upload.wikimedia.org/wikipedia/commons/2/25/NYCS-bull-trans-3.svg',
    '4': 'https://upload.wikimedia.org/wikipedia/commons/e/ec/NYCS-bull-trans-4.svg',
    '5': 'https://upload.wikimedia.org/wikipedia/commons/7/78/NYCS-bull-trans-5.svg',
    '5X': 'https://upload.wikimedia.org/wikipedia/commons/f/ff/NYCS-bull-trans-5d.svg',
    '6': 'https://upload.wikimedia.org/wikipedia/commons/5/53/NYCS-bull-trans-6.svg',
    '6X': 'https://upload.wikimedia.org/wikipedia/commons/0/0c/NYCS-bull-trans-6d.svg',
    '7': 'https://upload.wikimedia.org/wikipedia/commons/a/a0/NYCS-bull-trans-7.svg',
    '7X': 'https://upload.wikimedia.org/wikipedia/commons/e/e6/NYCS-bull-trans-7d.svg',
    'A': 'https://upload.wikimedia.org/wikipedia/commons/0/0c/NYCS-bull-trans-A.svg',
    'C': 'https://upload.wikimedia.org/wikipedia/commons/0/03/NYCS-bull-trans-C.svg',
    'E': 'https://upload.wikimedia.org/wikipedia/commons/2/2f/NYCS-bull-trans-E.svg',
    'B': 'https://upload.wikimedia.org/wikipedia/commons/f/fb/NYCS-bull-trans-B.svg',
    'D': 'https://upload.wikimedia.org/wikipedia/commons/3/39/NYCS-bull-trans-D.svg',
    'F': 'https://upload.wikimedia.org/wikipedia/commons/c/cb/NYCS-bull-trans-F.svg',
    'M': 'https://upload.wikimedia.org/wikipedia/commons/1/1d/NYCS-bull-trans-M.svg',
    'N': 'https://upload.wikimedia.org/wikipedia/commons/3/37/NYCS-bull-trans-N.svg',
    'Q': 'https://upload.wikimedia.org/wikipedia/commons/8/84/NYCS-bull-trans-Q.svg',
    'R': 'https://upload.wikimedia.org/wikipedia/commons/c/ce/NYCS-bull-trans-R.svg',
    'W': 'https://upload.wikimedia.org/wikipedia/commons/c/c1/NYCS-bull-trans-W.svg',
    'G': 'https://upload.wikimedia.org/wikipedia/commons/e/ee/NYCS-bull-trans-G.svg',
    'J': 'https://upload.wikimedia.org/wikipedia/commons/f/f4/NYCS-bull-trans-J.svg',
    'Z': 'https://upload.wikimedia.org/wikipedia/commons/b/b7/NYCS-bull-trans-Z.svg',
    'L': 'https://upload.wikimedia.org/wikipedia/commons/b/bd/NYCS-bull-trans-L.svg',
    'S': 'https://upload.wikimedia.org/wikipedia/commons/8/8a/NYCS-bull-trans-S.svg',
    '': 'https://upload.wikimedia.org/wikipedia/commons/8/8a/NYCS-bull-trans-S.svg'
}

var RTNYC = {};

RTNYC.Subway = {
    subwayStations: {},
    subwayComplexes: {},
    stationToLatLng: {},
    stationToName: {},

    init: function() {
        try {
            window.stationlines = subwaystationscsv.split("\n");
            let l = window.stationlines;
            for (var i in stationlines) {
                var line = l[i].split(",");
                var lat = line[9];
                var lng = line[10];
                this.stationToLatLng[line[2]] = [lat, lng];
                this.stationToName[line[2]] = line[5];
            }
        } catch (e) {
            console.warn('did not load subway stations GTFS data, please include script')
        }
    },

    getStopIDLatLng: function(stop_id) {
        if (stop_id.length < 3) {
            return null;
        }
        return this.stationToLatLng[stop_id.slice(0,3)];
    },

    getStopIDName: function(stop_id) {
        if (stop_id.length < 3) {
            return null;
        }
        return this.stationToName[stop_id.slice(0,3)];
    },

    getSubwayIconURL: function(train_route) {
        return SubwayIconSVGMap[train_route] || "http://subwaytime.mta.info/img/{0}.png".format(train_route);
    },

    stationSearch: function(search_query, callback) {
        var results = [];

        for (var i in stationlines) {
            let s = stationlines[i].split(",");
    
            let sstopid = s[2];
            var services = [];
            let sname = s[5];
            let lat = s[9];
            let lng = s[10];
            if (search_query) {
                if (!sname.toUpperCase().includes(search_query.toUpperCase()))
                    continue;
            }

            let sservices = s[7].split(' ');
            for (let i in sservices) {
                if (!services.includes(sservices[i]))
                    services.push(sservices[i]);
            }
            services.sort();

            if (callback)
                callback(sstopid, sname, lat, lng, services);
            else
                results.push([sstopid, sname, lat, lng, services]);
        }

        return results;
    },

    getServicesAtStation: function(gtfs_stop_id) {
        let complexid = this.subwayStations[gtfs_stop_id][1];
        let services = []
        for (var stopid of this.subwayComplexes[complexid]) {
            let stop = this.subwayStations[stopid]
            let sservices = stop[7].split(' ');
            for (let i in sservices) {
                if (!services.includes(sservices[i]))
                    services.push(sservices[i]);
            }
            services.sort();
        }
    
        return services;
    }
};

RTNYC.Subway.init();