function fetchSubwayStops(line, callback) {
    var ae = 'http://traintimelb-367443097.us-east-1.elb.amazonaws.com/getStationsByLine/' + line.toUpperCase();
    $.getJSON(ae, function(data) {
        //console.log(data);

        var j = JSON.parse(data);
        callback(j)
    })
}

function loadSubwayStatus(lines) {
    for (var line of lines) {
        console.log(line);
        var ln = line.name;
        if (ln == 'NQR') {
            ln = 'NRQW';
        }
        $ss = $('#subway-status-' + name);
        // switch (line.status) {
        //     case 'GOOD SERVICE':
        //         $ss.
        // }
    }

    
}


function loadMTAStatus() {
    var differential = Math.round((new Date().getTime())/60000);


    $.getJSON('https://cors-proxy.htmldriven.com/?url=http://www.mta.info/service_status_json/' + differential, function (data) {
        console.log(data);
        var body = data.body;
        // fucking javascript
        var j = JSON.parse(JSON.parse(body));
        console.log(j)
        loadSubwayStatus(j.subway.line);
    });
}


/* === SUBWAY DATA LOADING  === */

function getMtaStationStatus(l, stopid, callback) {
    var request_url = 'http://traintimelb-367443097.us-east-1.elb.amazonaws.com/getTime/' + l + '/' + stopid;
    var request = $.get(request_url+'', function(data) {
        console.log(data);
        callback(data);
    });
}

function getStationStatus(gtfs_station_id, callback) {
    getMtaStationStatus(gtfs_station_id[0], gtfs_station_id, function(data) {
        let j = {'Northbound':[], 'Southbound': []}
        let station_name = data['stationName']
        j['NorthboundDirection'] = data.direction1.name;
        for (var i in data.direction1.times) {
            var time = data.direction1.times[i];
            j['Northbound'].push({ServiceId: time.route, DestinationName: time.lastStation, ArrivingOn: time.minutes, TripId: ''})
        }
        j['SouthboundDirection'] = data.direction2.name;
        for (var i in data.direction2.times) {
            var time = data.direction2.times[i];
            j['Southbound'].push({ServiceId: time.route, DestinationName: time.lastStation, ArrivingOn: time.minutes, TripId: ''})
        }
        j['StationName'] = station_name;
        // console.log('TS: %s', data.lastUpdatedOn)
        j['Timestamp'] = parseInt(data.lastUpdatedOn);
        callback(j);
        return;
    })
};

function getMTABusStopData(stop_id, callback) {
    $.getJSON("http://bustime.mta.info/api/siri/stop-monitoring.json?callback=?&key=TEST&OperatorRef=MTA&MonitoringRef=" + stop_id, function(data) {
        console.log(data);
        callback(data);
    })
}

function processVehicleData(items) {
    // console.warn(data);
    let stopName = '';
    let vehicles = [
        /*{
            'Route': 'BX12+',
            'PublishedRoute': 'Bx12-SBS',
            'Direction': '0',
            'OriginStop': '306619',
            'DestinationStop': '801007',
            'DestinationName': 'PELHAM BAY',
            'ActiveAlerts': ['MTA NYCT_203077'],
            'Location': {
                'Latitude': -73.993114,
                'Longitude': 40.661613
            },
            'Bearing': 44.387703,
            'ProgressRate': 'normalProgress',
            'ProgressStatus': 'layover,prevTrip',
            'CurrentStatus': 'ENROUTE | ENROUTE_TO_TERMINAL | AT_TERMINAL',
            'Vehicle': 'MTA NYCT_471',
            'ArrivalTime': 0,
            'DepartureTime': 0,
            'PresentableDistance': '1.2 miles away',
            'StopsAway': 9
        }*/
    ];

    // let stopMonitor = data.Siri.ServiceDelivery.StopMonitoringDelivery[0];
    // let dataUpdatedTime = Date.parse(stopMonitor.ResponseTimestamp);
    for (var item of /*stopMonitor.MonitoredStopVisit*/items) {
        var vehicle = item.MonitoredVehicleJourney;

        let originStop = vehicle.OriginRef;
        originStop = originStop.split('_')[1];
        let destStop = vehicle.DestinationRef;
        destStop = destStop.split('_')[1];

        let activeAlerts = [];
        if (vehicle.SituationRef) {
            for (var sr of vehicle.SituationRef) {
                activeAlerts.push(sr.SituationSimpleRef);
            }
        }

        let mon = vehicle.MonitoredCall;
        if (mon) {
            var currentStop = mon.StopPointRef;
            var currentStopName = mon.StopPointName;
            var arrivalTime = Date.parse(mon.ExpectedArrivalTime);
            var departureTime = Date.parse(mon.ExpectedDepartureTime);
            var arrivalString = mon.Extensions.Distances.PresentableDistance;
            var stopsAway = mon.Extensions.Distances.StopsFromCall;
            stopName = mon.StopPointName;
        } else {
            console.warn('vehicle object has no MonitoredCall: ', vehicle);
        }

        let progressStatus = vehicle.ProgressStatus || '';
        let currentStatus = '';
        let originDepartureTime = 0;


        if (progressStatus.includes('prevTrip')) {
            if (progressStatus.includes('layover')) {
                currentStatus = 'ENROUTE_TO_TERMINAL';
            } else {
                currentStatus = 'AT_TERMINAL';
            }
        } else if (progressStatus.includes('layover')) {
            currentStatus = 'AT_TERMINAL';
        } else {
            currentStatus = 'ENROUTE';
        }

        if (vehicle.OriginAimedDepartureTime) {
            originDepartureTime = Date.parse(vehicle.OriginAimedDepartureTime);
        }

        vehicles.push({
            'Timestamp': item.RecordedAtTime,
            'Route': vehicle.LineRef,
            'PublishedRoute': vehicle.PublishedLineName,
            'Direction': vehicle.DirectionRef,
            'OriginStop': originStop,
            'DestinationStop': destStop,
            'DestinationName': vehicle.DestinationName,
            'ActiveAlerts': activeAlerts,
            'Location': {
                'Latitude': vehicle.VehicleLocation.Latitude,
                'Longitude': vehicle.VehicleLocation.Longitude
            },
            'Bearing': vehicle.Bearing,
            'ProgressRate': vehicle.ProgressRate,
            'ProgressStatus': progressStatus,
            'Vehicle': vehicle.VehicleRef,
            'ArrivalTime': arrivalTime,
            'DepartureTime': departureTime,
            'PresentableDistance': arrivalString,
            'StopsAway': stopsAway,
            'TerminalDepartureTime': originDepartureTime,
            'CurrentStatus': currentStatus,
            'CurrentStop': currentStop,
            'CurrentStopName': currentStopName
        });
    }

    return [stopName, vehicles];
    
    //callback(stopName, vehicles);
}

function getBusStopStatus(stop_id, callback) {
    getMTABusStopData(stop_id, function(data) {
        let stopName = '';

        let stopMonitor = data.Siri.ServiceDelivery.StopMonitoringDelivery[0];
        let dataUpdatedTime = Date.parse(stopMonitor.ResponseTimestamp);

        var r = processVehicleData(stopMonitor.MonitoredStopVisit);
        callback(r[0], r[1]);
    });
}

function getMTABusSeqStops(route, direction, callback) {
    $.getJSON('http://bustime.mta.info/api/stops-on-route-for-direction?callback=?&routeId=' + encodeURIComponent(route) + '&directionId=' + direction, function(data) {
        console.log('getMTABusSeqStops', data);
        callback(data);
    })
}

function getBusRouteStopsForDirection(route, direction_id, callback) {
    getMTABusSeqStops(route, direction_id, function(data) {
        callback(data.stops);
    });
}

function getMTARouteStatusData(route, callback) {
    $.getJSON('http://api.prod.obanyc.com/api/siri/vehicle-monitoring.json?callback=?&key=TEST&LineRef=' + encodeURIComponent(route), function(data) {
        console.log('getMTARouteStatusData', data);
        callback(data);
    });
}

function getBusRouteStatus(route, callback) {
    getMTARouteStatusData(route, function(data) {
        let stopMonitor = data.Siri.ServiceDelivery.VehicleMonitoringDelivery[0].VehicleActivity;
        let dataUpdatedTime = Date.parse(stopMonitor.ResponseTimestamp);

        var r = processVehicleData(stopMonitor);
        console.log(r);
        callback(r[1]);
    })
}