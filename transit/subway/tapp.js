
var stationlines = subwaystationscsv.split("\n");

var busroutes = {};
var busroutecolors = {};

RTNYC.Bus = {
    stopSearch: function(search_query, callback) {
        var stop_count = 0;
        for (var key in busroutes) {
            let stop = busroutes[key];
            var s = key.split('$');
            var name = s[0];
            var services = s[1].split(",");
            if (!name.toUpperCase().includes(search_query.toUpperCase())) {
                continue;
            }

            callback(stop_count, stop, key, name, services);
            stop_count++;
        }

        return stop_count;
    },

    // parseMtaGeocodeResult: function(data) {
    //     var results_out = [];
    //     var results = data.searchResults;

    //     if (results.matches.length == 0) {
    //         return results_out;
    //     }

    //     var matches = results.matches[0];


    // }
}


var State = {
    MonitoringSubwayStation: 'MonitoringSubwayStation',
    MonitoringTrainTrip: 'MonitoringTrainTrip',
    MonitoringBusStop: 'MonitoringBusStop',
    ViewingSubwayLine: 'ViewingSubwayLine',
    ViewingBusRoute: 'ViewingBusRoute',
    ViewingBusRouteDirection: 'ViewingBusRouteDirection'
}

RTNYC.Tapp = {
    hasLoadedStations: false,
    subwayStations: {},
    subwayComplexes: {},

    busStopCount: 0,

    state: ['', window.location.hash],
    lastState: ['', '#'],
    lastStateStack: [['', '#0']],

    timerBusStopDataUpdator: null,
    timerStopDataUpdator: null, // TODO: rename to subway
    searchUpdateTimer: null,

    init: function() {
        this.loadStations();
    },

    loadStations: function() {
        for (var i in stationlines) {
            let s = stationlines[i].split(",");
    
            let sstopid = s[2];
            var services = [];
            let sname = s[5];
            let scomplex = s[1];
    
            Subway.subwayStations[sstopid] = s;
            //window.subwayStations = this.subwayStations;
            if (Subway.subwayComplexes[scomplex] == undefined) {
                Subway.subwayComplexes[scomplex] = [sstopid];
            } else {
                Subway.subwayComplexes[scomplex].push(sstopid);
            }
        }
    },

    getPageParamaters: function() {
        let params = {
            positionString: QueryString.getValue('mappos')
        }
        if (params.positionString) {
            params.mapLocation = params.positionString.split(',');
        }
        
        if (QueryString.getValue('station')) {
            params.action = State.MonitoringSubwayStation;
            params.argument = QueryString.getValue('station');
        }
        else if (QueryString.getValue('line')) {
            params.action = State.ViewingSubwayLine;
            params.argument = QueryString.getValue('line');
        }
        else if (QueryString.getValue('busstop')) {
            params.action = State.MonitoringBusStop;
            params.argument = QueryString.getValue('busstop');
        }
        else if (QueryString.getValue('route') && QueryString.getValue('direction')) {
            params.action = State.ViewingBusRouteDirection;
            params.argument = [QueryString.getValue('route'), QueryString.getValue('direction')];
        } 
        else if (QueryString.getValue('route')) {
            params.action = State.ViewingBusRoute;
            params.argument = QueryString.getValue('route');
        }
        return params;
    },

    getPageReferrers: function() {
        return {
            'rstop': QueryString.getValue('rstop'),
            'rvehicleid': QueryString.getValue('rvehicleid')
        };
    },

    loadNearbyScreen: function() {
        console.log('Loading nearby screen.');

        let v = $('#x-stop-search').val();
        if (v)
            $('#x-stop-search').trigger('input');

        $(".x-mainmenu").show();
        $("#x-station-picker").html(`
        <tr>
            <td colspan="2" style='white-space:normal;'>Enable location permissions to view nearby stations. You can also "Save" stations that will be pinned here.</td>
        </tr>`);
    },

    loadSubwayStation: function(station_id) {
        $(".x-clearable").text("Loading...")
        $('.x-direction1-table tbody').empty();
        $('.x-direction2-table tbody').empty();

        $(".x-stationdata").show();

        this.startTrackingSubwayStation(station_id);
    },

    loadBusStop: function(stop_id) {
        $(".x-clearable").text("Loading...")
        $("#x-bus-direction1-table tbody").empty();
        $("#x-bus-direction2-table tbody").empty();

        $(".x-busdata").show();

        this.startTrackingBusStop(stop_id);
    },

    showLoadingBar: function() {
        $('#loaderbar').show();
    },

    load: function() {
        this.hidePages();
        this.loadState();


        var params = this.getPageParamaters();
        console.log('load()', params);

        try {
            if (params.mapLocation) {
                var s = params.mapLocation;
                let $map = $('#rtnyc-map')[0];
                executeOnReadyFrame($map, function() {
                    $map.contentWindow.goTo([parseFloat(s[0]), parseFloat(s[1])], 16);
                    $map.contentWindow.location.hash = '#embed=1&location={0},{1}&zoom={2}'.format(s[0], s[1], 16);
                });
            }
        } catch (e) {
            console.error(e);
        }

        this.clearTimers();

        switch (params.action) {
            case State.MonitoringSubwayStation:
                this.loadSubwayStation(params.argument);
                break;
            case State.MonitoringTrainTrip:
                console.warn('unhandled trip monitoring request');
                break;
            case State.MonitoringBusStop:
                this.loadBusStop(params.argument);
                break;
            case State.ViewingSubwayLine:
                this.loadTrainLine(params.argument);
                break;
            case State.ViewingBusRoute:
                this.showLoadingBar();

                if (!window.BusRouteTerminalMap)
                    loadRouteTerminals(function() {
                        tapp.loadBusRoute(params.argument);
                    });
                else
                    tapp.loadBusRoute(params.argument);                
                break;
            case State.ViewingBusRouteDirection:
                this.showLoadingBar();

                $('#x-busroute-stops').before('');
                
                console.log('triggered ViewingBusRouteDirection')
                if (!window.BusRouteTerminalMap || !window.BusStopDirectionMap)
                    loadRouteTerminals(function() {
                        tapp.loadBusRouteDirection(params.argument[0], params.argument[1]);
                    });
                else
                    tapp.loadBusRouteDirection(params.argument[0], params.argument[1]);
                break;
            default:
                console.log('defaulting for action: ' + params.action);
                this.loadNearbyScreen();
                break;
        }
    },

    /* Form Actors */

    hidePages: function() {
        $('.bodycontainer > div').css('display','none');
    },

    startTrackingBusStop: function(stop_id) {
        clearInterval(this.timerBusStopDataUpdator);

        this.displayBusStationData(stop_id);

        this.timerBusStopDataUpdator = setInterval(function() {
            tapp.displayBusStationData(stop_id);
        }, 15 * 1000);
    },

    startTrackingSubwayStation: function(station_id) {
        clearInterval(this.timerStopDataUpdator);
        
        this.displaySubwayStationData(station_id);

        this.timerStopDataUpdator = setInterval(function() {
            tapp.displaySubwayStationData(station_id);
        }, 15 * 1000);
    },

    displayBusStationData: function(stop_id) {
        var stop_ids = stop_id.split('.');

        if (stop_ids.length > 1) {
            $('#x-bus-direction2').show();
        } else {
            $('#x-bus-direction2').hide();
        }

        var first = true;
    for (var stopid of stop_ids)
        getBusStopStatus(stopid, function(name, vehicles) {
            if (first) {
                $("#x-bus-direction1-table tbody").empty();
                $("#x-bus-direction2-table tbody").empty();
    
                $("#x-busstop-name").text(name.toLowerCase().replace('/', ' & '));    
            }


            if (!vehicles) {
                console.warn('could not fetch bus data');
                return;
            }

            // $('#x-lastupdate').text(new Date(station_status.Timestamp * 1000).toLocaleString());
            
            for (var vehicle of vehicles) {
                console.log(vehicle);

                $classes = 'busitem ';
                let $svcBox = tapp.busBoxFactory(vehicle.Route);

                $destination = vehicle.DestinationName.toLowerCase();
                var ts_now = Date.now();
                var tdelta = vehicle.ArrivalTime - ts_now;
                var trem = Math.floor(Math.floor(tdelta / 1000) / 60);
                $eta = `${trem} min`;

                if (trem > 15 && vehicles.length > 4) {
                    continue;
                }

                if (vehicle.CurrentStatus == 'ENROUTE') {
                    
                } else if (vehicle.CurrentStatus == 'AT_TERMINAL') {
                    $classes += 'busitem-atTerminal';
                    let termDepartureTime = new Date(vehicle.TerminalDepartureTime);
                    var tdDelta = termDepartureTime - ts_now;
                    $destination += `<br/><em style='text-transform:none;'>(at terminal, will depart at ${termDepartureTime.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'})})</em>`;
                    var tdRem = Math.floor(Math.floor(tdDelta / 1000) / 60);
                    if (tdRem > 10 || vehicle.StopsAway > 3) { //TODO: remove
                        continue;
                    }
                    $eta = `Departing in ${tdRem} min`;
                } else if (vehicle.CurrentStatus == 'ENROUTE_TO_TERMINAL') {
                    $classes += 'busitem-enrouteToTerminal';
                    let termDepartureTime = new Date(vehicle.TerminalDepartureTime);
                    var tdDelta = termDepartureTime - ts_now;
                    $destination += `<br/><em style='text-transform:none;'>(will depart terminal at ${termDepartureTime.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'})})</em>`;
                    $eta = `Enroute to terminal`;
                    if (tdRem > 10 || vehicle.StopsAway > 3) {
                        continue;
                    }
                }

                let html = `
                <tr class="${$classes}">
                    <td class='bus-l-td'>${$svcBox}</td>
                    <td class='bushead-td'>${$destination}</td>
                    <td class='bus-eta-td'>${$eta}</span></td>
                </tr>`;

                $(first ? "#x-bus-direction1-table tbody" : "#x-bus-direction2-table tbody").append(html);
                $(first ? "#b-bus-direction1-name" : "#b-bus-direction2-name").text(first ? '#' + stop_ids[0] : '#' + stop_ids[1]);
                console.log(html);
            }

            first = false;
        });
    },

    displaySubwayStationData: function(station_id) {
        getStationStatus(station_id, function(station_status) {
            $('.x-direction1-table tbody').empty();
            $('.x-direction2-table tbody').empty();
            if (!station_status) {
                console.warn('could not fetch station data!')
                return;
            }
            $('#x-lastupdate').text(new Date(station_status.Timestamp * 1000).toLocaleString());
            for (var i in station_status.Northbound) {
                let train = station_status.Northbound[i];
                let icon_type = train.ServiceId.includes("X") ? "exp" : "lcl";
                let icon = Subway.getSubwayIconURL(train.ServiceId);
                let html = 
                `<tr>
                    <td class="fcenter" style="width:20%;"><img class="bullet-{1}" src="{0}"/></th>
                    <td>{2}</th>
                    <td>{3} min</th>
                </tr>`.format(icon, icon_type, train.DestinationName, train.ArrivingOn);
                $(".x-direction1-table tbody").append(html);
            }
            var direction_str = "Uptown";
            if (station_status.NorthboundDirection == "Bronx") {
                var direction_str = "Uptown & The Bronx";
            } else if (station_status.NorthboundDirection == "Brooklyn") {
                var direction_str = "Downtown & Brooklyn";
            } else if (station_status.NorthboundDirection) {
                direction_str = station_status.NorthboundDirection.replace("/", " & ");
            }
            $("#x-direction1").text(direction_str);
            for (var i in station_status.Southbound) {
                let train = station_status.Southbound[i];
                let icon_type = train.ServiceId.includes("X") ? "exp" : "lcl";
                let icon = Subway.getSubwayIconURL(train.ServiceId);
                let html = 
                `<tr>
                    <td class="fcenter" style="width:20%;"><img class="bullet-{1}" src="{0}"/></th>
                    <td>{2}</th>
                    <td>{3} min</th>
                </tr>`.format(icon, icon_type, train.DestinationName, train.ArrivingOn);
                $(".x-direction2-table tbody").append(html);
            }
            var direction_str = "Downtown";
            if (station_status.SouthboundDirection == "Bronx") {
                var direction_str = "Uptown & The Bronx";
            } else if (station_status.SouthboundDirection == "Brooklyn") {
                var direction_str = "Downtown & Brooklyn";
            } else if (station_status.SouthboundDirection) {
                direction_str = station_status.SouthboundDirection.replace("/", " & ");
            }
            $("#x-direction2").text(direction_str);
            $("#x-station-name").text(station_status.StationName)
        })    
    },

    /* States */
    clearTimers: function() {
        clearInterval(this.timerStopDataUpdator);
        clearInterval(this.timerBusStopDataUpdator);
    },

    loadState: function() {
        this.state = ['', window.location.hash];
    },

    resetState: function() {
        window.location.hash = '#0';
        this.lastState = this.state;
        this.lastStateStack.push(this.state);
        this.state = ['', window.location.hash];
    },

    backState: function() {
        let current = this.state;
        //this.state = this.lastState;
        this.state = this.lastStateStack.pop() || ['', '#0'];
        this.lastState = current;
        window.location.hash = this.state[1];
    },

    setStateAsMonitoringSubwayStation: function(station_id, location) {
        window.location.hash = "#station=" + station_id + '&mappos=' + location;
        this.lastState = this.state;
        this.lastStateStack.push(this.state);
        this.state = [State.MonitoringSubwayStation, window.location.hash];
    },

    setStateAsViewingSubwayLine: function(line) {
        window.location.hash = "#line=" + line;
        this.lastState = this.state;
        this.lastStateStack.push(this.state);
        this.state = [State.ViewingSubwayLine, window.location.hash];
    },

    setStateAsMonitoringBusStop: function(stop_id, location) {
        window.location.hash = "#busstop=" + stop_id + '&mappos=' + location;
        this.lastStateStack.push(this.state);
        this.state = [State.MonitoringBusStop, window.location.hash];
    },

    setStateAsViewingBusLine: function(line) {
        window.location.hash = '#route=' + encodeURIComponent(line);
        this.lastStateStack.push(this.state);
        this.state = [State.ViewingBusRoute, window.location.hash];
    },

    setStateAsViewingBusLineDirection: function(line, direction) {
        window.location.hash = '#route=' + encodeURIComponent(line) + '&direction=' + direction;
        this.lastStateStack.push(this.state);
        this.state = [State.ViewingBusRouteDirection, window.location.hash];
    },

    // navbar state
    toggleSubwayBar: function(close) {
        var nr = $('#x-nav-subway')
        var closing = nr.hasClass('x-nav-open') || !!closed;
        if (closing) {
            $('#x-nb-subway').css('color', 'black');
            nr.removeClass('x-nav-open')
        } else {
            $('.x-nav-open').removeClass('x-nav-open');
            $('#x-nb-subway').css('color', 'purple');
            nr.addClass('x-nav-open');
        }
    },

    toggleBusBar: function() {
        var nr = $('#x-nav-bus');
        if (nr.hasClass('x-nav-open')) {
            $('#x-nb-bus').css('color', 'black');
            nr.removeClass('x-nav-open');
        } else {
            $('.x-nav-open').removeClass('x-nav-open');
            $('#x-nb-bus').css('color', 'purple');
            nr.addClass('x-nav-open');
        }
    },
    
    /* Searching */

    clearSearch: function() {
        $('#x-station-picker').empty();
        this.busStopCount = 0;
    },

    showRegionalSearch: function(query) {
        if (window.BusComplexMap == undefined) {
            $.getScript('buscomplexmap.js');
        }

        var e = encodeURIComponent(query);
        $.getJSON("http://bustime.mta.info/api/search?callback=?&q=" + e, function (data) {
            var results = data.searchResults;
    
            if (results.matches.length == 0) {
                // we found no matches
                console.debug('showRegionalSearch(%s): No matches', query);
                $('#x-station-picker').empty();
            } else {
                let match = results.matches[0];
                let title = match.formattedAddress;
                try {
                    var s;
                    s = title.split(', Bronx, NY'); 
                    title = s.length > 1 ? s[0] + ', The Bronx' : title;
                    s = title.split(', Queens, NY'); 
                    title = s.length > 1 ? s[0] + ', Queens' : title;
                    s = title.split(', New York, NY'); 
                    title = s.length > 1 ? s[0] + ', Manhattan' : title;
                    s = title.split(', Brooklyn, NY'); 
                    title = s.length > 1 ? s[0] + ', Brooklyn' : title;
                    s = title.split(', Staten Island, NY'); 
                    title = s.length > 1 ? s[0] + ', Staten Island' : title;
                    
                    // title = title.split(', Bronx, NY')[0] 
                    // title = title.split(', Queens, NY')[0] + ', Queens'
                    // title = title.split(', New York, NY')[0] + ', Manhattan';
                    // title = title.split(', Brooklyn, NY')[0] + ', Brooklyn';
                    // title = title.split(', Staten Island, NY')[0] + ', Staten Island';
                } catch (e) {
                    title = match.formattedAddress;
                }
    
                $('#x-station-picker').empty();
                $('#x-station-picker').append(`
                <tr>
                    <td colspan=2 style='background-color:black;color:white;text-align:center!important;'>
                        Near ${ title }
                    </td>
                </tr>`);
    
                let lat = match.latitude;
                let lng = match.longitude;
    
                let deltaLat = 0.002;
                let deltaLng = 0.002;
    
                if (match.bounds) {
                    let maxLat = match.bounds.maxLat;
                    let minLat = match.bounds.minLat;
                    let maxLng = match.bounds.maxLon;
                    let minLng = match.bounds.minLon;
    
                    deltaLat = maxLat - minLat;
                    deltaLng = maxLng - minLng;
                }
    
    
                for (var nearbyRoute of match.nearbyRoutes) {
                    let id = nearbyRoute.id;
                    let routeId = nearbyRoute.shortName;
                    let routeName = nearbyRoute.longName;
    
                    let html = '<tr value="{0}" class="x-busroute-item">'.format('') // TODO: when we support route lines
                    html += '   <td>' + routeName + '</td>'
                    html += '   <td>' + routeId + '</td>'
                    html += '</tr>'
                    $('#x-station-picker').append(html);
                }
    
                let url = 'http://bustime.mta.info/api/where/stops-for-location.json?callback=?&lat={0}&lon={1}&latSpan={2}&lonSpan={3}&key=TEST'.format(
                    lat, lng, deltaLat, deltaLng
                );
    
                $.getJSON(url, function(data) {
                    if (data.code != 200) {
                        console.warn('showRegionalSearch(%s): Failed to get stops for location (url: %s) because server returned %d', 
                            query, url, data.code);
                        return;
                    }
                    
                    $('.x-busroute-item').remove();
    
                    let stops = data.data.stops;
                    if (stops.length == 0) {
                        console.debug('Stop search: found no stops');
                        
                        let html = '<tr value="{0}">'.format('')
                        html += "   <td colspan=2>No bus stops were found in the area. Try a different search query.</td>"
                        html += '</tr>'
                        $('#x-station-picker').append(html);
                    } else {
                        var found_stop_complexes = {};
                        for (var stop of stops) {
                            let code = stop.code;
                            let dir = stop.direction;
    
                            if (BusComplexMap[code] != undefined) {
                                let ccode = BusComplexMap[code];
                                if (found_stop_complexes[ccode] == undefined) found_stop_complexes[ccode] = [];
                                found_stop_complexes[ccode].push(stop);
                            } else {
                                console.warn('showRegionalSearch(%s): Unknown stop code: %s', query, code);
                            }
                        }
                        console.debug(found_stop_complexes);
                        for (var cc in found_stop_complexes) {
                            
                            let firstStop = found_stop_complexes[cc][0];
                            let stopName = firstStop.name;
                            let fStopName = stopName.replace('/', ' &amp; ').toLowerCase();
    
                            // console.log(found_stop_complexes);
                            let servedByRoutesHtml = '';
    
                            // console.log(cc);
    
                            for (var service of firstStop.routes) {
                                servedByRoutesHtml += `<span class='busborder' style='margin-right:2px;background-color:#${service.color};color:#${service.textColor}'>${service.shortName}</span>`;
                            }
    
                            let html = '<tr value="{0}" location="{1},{2}" class="x-busstop-item">'.format(cc, firstStop.lat, firstStop.lon);
                            html += '   <td class="busstop-td bushead-td">' + fStopName + '</td>'
                            html += '   <td class="busservices-td">' + servedByRoutesHtml + '</td>'
                            html += '</tr>';
                            $('#x-station-picker').append(html);
                        }
    
                        let html = '<tr value="{0}">'.format('')
                        html += "   <td colspan=2 style='text-align:center !important;font-weight:bold;'>More results &rarr;</td>"
                        html += '</tr>'
                        $('#x-station-picker').append(html);

                        $('.x-busstop-item').click(function() {
                            let val = $(this).attr('value');
                            let loc = $(this).attr('location');
                            tapp.onBusStopClick(val, loc);
                        });
                    }
                })
            }
        });
    },

    showSubwaySearch: function(query) {

        RTNYC.Subway.stationSearch(query, function(stop_id, stop_name, lat, lng, services) {
            var services_html = "";
            for (var i in services) {
                var service = services[i];
                //console.log(service);
                var h = '<img class="bullet-lcl" src="{0}"/>'.format(RTNYC.Subway.getSubwayIconURL(service));
                services_html += h;
            }
            
            let html = '<tr value="{0}" location="{1},{2}" class="x-station-item">'.format(stop_id, lat, lng)
                html += '   <td>' + stop_name + '</td>'
                html += '   <td>' + services_html + '</td>'
                html += '</tr>'
            $('#x-station-picker').append(html);
        });
        
        $('.x-station-item').on('click', function() {
            let station = $(this).attr("value");
            tapp.onStationClick(station, $(this).attr('location'));
        })
    },

    getBusRouteColor: function(routeId) {
        let serviceCol = busroutecolors[routeId.toUpperCase()];
        return serviceCol;
    },

    busBoxFactory: function(routeId, extraClasses) {
        if (routeId.includes('_'))
            routeId = routeId.split("_")[1];
        routeId = routeId.replace("BX", "Bx");
        let serviceCol = busroutecolors[routeId.toUpperCase()];

        let classes = 'busborder ';
        
        if (extraClasses) classes += extraClasses;

        return `
        <span class='${classes}' style='background-color:#${serviceCol ? serviceCol[0] : '000000'};color:white;'>
            ${routeId}
        </span>`
    },

    showBusSearch: function(search_query) {
        
        loadRouteTerminals();

        this.busStopCount = Bus.stopSearch(search_query, function(index, stop, stop_id, name, services) {
            var services_html = '';
        
            for (var service of services) {
    
                let serviceId = service.split("_")[1].replace("BX", "Bx");
                let serviceCol = busroutecolors[serviceId.toUpperCase()];
    
    
                services_html += `
                <span class='busborder' style='background-color:#${serviceCol ? serviceCol[0] : '000000'};color:white;'>
                    ${serviceId}
                </span>`
            }

            var latitude, longitude;
            var terminals = {};
            if (stop.length == 1 && window.BusStopDirectionMap) { // single direction
                let stop1 = stop[0]
                let servedByRoute = BusStopDirectionMap[stop1['c']]
                for (var routeAndDirection of servedByRoute) {
                    let S = routeAndDirection.split('.')
                    let rt = S[0];
                    let dr = S[1];
                    let term = BusRouteTerminalMap[rt][dr]
                    terminals[term] = null;
                    //console.log('to ' + term);
                    //name += '<br/><small>to <strong class="bushead-td">' + term.toLowerCase() + '</strong></small>';
                }
                //console.log(servedByRoute)
            }

            if (stop.length) {
                latitude = stop[0].t;
                longitude = stop[0].n;
            }

            for (var term in terminals) {
                name += '<br/><small>to <strong class="bushead-td">' + term.toLowerCase() + '</strong></small>';
            }

            let html = '';
            if (index == 0) { // first
                html = '<tr value="{0}" location="{1},{2}" class="x-busstop-item" id="x-bustop-item-first">'.format(stop_id, latitude, longitude)
            } else {
                html = '<tr value="{0}" location="{1},{2}" class="x-busstop-item">'.format(stop_id, latitude, longitude)
            }
            html += '   <td class="busstop-td">' + name + '</td>'
            html += '   <td class="busservices-td">' + services_html + '</td>'
            html += '</tr>'
            $('#x-station-picker').append(html);
        });

        $('.x-busstop-item').click(function() {
            // todo: handler
            let stopid = $(this).attr('value');
            console.debug(busroutes[stopid]);
            let r = busroutes[stopid];
            let ss = '';
            if (r.length > 1) {
                ss = r[0].c + '.' + r[1].c;
            } else if (r.length == 1) {
                ss = r[0].c;
            } else {
                console.error('Unknown stop ID: ', stopid);
            }
            let loc = $(this).attr('location');
            tapp.onBusStopClick(ss, loc);
        })
    },

    
    showServicesSearch: function(query) {
        var q = query.toUpperCase();
        q = q.replace('-SBS', '+');
        if (q.length > 5) {
            console.debug('too big')
            return;
        }

        if (!window.BusRouteTerminalMap) {
            console.log('!window.BusRouteTerminalMap')
            return;
        }

        var route = BusRouteTerminalMap[q];
        console.log(route);

        if (route) {
            for (var dir in route) {
                var name = route[dir];
                $('#x-station-picker').prepend(`<tr><td colspan="2" onclick="onBusRouteBtnClick('MTA NYCT_${q}', '${dir}')">${this.busBoxFactory(q)} to ${name}</td></tr>`);
            }
        }
    },

    showExtraSearchSuggestions: function(search_query) {
        var urlencoded_sq = encodeURIComponent(search_query);
    
        clearTimeout(this.searchUpdateTimer);
        this.searchUpdateTimer = setTimeout(function() {
            $.getJSON("http://bustime.mta.info/api/autocomplete?callback=?&_=?&term=" + urlencoded_sq, function(result){
                //response data are now in the result variable
                //alert(result);
                $('.suggestion-td').remove();
    
                for (var suggestion of result) {
                    let html = `<tr class='suggestion-td'><td colspan=2 onclick='onSuggestionClick("${suggestion}")'>Stops &amp; directions near <strong>${suggestion}</strong> &rarr;</td></tr>`;
                    if (this.busStopCount > 5) {
                        $('#x-bustop-item-first').before(html);
                    } else {
                        $('#x-station-picker').append(html);
                    }
                }
            });
        }, 300);
    },

    executeSearch: function(query) {
        this.clearSearch();

        if (query.includes(', NY')) {
            this.showRegionalSearch(query);
        } else {
            this.showServicesSearch(query);
            this.showSubwaySearch(query);
            this.showExtraSearchSuggestions(query);
            this.showBusSearch(query);
        }
    },

    showSubwayLineSequentialStops: function() {
        $('#x-seqstops').css('display','inherit');
        this.toggleSubwayBar(true);
    },

    /* Event handlers */

    onStopSearch: function($html) {
        clearTimeout(this.searchTimer);
        let val = $html.val();
        this.searchTimer = setTimeout(function() {
            if (val.length == 0) {
                tapp.resetState();
                tapp.load();
                return;
            }
    
            tapp.executeSearch(val); 
        }, 125);
    },

    onBusStopClick: function(stop_id, location) {
        console.log('Bus Stop Clicked', stop_id, location);
        this.setStateAsMonitoringBusStop(stop_id, location);
    },

    onStationClick: function(station_id, location) {
        if (!location) {
            let s = Subway.subwayStations[station_id];
            location = s[9] + ',' + s[10];
        }
        console.log('onStationClick(%s): called', station_id);
        this.setStateAsMonitoringSubwayStation(station_id, location);
    },

    loadBusRoute: function(busRoute) {
        this.hidePages();

        $('#x-busroute-stops').show();
        $('#x-busrt-directions').show();
        $('#x-busroute-stops .service-stop-table').hide();

        var busRouteId = busRoute.split('_')[1];

        let terminals = BusRouteTerminalMap[busRouteId];

        console.log(terminals, busRoute);

        $('#x-busrtstops-route').html(this.busBoxFactory(busRouteId));

        var dirs_html = '';

        for (var dir in terminals) {
            var dir_name = terminals[dir];
            
            dirs_html += `
            <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised" style='text-transform:none;' onclick="onBusRouteBtnClick('${busRoute}', '${dir}')">
                to ${dir_name}
            </button>&nbsp;`;
        }

        $('#x-busrt-directions').html(dirs_html);
    },

    loadBusRouteDirection: function(busRoute, direction) {
        this.hidePages();

        $('#x-busroute-stops').show();
        $('#x-busrt-directions').hide();
        $('#x-busroute-stops .service-stop-table').show();

        var busRouteId = busRoute.split('_')[1] || busRoute;

        let terminals = BusRouteTerminalMap[busRouteId];

        console.debug(BusRouteTerminalMap);

        let terminal = terminals[parseInt(direction)];

        $('#x-busrtstops-route').html(this.busBoxFactory(busRouteId));

        let c = this.getBusRouteColor(busRouteId);
        console.warn(c);
        $('#x-busroute-terminal-name').css('background-color', '#' + c[0]).css('color', c[1]);
        $('#x-busroute-terminal-name').text(`${busRouteId} to ${terminal}`);

        getBusRouteStopsForDirection(busRoute, direction, function(stops) {
            $('#x-busroute-stops .service-stop-table tbody').empty();
            for (var stop of stops) {
                //console.log(stop);
                let stopName = stop.name;
                stopName = stopName.toLowerCase().replace('/', ' &amp; ');
                let stopServicesHtml = '<br/>';

                var stopCode = stop.id.split('_')[1];
                var routesAtStop = BusStopDirectionMap[stopCode];
                console.assert(routesAtStop, `No stop data found for stop "${stopCode}"`);
            
                if (routesAtStop)
                for (var routeAtStop of routesAtStop) {
                    var s = routeAtStop.split('.');
                    var routeName = s[0];
                    var routeDirection = s[1];
                    let busIconHtml = tapp.busBoxFactory(routeName, 'busborder-mini');
                    stopServicesHtml += busIconHtml + '&nbsp;';
                }

                $('#x-busroute-stops .service-stop-table tbody').append(`
                <tr onclick="tapp.onBusStopClick(${stopCode}, '${stop.latitude},${stop.longitude}')">
                    <td class='busstop-td'>
                        ${stopName}
                        ${stopServicesHtml}
                    </td>
                    <td class='routestop-items' id='routestop-items-${stopCode}'>
                        <div style='border-left: 6px solid #${c[0]}'>
                        </div>
                    </td>
                </tr>
                `);
            }

            getBusRouteStatus(busRoute, function(data) {
                console.log(data);

                var topItems = {};
                var centerItems = {};

                for (trip of data) {

                    var destinationName = trip.DestinationName || '';

                    if (trip.Direction != direction) {
                        // console.log('trip.Direction(%s) != direction(%s)', trip.Direction, direction);
                        continue;
                    }
    
                    if (trip.CurrentStatus != 'ENROUTE') { // Trip has not departed & at/enroute to terminal
                        // skip for now
                        console.log('CS != Enroute: ', trip.CurrentStatus);
                    } else {
                        let $rs = $('#routestop-items-' + trip.CurrentStop.replace('MTA_',''));
                        
                        var color = 'black';
                        if (destinationName.includes('LTD')) {
                            color = 'purple';
                        } else if (destinationName.includes('SELECT')) {
                            color = 'navy';
                        }
                        
                        if (trip.PresentableDistance == 'approaching' || trip.PresentableDistance == 'at stop') {
                            if (!$rs.has('.routestop-item-center-container').length) {
                                $rs.append(`<span class='routestop-item-center-container'></span>`);
                            }
                            let $cc = $rs.children('.routestop-item-center-container')
                            $cc.append(`<span class='routestop-item' style='background-color:${color}'><i class="fas fa-bus"></i> ${trip.PresentableDistance}</span></span>`);
                            // put it in the center
                            //centerItems += `<span class='routestop-item'><i class="fas fa-bus"></i> ${data.PresentableDistance}</span><br/></span>`;
                        } else {
                            // put it at the top as generic enroute
                            if (!$rs.has('.routestop-item-top-container').length) {
                                $rs.append(`<span class='routestop-item-top-container'></span>`);
                            }
                            let $cc = $rs.children('.routestop-item-top-container')
                            $cc.append(`<span class='routestop-item' style='background-color:${color}'><i class="fas fa-bus"></i> ${trip.PresentableDistance}</span></span>`);
                        }
                    }
                }
            })
        });
    },

    loadTrainLine: function(lineName) {
        // this.setStateAsViewingSubwayLine(lineName);
        this.hidePages();
        $('#x-seqstops').show();
        
        $('#x-seqstops-line').html(`LOADING... <div class="mdl-spinner mdl-js-spinner is-active"></div>`);

        fetchSubwayStops(lineName, function(data) {
            $('#x-seqstops-line').html('<img class="bullet-lcl" src="{0}"/>'.format(Subway.getSubwayIconURL(lineName)));
            var html = `<tbody>`;

            console.log(data)
            for (var b in data) {
                let borodata = data[b];
                let borough = borodata.borough.toLowerCase();
                let color = borodata.color;
                html += `
                <tr style='height:20px;'>
                    <th colspan=2 class='borough-header' style='background-color:${color}!important;'>
                        ${borough.replace('bronx', 'the bronx')}
                    </th>
                </tr>`;
                for (var station of borodata.stations) {
                    let stationName = station.name;
                    let stationType = station.type;
                    let stationGtfsId = station.id;
    
                    var stop_data = Subway.subwayStations[stationGtfsId];
                    //console.log(stop_data);
    
                    let services = Subway.getServicesAtStation(stationGtfsId);
    
                    var services_html = "";
                    for (var i in services) {
                        var service = services[i];
                        //console.log(service);
                        if ((i % 3) == 0 && i != 0)
                            services_html += '<br/>'
                        var h = '<img class="bullet-lcl" src="{0}"/>'.format(Subway.getSubwayIconURL(service));
                        services_html += h;
                    }
    
                    var default_string = `
                    <tr class='x-station-item' onclick='onStationClick("${stationGtfsId}")'>
                        <td class='stopname-td'>${stationName}</strong></td>
                        <td class='bullet-td'>${services_html}</td>
                    </tr>`;
    
                    if (stationType == "0") {
                        html += default_string;    
                    } else if (stationType == "2") {
                        html += `
                        <tr style='background-color:lightgray' class='x-station-item' onclick='onStationClick("${stationGtfsId}")'>
                            <td class='stopname-td' style='color:gray'><i class="fas fa-moon"></i> ${stationName}</strong></td>
                            <td class='bullet-td'>${services_html}</td>
                        </tr>`;
                    } else if (stationType == "1") {
                        if (lineName == '2' || lineName == '4') {
                            html += `
                            <tr  class='x-station-item' onclick='onStationClick("${stationGtfsId}")'>
                                <td class='stopname-td' style='color:gray'>${stationName}<br/><span style='font-size:11px;'>Weekdays: Skipped Downtown 7-9am, Uptown 4:45-6:20pm</span></strong></td>
                                <td class='bullet-td'>${services_html}</td>
                            </tr>`;
                        } else if (lineName == '3' || lineName == '5') {
                            html += `
                            <tr style='background-color:lightgray' class='x-station-item' onclick='onStationClick("${stationGtfsId}")'>
                                <td class='stopname-td' style='color:gray'>🌞 ${stationName}<br/>
                                <span style='font-size:11px;'>No 
                                <img class="bullet-lcl" style='width:18px;height:18px;' src="${Subway.getSubwayIconURL(lineName)}"/>
                                service at station 12-5am.</span></strong></td>
                                <td class='bullet-td'>${services_html}</td>
                            </tr>`;
                        } else {
                            // todo: others
                            html += default_string;
                        }
                    }
                }
            }
    
            html += '</tbody>'
            //console.log(html) 
            $('#x-seqstops .service-stop-table').html(html);
        });
    },

    onStationDataBackButton: function() {
        this.backState();
    },

    onSubwayLineBackButton: function() {
        this.backState();
    }
}

var Subway = RTNYC.Subway;
var Bus = RTNYC.Bus;

var hasLoadedBronxBusComplexes = false;
var hasLoadedBusRouteColors = false;

function loadBusRouteColors() {
    if (!hasLoadedBusRouteColors)
        $.getJSON('data/busroutecolors.json', function(data) {
            busroutecolors = data;
        });
}

loadBusRouteColors();

$('#x-stop-search').on('click', function() {
    if (!hasLoadedBronxBusComplexes)
        $.getJSON('data/buscomplexes-bronx.json', function(data) {
            Object.assign(busroutes, data);
        })

    
    let $val = $(this).val();
    if ($val.includes(', NY')) {
        this.setSelectionRange(0, $val.length);
    }
})

function loadRouteTerminals(callback) {

    var haveDirectionMap = false;
    var haveRouteTerminalMap = false;

    if (!window.BusStopDirectionMap) {
        $.getScript('busstopcodedirmap.js', function() {
            haveDirectionMap = true;
            if (haveRouteTerminalMap)
                callback();
        });
    }
    if (!window.BusRouteTerminalMap) {
        $.getScript('busstoptermsmap.js', function() {
            haveRouteTerminalMap = true;
            if (haveDirectionMap)
                callback();
        });
    }
}

var tapp = RTNYC.Tapp;

tapp.init();

$('#x-stop-search').on('input', function() {
    tapp.onStopSearch($(this));
})

$('#x-nb-subway').click(function() {
    tapp.toggleSubwayBar();
})

$('#x-nb-bus').click(function() {
    tapp.toggleBusBar();
});

// Back buttons
$("#x-stationdata-back-btn").on('click', function() {
    console.log('user clicked stationdata back button');
    tapp.onStationDataBackButton();
});

$('#x-seqstops-back-btn').on('click', function() {
    console.log('user clicked subway line back button');    
    tapp.onSubwayLineBackButton();
})

$('#x-busrtstops-back-btn').on('click', function() {
    tapp.onStationDataBackButton();
})

$('#x-busdata-back-btn').on('click', function() {
    tapp.onStationDataBackButton();
})

// Bus buttons
$('.busbtn').click(function() {
    tapp.toggleBusBar(true);

    let route = $(this).attr('route');
    if (!route) {
        route = $(this).text();
        route = 'MTA NYCT_' + route.toUpperCase().replace('-SBS', '+');
    }
    
    tapp.setStateAsViewingBusLine(route);
})

function onSuggestionClick(suggestion_location) {
    $('#x-stop-search').val(suggestion_location);
    $('#x-stop-search').trigger('input');
}

function bulletClick(lineName) {
    tapp.toggleSubwayBar(true);
    tapp.setStateAsViewingSubwayLine(lineName);
    //tapp.loadTrainLine(lineName);
}

function onStationClick(station_id, location) {
    tapp.onStationClick(station_id, location);
}

function onBusRouteBtnClick(route_id, direction) {
    //if (route_id.length)
    console.log('onBusRoutrBtnClick(routeId=%s, direction=%s)', route_id, direction);
    tapp.setStateAsViewingBusLineDirection(route_id, direction);
}

window.addEventListener('hashchange', function() {
    tapp.load();
})

// function updateLocation(coordinates) {
//     tapp.updateLocation(coordinates.latitude, coordinates.longitude);
// }

// function handleLocation(location) {
//     console.log('Location:');
//     console.log(location);
//     alert('Location: ' + location);
//     var coords = location.coords;
//     parent.updateLocation(coords);
//     parent.location.hash = `#mappos=${coords.latitude},${coords.longitude}`
//     //parent.location.load();
// }

// function locate() {
//     //let $frame = $('#location-services-frame')[0];
//     // $('#location-services-frame').load(function() {
//     //     alert('Frame loaded');
//     //     this.contentWindow.getCurrentLocation(function(location) {
//     //         parent.handleLocation(location);
//     //     }, true);
//     // });

//     document.getElementById('location-services-frame').onload = function() {
//         alert('Frame loaded!');
//         this.contentWindow.getCurrentLocation(function(location) {
//             parent.handleLocation(location);
//         });
//     }
// }

// locate();

tapp.load();