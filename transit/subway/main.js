if(!String.prototype.format){
    String.prototype.format=function(){var args=arguments;return this.replace(/{(\d+)}/g,function(match,number){return typeof args[number]!='undefined'?args[number]:match})}}

var stationlines = subwaystationscsv.split("\n");

var line_to_svgurl_map = {
    '1': 'https://upload.wikimedia.org/wikipedia/commons/3/3f/NYCS-bull-trans-1.svg',
    '2': 'https://upload.wikimedia.org/wikipedia/commons/6/61/NYCS-bull-trans-2.svg',
    '3': 'https://upload.wikimedia.org/wikipedia/commons/2/25/NYCS-bull-trans-3.svg',
    '4': 'https://upload.wikimedia.org/wikipedia/commons/e/ec/NYCS-bull-trans-4.svg',
    '5': 'https://upload.wikimedia.org/wikipedia/commons/7/78/NYCS-bull-trans-5.svg',
    '5X': 'https://upload.wikimedia.org/wikipedia/commons/f/ff/NYCS-bull-trans-5d.svg',
    '6': 'https://upload.wikimedia.org/wikipedia/commons/5/53/NYCS-bull-trans-6.svg',
    '6X': 'https://upload.wikimedia.org/wikipedia/commons/0/0c/NYCS-bull-trans-6d.svg',
    '7': 'https://upload.wikimedia.org/wikipedia/commons/a/a0/NYCS-bull-trans-7.svg',
    '7X': 'https://upload.wikimedia.org/wikipedia/commons/e/e6/NYCS-bull-trans-7d.svg',
    'A': 'https://upload.wikimedia.org/wikipedia/commons/0/0c/NYCS-bull-trans-A.svg',
    'C': 'https://upload.wikimedia.org/wikipedia/commons/0/03/NYCS-bull-trans-C.svg',
    'E': 'https://upload.wikimedia.org/wikipedia/commons/2/2f/NYCS-bull-trans-E.svg',
    'B': 'https://upload.wikimedia.org/wikipedia/commons/f/fb/NYCS-bull-trans-B.svg',
    'D': 'https://upload.wikimedia.org/wikipedia/commons/3/39/NYCS-bull-trans-D.svg',
    'F': 'https://upload.wikimedia.org/wikipedia/commons/c/cb/NYCS-bull-trans-F.svg',
    'M': 'https://upload.wikimedia.org/wikipedia/commons/1/1d/NYCS-bull-trans-M.svg',
    'N': 'https://upload.wikimedia.org/wikipedia/commons/3/37/NYCS-bull-trans-N.svg',
    'Q': 'https://upload.wikimedia.org/wikipedia/commons/8/84/NYCS-bull-trans-Q.svg',
    'R': 'https://upload.wikimedia.org/wikipedia/commons/c/ce/NYCS-bull-trans-R.svg',
    'W': 'https://upload.wikimedia.org/wikipedia/commons/c/c1/NYCS-bull-trans-W.svg',
    'G': 'https://upload.wikimedia.org/wikipedia/commons/e/ee/NYCS-bull-trans-G.svg',
    'J': 'https://upload.wikimedia.org/wikipedia/commons/f/f4/NYCS-bull-trans-J.svg',
    'Z': 'https://upload.wikimedia.org/wikipedia/commons/b/b7/NYCS-bull-trans-Z.svg',
    'L': 'https://upload.wikimedia.org/wikipedia/commons/b/bd/NYCS-bull-trans-L.svg',
    'S': 'https://upload.wikimedia.org/wikipedia/commons/8/8a/NYCS-bull-trans-S.svg',
    '': 'https://upload.wikimedia.org/wikipedia/commons/8/8a/NYCS-bull-trans-S.svg'
}

function getTrainBulletIcon(train_route) {
    //console.log('get svc ' + train_route)
    //console.log("http://subwaytime.mta.info/img/{0}.png".format(train_route))
    // return "http://subwaytime.mta.info/img/{0}.png".format(train_route);
    return line_to_svgurl_map[train_route] ||
    "http://subwaytime.mta.info/img/{0}.png".format(train_route);
}

var stations = {};
var complex_children = {};

function loadStations() {
    for (var i in stationlines) {
        let s = stationlines[i].split(",");

        let sstopid = s[2];
        var services = [];
        let sname = s[5];
        let scomplex = s[1];

        stations[sstopid] = s;
        if (complex_children[scomplex] == undefined) {
            complex_children[scomplex] = [sstopid];
        } else {
            complex_children[scomplex].push(sstopid);
        }
    }
}

loadStations();

function display(search_query) {
    for (var i in stationlines) {
        let s = stationlines[i].split(",");

        let sstopid = s[2];
        var services = [];
        let sname = s[5];
        if (search_query) {
            if (!sname.toUpperCase().includes(search_query.toUpperCase()))
                continue;
        }
        let sservices = s[7].split(' ');
        for (let i in sservices) {
            if (!services.includes(sservices[i]))
                services.push(sservices[i]);
        }
        services.sort();
    
        var services_html = "";
        for (var i in services) {
            var service = services[i];
            //console.log(service);
            var h = '<img class="bullet-lcl" src="{0}"/>'.format(getTrainBulletIcon(service));
            services_html += h;
        }
        
        let html = '<tr value="{0}" location="{1},{2}" class="x-station-item">'.format(sstopid,s[9], s[10])
            html += '   <td>' + sname + '</td>'
            html += '   <td>' + services_html + '</td>'
            html += '</tr>'
        $('#x-station-picker').append(html);
    }
}

function loadRouteTerminals() {
    if (!window.BusStopDirectionMap) {
        $.getScript('busstopcodedirmap.js');
    }
    if (!window.BusRouteTerminalMap) {
        $.getScript('busstoptermsmap.js');
    }
}

var busroutes = {};
var busroutecolors = {};

var busstoop_count = 0;
function displayBus(search_query) {
    loadRouteTerminals();

    busstoop_count = 0;
    for (var key in busroutes) {
        let stop = busroutes[key];
        var s = key.split('$');
        var name = s[0];
        var services = s[1].split(",");
        if (!name.toUpperCase().includes(search_query.toUpperCase())) {
            continue;
        }

        var services_html = '';
        
        for (var service of services) {

            let serviceId = service.split("_")[1].replace("BX", "Bx");
            let serviceCol = busroutecolors[serviceId.toUpperCase()];


            services_html += `
            <span class='busborder' style='background-color:#${serviceCol ? serviceCol[0] : '000000'};color:white;'>
                ${serviceId}
            </span>`
        }

        // console.log(stop);
        // let direction = stop[0].d;

        var terminals = {};
        if (stop.length == 1 && window.BusStopDirectionMap) { // single direction
            let stop1 = stop[0]
            let servedByRoute = BusStopDirectionMap[stop1['c']]
            for (var routeAndDirection of servedByRoute) {
                let S = routeAndDirection.split('.')
                let rt = S[0];
                let dr = S[1];
                let term = BusRouteTerminalMap[rt][dr]
                terminals[term] = null;
                //console.log('to ' + term);
                //name += '<br/><small>to <strong class="bushead-td">' + term.toLowerCase() + '</strong></small>';
            }
            //console.log(servedByRoute)
        }

        for (var term in terminals) {
            name += '<br/><small>to <strong class="bushead-td">' + term.toLowerCase() + '</strong></small>';
        }

        // if (busroutes[key].length == 1) { // single direction
        //     if (direction == 'W' || direction == 'NW' || direction == 'SW') {
        //         name += '<br/>SOUTHBOUND';
        //     } else if (direction == 'E' || direction == 'NE' || direction == 'SE') {
        //         name += '<br/>NORTHBOUND';
        //     } else if (direction == 'N') {
        //         name += "<br/>EASTBOUND";
        //     } else if (direction == 'S') {
        //         name += '<br/>WESTBOUND';
        //     } else {
        //         name += '<br/>? '// + direction
        //     };
        //     name += ' - ' + direction;
        // }
        

        let html = '';
        if (busstoop_count == 0) { // fist
            html = '<tr value="{0}" class="x-busstop-item" id="x-bustop-item-first">'.format(key)
        } else {
            html = '<tr value="{0}" class="x-busstop-item">'.format(key)
        }
        html += '   <td class="busstop-td">' + name + '</td>'
        html += '   <td class="busservices-td">' + services_html + '</td>'
        html += '</tr>'
        $('#x-station-picker').append(html);
        busstoop_count++;
    }
}

async function displayClear(search_query) {
    $('#x-station-picker').empty();

    if (search_query.includes(', NY')) {
        displayRegionalSearch(search_query);
    } else {
        display(search_query);
        displayExtraSuggestions(search_query);
        displayBus(search_query);
    }
}

function displayRegionalSearch(search_query) {
    if (window.BusComplexMap == undefined) {
        $.getScript('buscomplexmap.js');
    }

    var e = encodeURIComponent(search_query);
    $.getJSON("http://bustime.mta.info/api/search?callback=?&q=" + e, function (data) {
        var results = data.searchResults;

        if (results.matches.length == 0) {
            // we found no matches
            console.debug('No matches');
            $('#x-station-picker').empty();
        } else {
            let match = results.matches[0];

            $('#x-station-picker').empty();
            $('#x-station-picker').append(`<tr>
                <td colspan=2 style='background-color:black;color:white;text-align:center!important;'>
                    Near ${ match.formattedAddress }
                </td>
            </tr>`);

            let lat = match.latitude;
            let lng = match.longitude;

            let deltaLat = 0.002;
            let deltaLng = 0.002;

            if (match.bounds) {
                let maxLat = match.bounds.maxLat;
                let minLat = match.bounds.minLat;
                let maxLng = match.bounds.maxLon;
                let minLng = match.bounds.minLon;

                deltaLat = maxLat - minLat;
                deltaLng = maxLng - minLng;
            }


            for (var nearbyRoute of match.nearbyRoutes) {
                let id = nearbyRoute.id;
                let routeId = nearbyRoute.shortName;
                let routeName = nearbyRoute.longName;

                let html = '<tr value="{0}" class="x-bus-item">'.format('')
                html += '   <td>' + routeName + '</td>'
                html += '   <td>' + routeId + '</td>'
                html += '</tr>'
                $('#x-station-picker').append(html);
            }

            let url = 'http://bustime.mta.info/api/where/stops-for-location.json?callback=?&lat={0}&lon={1}&latSpan={2}&lonSpan={3}&key=TEST'.format(
                lat, lng, deltaLat, deltaLng
            );

            $.getJSON(url, function(data) {
                if (data.code != 200) {
                    console.warn('Failed to get stops for location (url: %s) because server returned %d', url, data.code);
                    return;
                }
                $('.x-bus-item').remove();

                let stops = data.data.stops;
                if (stops.length == 0) {
                    console.debug('Stop search: found no stops');
                    
                    let html = '<tr value="{0}">'.format('')
                    html += "   <td colspan=2>No bus stops were found in the area. Try a different search query.</td>"
                    html += '</tr>'
                    $('#x-station-picker').append(html);
                } else {
                    var found_stop_complexes = {};
                    for (var stop of stops) {
                        let code = stop.code;
                        let dir = stop.direction;

                        if (BusComplexMap[code] != undefined) {
                            let ccode = BusComplexMap[code];
                            if (found_stop_complexes[ccode] == undefined) found_stop_complexes[ccode] = [];
                            found_stop_complexes[ccode].push(stop);
                        } else {
                            console.warn('Unknown stop code: %s', code);
                        }
                    }
                    console.debug(found_stop_complexes);
                    for (var cc in found_stop_complexes) {
                        
                        let firstStop = found_stop_complexes[cc][0];
                        let stopName = firstStop.name;
                        let fStopName = stopName.replace('/', ' &amp; ').toLowerCase();

                        console.log(found_stop_complexes);

                        var terminals = {};
                        let stop = found_stop_complexes[cc];
                        if (stop.length == 1 && window.BusStopDirectionMap) { // single direction
                            let stop1 = stop[0]
                            let servedByRoute = BusStopDirectionMap[stop1['code']]
                            for (var routeAndDirection of servedByRoute) {
                                let S = routeAndDirection.split('.')
                                let rt = S[0];
                                let dr = S[1];
                                let term = BusRouteTerminalMap[rt][dr]
                                terminals[term] = null;
                                //console.log('to ' + term);
                                //name += '<br/><small>to <strong class="bushead-td">' + term.toLowerCase() + '</strong></small>';
                            }
                            //console.log(servedByRoute)
                        }
                
                        for (var term in terminals) {
                            fStopName += '<br/><small>to <strong class="bushead-td">' + term.toLowerCase() + '</strong></small>';
                        }
                        

                        let servedByRoutesHtml = '';

                        console.log(cc);

                        for (var service of firstStop.routes) {
                            servedByRoutesHtml += `<span class='busborder' style='margin-right:2px;background-color:#${service.color};color:#${service.textColor}'>${service.shortName}</span>`;
                        }

                        let html = '<tr value="{0}" class="x-bus-item">'.format(cc)
                        html += '   <td class="busstop-td bushead-td">' + fStopName + '</td>'
                        html += '   <td class="busservices-td">' + servedByRoutesHtml + '</td>'
                        html += '</tr>';
                        $('#x-station-picker').append(html);
                    }

                    let html = '<tr value="{0}">'.format('')
                    html += "   <td colspan=2 style='text-align:center !important;font-weight:bold;'>More results &rarr;</td>"
                    html += '</tr>'
                    $('#x-station-picker').append(html);
                }
            })
        }
    });
}

function onSuggestionClick(suggestion_location) {
    $('#x-stop-search').val(suggestion_location);
    $('#x-stop-search').trigger('input');
}

var sqDelayTimer;

async function displayExtraSuggestions(search_query) {
    var urlencoded_sq = encodeURIComponent(search_query);
    
    clearTimeout(sqDelayTimer);
    sqDelayTimer = setTimeout(function() {
        $.getJSON("http://bustime.mta.info/api/autocomplete?callback=?&_=?&term=" + urlencoded_sq, function(result){
            //response data are now in the result variable
            //alert(result);
            $('.suggestion-td').remove();

            for (var suggestion of result) {
                let html = `<tr class='suggestion-td'><td colspan=2 onclick='onSuggestionClick("${suggestion}")'>Stops &amp; directions near <strong>${suggestion}</strong> &rarr;</td></tr>`;
                if (busstoop_count > 5) {
                    $('#x-bustop-item-first').before(html);
                } else {
                    $('#x-station-picker').append(html);
                }
            }
        });
    }, 300)
}

function onStationClick(station, location) {
    if (!location) {
        console.log()
        let s = stations[station];
        location = s[9] + ',' + s[10];
    }
    console.log(station);
    window.location.hash = "#station=" + station + '&mappos=' + location;
    //load();
}

var loadedBusComplexes = false;
var loadedBusColors = false;

// Only load on click to speed up page load
$('#x-stop-search').click(function() {
    if (!loadedBusComplexes)
        $.getJSON('data/buscomplexes-bronx.json', function(data) {
            Object.assign(busroutes, data);
            loadedBusComplexes = true;
        })
    if (!loadedBusColors)
        $.getJSON('data/busroutecolors.json', function(data) {
            busroutecolors = data;
            loadedBusColors = true;
        });
    let $val = $(this).val();
    if ($val.includes(', NY')) {
        this.setSelectionRange(0, $val.length);
    }
})

var searchDelayTimer;


$('#x-stop-search').on('input', function() {
    clearTimeout(searchDelayTimer);
    let val = $(this).val();
    setTimeout(function() {
        //console.debug('change ' + val)
        if (val.length == 0) {
            window.location.hash = "#0"
            load(true);
            return;
        }
    
        displayClear(val)
    
        $('.x-station-item').on('click', function() {
            let station = $(this).attr("value");
            onStationClick(station, $(this).attr('location'));
        })
    }, 125);
})

$("#x-stationdata-back-btn").on('click', function() {
    console.log('got back btn')
    // hideAll();
    window.location.hash = "#0"
    //load(false);
    //history.back();
});

$('#x-seqstops-back-btn').on('click', function() {
    console.log('got back btn')
    hideAll();
    window.location.hash = "#0"
    //history.back();
    //load();
})

//display();

/* === SUBWAY DATA LOADING  === */

function getMtaStationStatus(l, stopid, callback) {
    var request_url = 'http://traintimelb-367443097.us-east-1.elb.amazonaws.com/getTime/' + l + '/' + stopid;
    var request = $.get(request_url+'', function(data) {
        console.log(data);
        callback(data);
    });
}

function getStationStatus(gtfs_station_id, callback) {
    getMtaStationStatus(gtfs_station_id[0], gtfs_station_id, function(data) {
        let j = {'Northbound':[], 'Southbound': []}
        let station_name = data['stationName']
        j['NorthboundDirection'] = data.direction1.name;
        for (var i in data.direction1.times) {
            var time = data.direction1.times[i];
            j['Northbound'].push({ServiceId: time.route, DestinationName: time.lastStation, ArrivingOn: time.minutes, TripId: ''})
        }
        j['SouthboundDirection'] = data.direction2.name;
        for (var i in data.direction2.times) {
            var time = data.direction2.times[i];
            j['Southbound'].push({ServiceId: time.route, DestinationName: time.lastStation, ArrivingOn: time.minutes, TripId: ''})
        }
        j['StationName'] = station_name;
        // console.log('TS: %s', data.lastUpdatedOn)
        j['Timestamp'] = parseInt(data.lastUpdatedOn);
        callback(j);
        return;
        // Sample Reply:
        // callback({
        //     "StationName": "125 St",
        //     "Northbound": [
        //         {
        //             "ServiceId": "6",
        //             "DestinationName": "Parkchester",
        //             "ArrivingOn": 000000,
        //             "TripID": "06 0000+ BBR/177"
        //         },
        //         {
        //             "ServiceId": "6X",
        //             "DestinationName": "Pelham Bay Park",
        //             "ArrivingOn": 000000,
        //             "TripID": "06 0000+ BBR/PEL"
        //         },
        //         {
        //             "ServiceId": "2",
        //             "DestinationName": "E 180 St",
        //             "ArrivingOn": 000000,
        //             "TripID": "02 0000+ FLA/180"
        //         }
        //     ],
        //     "Southbound": [
        //         {
        //             "ServiceId": "4",
        //             "DestinationName": "Crown Hts - Utica Av",
        //             "ArrivingOn": 000000,
        //             "TripID": "04 0000+ WDL/UTI"
        //         },
        //         {
        //             "ServiceId": "5",
        //             "DestinationName": "Bowling Green",
        //             "ArrivingOn": 000000,
        //             "TripID": "05 0000+ DYR/BWG"
        //         }
        //     ]
        // })
    })
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function getParameterByName(name) {
    var hashOrSearch = document.location.hash || document.location.search;
    var searchParams = new URLSearchParams(hashOrSearch.substring(1).toLowerCase());
    if (searchParams.has(name)) {
        return searchParams.get(name);
    }
}

function displayBusStopData() {
    let stop_str = getParameterByName('stop');

    let stops = stop_str.split('.');

    if (stops.length == 2) {
        let stop1 = stops[0];
        let stop2 = stops[1];
        $('#x-bus-direction1-table').show();
        $('#x-bus-direction2-table').show();
        
    } else {
        let stop1 = stop[0];
        $('#x-bus-direction2-table').hide();
    }
}

var stop_data_updator;

function displayStationData() {
    let station_id = getParameterByName("station");
    getStationStatus(station_id, function(station_status) {
        $('.x-direction1-table tbody').empty();
        $('.x-direction2-table tbody').empty();
        if (!station_status) {
            console.warn('could not fetch station data!')
            return;
        }
        $('#x-lastupdate').text(new Date(station_status.Timestamp * 1000).toLocaleString());
        for (var i in station_status.Northbound) {
            let train = station_status.Northbound[i];
            let icon_type = train.ServiceId.includes("X") ? "exp" : "lcl";
            let icon = getTrainBulletIcon(train.ServiceId);
            let html = 
            `<tr>
                <td class="fcenter" style="width:20%;"><img class="bullet-{1}" src="{0}"/></th>
                <td>{2}</th>
                <td>{3} min</th>
            </tr>`.format(icon, icon_type, train.DestinationName, train.ArrivingOn);
            $(".x-direction1-table tbody").append(html);
        }       
        var direction_str = "Uptown";
        if (station_status.NorthboundDirection == "Bronx") {
            var direction_str = "Uptown & The Bronx";
        } else if (station_status.NorthboundDirection == "Brooklyn") {
            var direction_str = "Downtown & Brooklyn";
        } else if (station_status.NorthboundDirection) {
            direction_str = station_status.NorthboundDirection.replace("/", " & ");
        }
        $("#x-direction1").text(direction_str);
        for (var i in station_status.Southbound) {
            let train = station_status.Southbound[i];
            let icon_type = train.ServiceId.includes("X") ? "exp" : "lcl";
            let icon = getTrainBulletIcon(train.ServiceId);
            let html = 
            `<tr>
                <td class="fcenter" style="width:20%;"><img class="bullet-{1}" src="{0}"/></th>
                <td>{2}</th>
                <td>{3} min</th>
            </tr>`.format(icon, icon_type, train.DestinationName, train.ArrivingOn);
            $(".x-direction2-table tbody").append(html);
        }
        var direction_str = "Downtown";
        if (station_status.SouthboundDirection == "Bronx") {
            var direction_str = "Uptown & The Bronx";
        } else if (station_status.SouthboundDirection == "Brooklyn") {
            var direction_str = "Downtown & Brooklyn";
        } else if (station_status.SouthboundDirection) {
            direction_str = station_status.SouthboundDirection.replace("/", " & ");
        }
        $("#x-direction2").text(direction_str);
        $("#x-station-name").text(station_status.StationName)
    })
}

function load(clear) {

    hideAll();

    var mappos = getParameterByName('mappos');
    
    try {
        if (mappos) {
            var s = mappos.split(',')
            let $map = $('#rtnyc-map')[0];
            executeOnReadyFrame($map, function() {
                $map.contentWindow.goTo([parseFloat(s[0]), parseFloat(s[1])], 16);
                $map.contentWindow.location.hash = '#embed=1&location={0},{1}&zoom={2}'.format(s[0], s[1], 16);
            });
        }    
    } catch (e) {
        console.error(e);
    }

    clearInterval(stop_data_updator);
    
    if (getParameterByName("station")) {
        console.log("Got a station.");

        $(".x-clearable").text("Loading...")
        $('.x-direction1-table tbody').empty();
        $('.x-direction2-table tbody').empty();

        $(".x-stationdata").show();

        // $(".x-mainmenu").hide();
        displayStationData();
        stop_data_updator = setInterval(function() {
            displayStationData();
        }, 15 * 1000)
    } else if (getParameterByName("train")) {
        console.log("Got a train.");
        // $(".x-mainmenu").hide();
        // $(".x-stationdata").hide();
        $(".x-traindata").show();
    } else if (getParameterByName("stop")) {
        console.log("Got a bus stop.");

        $(".x-clearable").text("Loading...")

        $('#x-bus-direction1-table tbody').empty();
        $('#x-bus-direction2-table tbody').empty();

        $('.x-busdata').show();

        displayBusStopData();
        // busstop_data_updator = setInterval(function() {
        //     displayBusStopData();
        // }, 15 * 1000);
    } else {
        $(".x-mainmenu").show();
        if (clear)
            $("#x-station-picker").html(`
            <tr>
                <td colspan="2" style='white-space:normal;'>Enable location permissions to view nearby stations. You can also "Save" stations that will be pinned here.</td>
            </tr>`);
        // $(".x-stationdata").hide();
    }

}
// displayClear()
load()

function toggleSubwayBar(closed) {
    var nr = $('#x-nav-subway')
    var closing = nr.hasClass('x-nav-open') || !!closed;
    if (closing) {
        $('#x-nb-subway').css('color', 'black');
        nr.removeClass('x-nav-open')
    } else {
        $('#x-nb-subway').css('color', 'purple');
        nr.addClass('x-nav-open');
    }
}

$('#x-nb-subway').click(function() {
    toggleSubwayBar();
})

$('#x-nb-bus').click(function() {
    //alert('click')
    var nr = $('#x-nav-bus');
    if (nr.hasClass('x-nav-open')) {
        $(this).css('color', 'black');
        nr.removeClass('x-nav-open');
    } else {
        $(this).css('color', 'purple');
        nr.addClass('x-nav-open');
    }
})

function getServicesAtStation(gtfs_stop_id) {
    let complexid = stations[gtfs_stop_id][1];
    let services = []
    for (var stopid of complex_children[complexid]) {
        let stop = stations[stopid]
        let sservices = stop[7].split(' ');
        for (let i in sservices) {
            if (!services.includes(sservices[i]))
                services.push(sservices[i]);
        }
        services.sort();
    }

    return services;
}

function hideAll() {
    $('.bodycontainer > div').css('display','none');    
}

function bulletClick(lineName) {
    console.log('showing stops for line ' + lineName);
    //$('.bodycontainer > div').css('display','none');
    hideAll();
    $('#x-seqstops').css('display','inherit');
    toggleSubwayBar(true);
    fetchSubwayStops(lineName, function(data) {
        $('#x-seqstops-line').html('<img class="bullet-lcl" src="{0}"/>'.format(getTrainBulletIcon(lineName)));
        var html = `<tbody>`;

        console.log(data)
        for (var b in data) {
            let borodata = data[b];
            let borough = borodata.borough.toLowerCase();
            let color = borodata.color;
            html += `
            <tr style='height:20px;'>
                <th colspan=2 class='borough-header' style='background-color:${color}!important;'>
                    ${borough.replace('bronx', 'the bronx')}
                </th>
            </tr>`;
            for (var station of borodata.stations) {
                let stationName = station.name;
                let stationType = station.type;
                let stationGtfsId = station.id;

                var stop_data = stations[stationGtfsId];
                console.log(stop_data);

                let services = getServicesAtStation(stationGtfsId);

                var services_html = "";
                for (var i in services) {
                    var service = services[i];
                    //console.log(service);
                    if ((i % 3) == 0 && i != 0)
                        services_html += '<br/>'
                    var h = '<img class="bullet-lcl" src="{0}"/>'.format(getTrainBulletIcon(service));
                    services_html += h;
                }

                var default_string = `
                <tr class='x-station-item' onclick='onStationClick("${stationGtfsId}")'>
                    <td class='stopname-td'>${stationName}</strong></td>
                    <td class='bullet-td'>${services_html}</td>
                </tr>`;

                if (stationType == "0") {
                    html += default_string;    
                } else if (stationType == "2") {
                    html += `
                    <tr style='background-color:lightgray' class='x-station-item' onclick='onStationClick("${stationGtfsId}")'>
                        <td class='stopname-td' style='color:gray'><i class="fas fa-moon"></i> ${stationName}</strong></td>
                        <td class='bullet-td'>${services_html}</td>
                    </tr>`;
                } else if (stationType == "1") {
                    if (lineName == '2' || lineName == '4') {
                        html += `
                        <tr  class='x-station-item' onclick='onStationClick("${stationGtfsId}")'>
                            <td class='stopname-td' style='color:gray'>${stationName}<br/><span style='font-size:11px;'>Weekdays: Skipped Downtown 7-9am, Uptown 4:45-6:20pm</span></strong></td>
                            <td class='bullet-td'>${services_html}</td>
                        </tr>`;
                    } else if (lineName == '3' || lineName == '5') {
                        html += `
                        <tr style='background-color:lightgray' class='x-station-item' onclick='onStationClick("${stationGtfsId}")'>
                            <td class='stopname-td' style='color:gray'>🌞 ${stationName}<br/>
                            <span style='font-size:11px;'>No 
                            <img class="bullet-lcl" style='width:18px;height:18px;' src="${getTrainBulletIcon(lineName)}"/>
                            service at station 12-5am.</span></strong></td>
                            <td class='bullet-td'>${services_html}</td>
                        </tr>`;
                    } else {
                        // todo: others
                        html += default_string;
                    }
                }
            }
        }

        html += '</tbody>'
        console.log(html) 
        $('.service-stop-table').html(html);
    });
    // setTimeout(function() {
    //     location.hash = '#x-seqstops';
    //     location.hash = '#line='+lineName;    
    // }, 250);
}

function handleLocation(location) {
    console.log('Location:');
    console.log(location);
    var coords = location.coords;
    parent.location.hash = `#mappos=${coords.latitude},${coords.longitude}`
    parent.location.load();
}

function locate() {
    let $frame = $('#location-services-frame')[0];
    $frame.onload = function() {
        $frame.contentWindow.getCurrentLocation(function(location) {
            parent.handleLocation(location);
        }, true);
    }
}

if (!getParameterByName('mappos'))
    locate();

window.addEventListener('hashchange', function() {
    load();
})